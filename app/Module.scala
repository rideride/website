import com.google.inject.AbstractModule
import model.FBDatabaseProvider
import services.{RedditAuthService, ScoreboardUpdateService}

class Module extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[FBDatabaseProvider])
    bind(classOf[RedditAuthService])
    bind(classOf[ScoreboardUpdateService])
  }

}
