package model.forms

import model.FBDatabaseProvider
import model.db.Park
import play.api.data.Forms._
import play.api.data._

case class EditTeamForm(tag: String, name: String, park: Park, discordRole: Option[String], colorDiscord: Int, colorRoster: Int, colorRosterBG: Int, visible: Boolean, allPlayers: Boolean, webhook: Option[String], logoURL: Option[String])

object EditTeamForm {

  def editTeamForm(implicit db: FBDatabaseProvider): Form[EditTeamForm] = Form(
    mapping(
      "tag" -> nonEmptyText(minLength = 2, maxLength = 3),
      "name" -> nonEmptyText(maxLength = 50),
      "park" -> parkFormat,
      "discordRole" -> optional(snowflakeFormat),
      "colorDiscord" -> colorFormat,
      "colorRoster" -> colorFormat,
      "colorRosterBG" -> colorFormat,
      "visible" -> default(boolean, false),
      "allPlayers" -> default(boolean, false),
      "webhook" -> optional(nonEmptyText),
      "logoURL" -> optional(nonEmptyText)
    )(EditTeamForm.apply)(EditTeamForm.unapply)
  )

}
