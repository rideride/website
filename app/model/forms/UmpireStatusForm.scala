package model.forms

import model.FBDatabaseProvider
import model.db._
import play.api.data.Forms._
import play.api.data._
import slick.jdbc.MySQLProfile.api._

case class UmpireStatusForm(user: String, newUmpStatus: Boolean)

object UmpireStatusForm {

  def umpireStatusForm(implicit db: FBDatabaseProvider): Form[UmpireStatusForm] = Form(
    mapping(
      "user" -> nonEmptyText.transform[String]("/u/" + _, _.substring(3)).verifying("This user does not exist.", name => Users.filter(_.redditName === name).singleQOpt.isDefined),
      "status" -> boolean
    )(UmpireStatusForm.apply)(UmpireStatusForm.unapply)
  )

}
