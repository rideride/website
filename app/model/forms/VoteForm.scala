package model.forms

import play.api.data.Forms._
import play.api.data._

case class VoteForm(option1: Boolean, option2: Boolean, option3: Boolean, option4: Boolean, option5: Boolean, option6: Boolean, option7: Boolean, option8: Boolean)

object VoteForm {
  val candidates: Seq[String] = Seq(
    "Mason Miller",
    "Rickey Tucker",
    "Robin Reynolds",
    "Dirtbag Darrell",
    "Matt Himynamis",
    "Dixon Uraz",
    "Jacky Wacky",
    "John Johnson Jr."
  ).sorted

  val voteForm: Form[VoteForm] = Form(
    mapping(
      "option1" -> default(boolean, false),
      "option2" -> default(boolean, false),
      "option3" -> default(boolean, false),
      "option4" -> default(boolean, false),
      "option5" -> default(boolean, false),
      "option6" -> default(boolean, false),
      "option7" -> default(boolean, false),
      "option8" -> default(boolean, false),
    )(VoteForm.apply)(VoteForm.unapply)
  )
}


