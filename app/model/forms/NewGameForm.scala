package model.forms

import model.FBDatabaseProvider
import model.db._
import play.api.data.Forms._
import play.api.data._
import slick.jdbc.MySQLProfile.api._

case class NewGameForm(season: Int, session: Int, awayTeam: Team, homeTeam: Team, reddit_link: Option[String], statsCalculation: Boolean, park: Option[Int]) {

  val id36: Option[String] = reddit_link.map {
    case NewGameForm.REDDIT_ID36_REGEX(matchedID36) => matchedID36
    case link => link
  }

}

object NewGameForm {

  private val REDDIT_ID36_REGEX = """.*?reddit\.com/r/fakebaseball/comments/([0-9a-zA-Z]{6,7})/.*""".r

  def newGameForm(implicit db: FBDatabaseProvider): Form[NewGameForm] = Form(
    mapping(
      "season" -> number(min = 1),
      "session" -> number(min = 0),
      "awayTeam" -> teamFormat,
      "homeTeam" -> teamFormat,
      "id36" -> optional(nonEmptyText.verifying("This doesn't look like a reddit link or ID36.", link => {
        link match {
          case REDDIT_ID36_REGEX(_*) => true
          case _ => (link.length == 6 || link.length == 7) && link.forall(_.isLetterOrDigit)
        }
      })),
      "statsCalculation" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_)),
      "park" -> optional(number(min = 1))
    )(NewGameForm.apply)(NewGameForm.unapply)
      .verifying("The home team cannot be the same as the away team.", form => form.awayTeam.id != form.homeTeam.id)
      .verifying("The away team is already scheduled for a game in this session.", form => Games.filter(g => g.season === form.season && g.session === form.session && g.awayTeamId === form.awayTeam.id).singleOpt.isEmpty)
      .verifying("The home team is already scheduled for a game in this session.", form => Games.filter(g => g.season === form.season && g.session === form.session && g.homeTeamId === form.homeTeam.id).singleOpt.isEmpty)
  )

}
