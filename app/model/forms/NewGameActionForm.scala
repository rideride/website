package model.forms

import model.FBDatabaseProvider
import model.db._
import play.api.data.Forms._
import play.api.data._

case class NewGameActionForm(batter: Player, swing: Option[Int], pitcher: Player, pitch: Option[Int], playType: PlayType.PlayType, playResult: Option[String], save: Boolean)

object NewGameActionForm {

  def newGameActionForm(implicit db: FBDatabaseProvider): Form[NewGameActionForm] = Form(
    mapping(
      "batter" -> playerFormat,
      "swing" -> optional(number(min = 1, max = 1000)),
      "pitcher" -> playerFormat,
      "pitch" -> optional(number(min = 1, max = 1000)),
      "playType" -> number(min = PlayType.values.min.id, max = PlayType.values.max.id).transform[PlayType.PlayType](PlayType.apply, _.id),
      "playResult" -> optional(nonEmptyText(minLength = 1, maxLength = 3)),
      "save" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_))
    )(NewGameActionForm.apply)(NewGameActionForm.unapply)
      .verifying("You must provide a pitch for this play type.", form => form.pitch.isDefined || form.playType == PlayType.AUTO_K || form.playType == PlayType.AUTO_BB || form.playType == PlayType.IBB)
      .verifying("You must provide a swing for this play type.", form => form.swing.isDefined || form.playType == PlayType.AUTO_K || form.playType == PlayType.AUTO_BB || form.playType == PlayType.IBB)
  )

}
