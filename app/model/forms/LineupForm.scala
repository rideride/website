package model.forms

import model.{FBDatabaseProvider, lineupPositions}
import model.db._
import play.api.data.Forms._
import play.api.data._
import services.GlobalSettingsProvider

case class LineupForm(force: Boolean, player1: Player, p1Pos: String, player2: Player, p2Pos: String, player3: Player, p3Pos: String, player4: Player, p4Pos: String, player5: Player, p5Pos: String, player6: Player, p6Pos: String, player7: Player, p7Pos: String, player8: Player, p8Pos: String, player9: Player, p9Pos: String, pitcher: Player) {
  def tupled: Seq[(Player, String)] = Seq((player1, p1Pos), (player2, p2Pos), (player3, p3Pos), (player4, p4Pos), (player5, p5Pos), (player6, p6Pos), (player7, p7Pos), (player8, p8Pos), (player9, p9Pos))
}

object LineupForm {

  def lineupForm(team: Team, allowTeamMismatch: Boolean = false)(implicit db: FBDatabaseProvider, settingsProvider: GlobalSettingsProvider): Form[LineupForm] = {
    val allowOutOfPosition = !settingsProvider.RULES_ENFORCE_POSITIONS
    Form(
      mapping(
        "force" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_)),
        "player1" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
        "p1Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
        "player2" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
        "p2Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
        "player3" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
        "p3Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
        "player4" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
        "p4Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
        "player5" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
        "p5Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
        "player6" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
        "p6Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
        "player7" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
        "p7Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
        "player8" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
        "p8Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
        "player9" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
        "p9Pos" -> nonEmptyText(minLength = 1, maxLength = 2).verifying("This is not a valid position.", lineupPositions.contains _),
        "pitcher" -> playerFormat.verifying("This is not a valid player for this team.", p => allowTeamMismatch || team.allPlayers || p.teamId == team.id),
      )(LineupForm.apply)(LineupForm.unapply)
        .verifying("This lineup contains the same player twice.", lineup => lineup.force || lineup.tupled.map(_._1).toSet.size == 9)
        .verifying("This lineup contains the same position twice.", lineup => lineup.force || lineup.tupled.map(_._2).toSet.size == 9)
        .verifying("This lineup contains a pitcher-batter and a DH.", lineup => lineup.force || lineup.tupled.map(_._2).count(pos => pos == "DH" || pos == "P") == 1)
        .verifying("The pitcher-batter must be the pitcher.", lineup => lineup.force || lineup.tupled.find(_._2 == "P").forall(_._1 == lineup.pitcher))
        .verifying("The player in position 1 is ineligible to field this position.", lineup => allowOutOfPosition || lineup.force || lineup.player1.canPlay(lineup.p1Pos))
        .verifying("The player in position 2 is ineligible to field this position.", lineup => allowOutOfPosition || lineup.force || lineup.player2.canPlay(lineup.p2Pos))
        .verifying("The player in position 3 is ineligible to field this position.", lineup => allowOutOfPosition || lineup.force || lineup.player3.canPlay(lineup.p3Pos))
        .verifying("The player in position 4 is ineligible to field this position.", lineup => allowOutOfPosition || lineup.force || lineup.player4.canPlay(lineup.p4Pos))
        .verifying("The player in position 5 is ineligible to field this position.", lineup => allowOutOfPosition || lineup.force || lineup.player5.canPlay(lineup.p5Pos))
        .verifying("The player in position 6 is ineligible to field this position.", lineup => allowOutOfPosition || lineup.force || lineup.player6.canPlay(lineup.p6Pos))
        .verifying("The player in position 7 is ineligible to field this position.", lineup => allowOutOfPosition || lineup.force || lineup.player7.canPlay(lineup.p7Pos))
        .verifying("The player in position 8 is ineligible to field this position.", lineup => allowOutOfPosition || lineup.force || lineup.player8.canPlay(lineup.p8Pos))
        .verifying("The player in position 9 is ineligible to field this position.", lineup => allowOutOfPosition || lineup.force || lineup.player9.canPlay(lineup.p9Pos))
        .verifying("This not a valid pitcher for this team.", lineup => allowTeamMismatch || allowOutOfPosition || lineup.force || lineup.pitcher.canPlay("P"))
    )
  }

}
