package model

import model.db._
import model.db.util.Identified
import play.api.data.Forms._
import play.api.data.format.Formatter
import play.api.data.{FormError, Mapping}
import slick.jdbc.MySQLProfile.api._
import slick.lifted.AbstractTable

import scala.util.Try

package object forms {

  val colorFormat: Mapping[Int] = of(
    new Formatter[Int] {
      override def bind(key: String, data: Map[String, String]): Either[Seq[FormError], Int] = {
        val parse = if (data(key).startsWith("#")) data(key).substring(1) else data(key)
        Try(Integer.parseInt(parse, 16)).map(Right(_)).getOrElse(Left(Seq(FormError("error.color", "This doesn't look like a hex color."))))
      }

      override def unbind(key: String, value: Int): Map[String, String] = Map(key -> f"#$value%06X")
    }
  )

  val id36Format: Mapping[String] = of(
    new Formatter[String] {
      override def bind(key: String, data: Map[String, String]): Either[Seq[FormError], String] = {
        data(key) match {
          case REDDIT_ID36_REGEX(matched) => Right(matched)
          case l: String if l.length >= 6 && l.length <= 7 && l.forall(_.isLetterOrDigit) => Right(l)
          case _ => Left(Seq(FormError("error.id36", "This doesn't look like a reddit thread.")))
        }
      }

      override def unbind(key: String, value: String): Map[String, String] = Map(key -> value)
    }
  )

  val snowflakeFormat: Mapping[String] = text(minLength = 18, maxLength = 18).verifying("This doesn't look like a Discord Snowflake.", _.forall(_.isDigit))

  def tableFormat[TR <: AbstractTable[_] with Identified](table: TableQuery[TR])(idSelector: TR#TableElementType => Int)(errorMessage: String)(implicit db: FBDatabaseProvider): Mapping[TR#TableElementType] = of(
    new Formatter[TR#TableElementType] {
      override def bind(key: String, data: Map[String, String]): Either[Seq[FormError], TR#TableElementType] = {
        Try(data(key).toInt).fold(
          _ => Left(Seq(FormError("error.int", errorMessage))),
          id => table.findById(id).map(Right(_))
            .getOrElse(Left(Seq(FormError("error.null", errorMessage))))
        )
      }
      override def unbind(key: String, value: TR#TableElementType): Map[String, String] = Map(key -> idSelector(value).toString)
    }
  )

  def userFormat(implicit db: FBDatabaseProvider): Mapping[User] = tableFormat(Users)(_.id.get)("This is not a valid user.")
  def playerFormat(implicit db: FBDatabaseProvider): Mapping[Player] = tableFormat(Players)(_.id.get)("This is not a valid player.")
  def teamFormat(implicit db: FBDatabaseProvider): Mapping[Team] = tableFormat(Teams)(_.id.get)("This is not a valid team.")
  def parkFormat(implicit db: FBDatabaseProvider): Mapping[Park] = tableFormat(Parks)(_.id.get)("This is not a valid park.")
  def gameFormat(implicit db: FBDatabaseProvider): Mapping[Game] = tableFormat(Games)(_.id.get)("This is not a valid game.")
  def battingTypeFormat(implicit db: FBDatabaseProvider): Mapping[BattingType] = tableFormat(BattingTypes)(_.id.get)("This is not a valid batting type.")
  def pitchingTypeFormat(implicit db: FBDatabaseProvider): Mapping[PitchingType] = tableFormat(PitchingTypes)(_.id.get)("This is not a valid pitching type.")
  def pitchingBonusFormat(implicit db: FBDatabaseProvider): Mapping[PitchingBonus] = tableFormat(PitchingBonuses)(_.id.get)("This is not a valid pitching bonus.")

}
