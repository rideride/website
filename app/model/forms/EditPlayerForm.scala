package model.forms

import model.{FBDatabaseProvider, PlayerNamed}
import model.db._
import play.api.data.Forms._
import play.api.data._

case class EditPlayerForm(player: Player, firstName: Option[String], lastName: String, battingType: BattingType, pitchingType: Option[PitchingType], pitchingBonus: Option[PitchingBonus], rightHanded: Boolean, positionPrimary: String, positionSecondary: Option[String], positionTertiary: Option[String]) extends PlayerNamed

object EditPlayerForm {

  def editPlayerForm(implicit db: FBDatabaseProvider): Form[EditPlayerForm] = Form(
    mapping(
      "player" -> playerFormat,
      "firstName" -> optional(nonEmptyText),
      "lastName" -> nonEmptyText,
      "battingType" -> battingTypeFormat,
      "pitchingType" -> optional(pitchingTypeFormat),
      "pitchingBonus" -> optional(pitchingBonusFormat),
      "rightHanded" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_)),
      "positionPrimary" -> nonEmptyText(minLength = 1, maxLength = 2),
      "positionSecondary" -> optional(nonEmptyText(minLength = 1, maxLength = 2)),
      "positionTertiary" -> optional(nonEmptyText(minLength = 1, maxLength = 2))
    )(EditPlayerForm.apply)(EditPlayerForm.unapply)
  )

}
