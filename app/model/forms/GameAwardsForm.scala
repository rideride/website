package model.forms

import model.FBDatabaseProvider
import model.db._
import play.api.data.Forms._
import play.api.data._

case class GameAwardsForm(winningPitcher: Player, losingPitcher: Player, playerOfTheGame: Player, save: Option[Player])

object GameAwardsForm {

  def gameAwardsForm(implicit db: FBDatabaseProvider): Form[GameAwardsForm] = Form(
    mapping(
      "winningPitcher" -> playerFormat,
      "losingPitcher" -> playerFormat,
      "playerOfTheGame" -> playerFormat,
      "save" -> optional(playerFormat)
    )(GameAwardsForm.apply)(GameAwardsForm.unapply)
  )

}
