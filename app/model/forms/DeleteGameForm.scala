package model.forms

import model.FBDatabaseProvider
import model.db._
import play.api.data.Forms._
import play.api.data._

case class DeleteGameForm(game: Game)

object DeleteGameForm {

  def deleteGameForm(implicit db: FBDatabaseProvider): Form[DeleteGameForm] = Form(
    mapping(
      "game" -> gameFormat
    )(DeleteGameForm.apply)(DeleteGameForm.unapply)
  )

}
