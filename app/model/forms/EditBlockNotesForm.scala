package model.forms

import play.api.data._
import play.api.data.Forms._

case class EditBlockNotesForm(newNotes: Option[String])

object EditBlockNotesForm {

  val editBlockNotesForm = Form(
    mapping(
      "newNotes" -> optional(text)
    )(EditBlockNotesForm.apply)(EditBlockNotesForm.unapply)
  )

}
