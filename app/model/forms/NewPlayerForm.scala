package model.forms

import model.{FBDatabaseProvider, availablePositions}
import model.db._
import play.api.data.Forms._
import play.api.data._
import slick.jdbc.MySQLProfile.api._

case class NewPlayerForm(returning: Boolean, firstName: Option[String], lastName: String, willingToJoinDiscord: Boolean, discordName: Option[String], positionPrimary: String, positionSecondary: Option[String], rightHanded: Boolean, battingType: BattingType, pitchingType: Option[PitchingType], pitchingBonus: Option[PitchingBonus], agreeToTerms: Boolean) {

  def validatePositions(): Boolean = {
    if (positionPrimary == "P")
      positionSecondary.isEmpty
    else
      positionSecondary.exists { pos =>
        NewPlayerForm.secondaryPositionMap
          .getOrElse(positionPrimary, Set())
          .contains(pos)
      }
  }

  def validatePlayerTypes(): Boolean = {
    if (positionPrimary != "P")
      pitchingType.isEmpty &&
        pitchingBonus.isEmpty
    else
      pitchingType.isDefined &&
        pitchingBonus.isDefined
  }

}

object NewPlayerForm {

  val secondaryPositionMap: Map[String, Set[String]] = Map(
    "C" -> Set("1B", "3B"),
    "1B" -> Set("3B", "LF"),
    "2B" -> Set("SS", "1B", "RF"),
    "3B" -> Set("2B", "SS", "LF"),
    "SS" -> Set("2B", "3B", "CF"),
    "LF" -> Set("RF", "1B"),
    "CF" -> Set("LF", "RF"),
    "RF" -> Set("CF", "1B")
  )

  def newPlayerForm(implicit db: FBDatabaseProvider): Form[NewPlayerForm] = Form(
    mapping(
      "returning" -> boolean,
      "firstName" -> optional(text(minLength = 1, maxLength = 15)),
      "lastName" -> text(minLength = 1, maxLength = 15),
      "willingToJoinDiscord" -> boolean,
      "discordName" -> optional(nonEmptyText),
      "positionPrimary" -> text(minLength = 1, maxLength = 2).verifying("This is not a valid position.", availablePositions.contains _),
      "positionSecondary" -> optional(text(minLength = 1, maxLength = 2)),
      "rightHanded" -> boolean,
      "battingType" -> battingTypeFormat,
      "pitchingType" -> optional(pitchingTypeFormat),
      "pitchingBonus" -> optional(pitchingBonusFormat),
      "agreeToTerms" -> checked("You must agree to these terms.")
    )(NewPlayerForm.apply)(NewPlayerForm.unapply)
      .verifying("This name is already taken.", app => Players.filter(p => p.firstName === app.firstName && p.lastName === app.lastName).singleQOpt.isEmpty)
      .verifying("That is not a valid secondary position for the selected primary position.", _.validatePositions())
      .verifying("Your batting/pitching types are not valid.", _.validatePlayerTypes())
  )
}
