package model.forms

import play.api.data.Forms._
import play.api.data._

case class EditScoringPlayForm(gameAction: Int, newDescription: String)

object EditScoringPlayForm {

  val editScoringPlayForm = Form(
    mapping(
      "gameAction" -> number(min = 1),
      "newDescription" -> nonEmptyText()
    )(EditScoringPlayForm.apply)(EditScoringPlayForm.unapply)
  )

}