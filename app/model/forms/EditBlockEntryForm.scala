package model.forms

import model.FBDatabaseProvider
import model.db._
import play.api.data.Forms._
import play.api.data._

case class EditBlockEntryForm(player: Player, notes: String, removal: Boolean)

object EditBlockEntryForm {

  def editBlockEntryForm(implicit db: FBDatabaseProvider): Form[EditBlockEntryForm] = Form(
    mapping(
      "player" -> playerFormat,
      "notes" -> nonEmptyText,
      "removal" -> default(boolean, false)
    )(EditBlockEntryForm.apply)(EditBlockEntryForm.unapply)
  )

}
