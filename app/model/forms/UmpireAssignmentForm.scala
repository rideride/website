package model.forms

import model.FBDatabaseProvider
import model.db._
import model.db.util._
import play.api.data.Forms._
import play.api.data._
import slick.jdbc.MySQLProfile.api._

case class UmpireAssignmentForm(umpire: User, game: Game, addition: Boolean)

object UmpireAssignmentForm {

  def umpireAssignmentForm(implicit db: FBDatabaseProvider): Form[UmpireAssignmentForm] = Form(
    mapping(
      "umpire" -> userFormat,
      "game" -> gameFormat,
      "addition" -> boolean
    )(UmpireAssignmentForm.apply)(UmpireAssignmentForm.unapply)
      .verifying("This umpire is already assigned to this game.", ua => !ua.addition || !UmpireAssignments.filter(row => row.umpireId === ua.umpire.id.get && row.gameId === ua.game.id.get).exists.result.run)
  )

}
