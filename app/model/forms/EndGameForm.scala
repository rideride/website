package model.forms

import model.FBDatabaseProvider
import model.db._
import play.api.data.Forms._
import play.api.data._

case class EndGameForm(game: Game, winningTeam: Team)

object EndGameForm {

  def endGameForm(implicit db: FBDatabaseProvider): Form[EndGameForm] = Form(
    mapping(
      "game" -> gameFormat,
      "winningTeam" -> teamFormat
    )(EndGameForm.apply)(EndGameForm.unapply)
  )

}
