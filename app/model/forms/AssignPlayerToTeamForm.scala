package model.forms

import model.FBDatabaseProvider
import model.db._
import play.api.data.Forms._
import play.api.data._

case class AssignPlayerToTeamForm(team: Option[Team], startSeason: Int, startSession: Int)

object AssignPlayerToTeamForm {

  def assignPlayerToTeamForm(implicit db: FBDatabaseProvider): Form[AssignPlayerToTeamForm] = Form(
    mapping(
      "team" -> optional(teamFormat),
      "startSeason" -> number(min = 1),
      "startSession" -> number(min = 1)
    )(AssignPlayerToTeamForm.apply)(AssignPlayerToTeamForm.unapply)
  )

}
