package model.forms

import play.api.data._
import play.api.data.Forms._

case class ScoreboardSessionForm(season: Int, session: Int)

object ScoreboardSessionForm {

  val scoreboardSessionForm = Form(
    mapping(
      "season" -> number,
      "session" -> number,
    )(ScoreboardSessionForm.apply)(ScoreboardSessionForm.unapply)
  )

}

