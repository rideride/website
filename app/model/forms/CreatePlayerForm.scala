package model.forms

import model.{FBDatabaseProvider, PlayerNamed}
import model.db._
import play.api.data.Forms._
import play.api.data._

case class CreatePlayerForm(redditName: String, firstName: Option[String], lastName: String, team: Option[Int], battingType: BattingType, pitchingType: Option[PitchingType], pitchingBonus: Option[PitchingBonus], rightHanded: Boolean, positionPrimary: String, positionSecondary: Option[String]) extends PlayerNamed

object CreatePlayerForm {

  def createPlayerForm(implicit db: FBDatabaseProvider): Form[CreatePlayerForm] = Form(
    mapping(
      "redditName" -> nonEmptyText(minLength = 3, maxLength = 23),
      "firstName" -> optional(nonEmptyText(maxLength = 15)),
      "lastName" -> nonEmptyText(maxLength = 15),
      "team" -> optional(number(min = 1)),
      "battingType" -> battingTypeFormat,
      "pitchingType" -> optional(pitchingTypeFormat),
      "pitchingBonus" -> optional(pitchingBonusFormat),
      "rightHanded" -> optional(boolean).transform[Boolean](_.getOrElse(false), Some(_)),
      "positionPrimary" -> nonEmptyText(maxLength = 2),
      "positionSecondary" -> optional(nonEmptyText(maxLength = 2))
    )(CreatePlayerForm.apply)(CreatePlayerForm.unapply)
  )

}
