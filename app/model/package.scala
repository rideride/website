import java.sql.Timestamp
import java.util.Date

import model.db.PlayType.PlayType
import model.db._
import play.api.libs.json.Writes

import scala.language.{higherKinds, implicitConversions}
import scala.util.matching.Regex

package object model {

  trait PlayerNamed {
    val firstName: Option[String]
    val lastName: String
    def fullName: String = s"${firstName.getOrElse("")} $lastName".trim
  }

  implicit val DATE_TIME_WRITE: Writes[Date] = play.api.libs.json.Writes.dateWrites("yyyy-MM-dd'T'HH:mm:ss'Z'")

  val availablePositions: Seq[String] = Seq(
    "P",
    "C",
    "1B",
    "2B",
    "3B",
    "SS",
    "LF",
    "CF",
    "RF"
  )

  val lineupPositions: Set[String] = availablePositions.toSet + "DH" + "PH"

  val REDDIT_ID36_REGEX: Regex = """.*?reddit\.com/r/.*?/comments/([0-9a-zA-Z]{6,7})/.*""".r

  implicit class BooleanToIntClass(b: Boolean) {
    def toInt: Int = if (b) 1 else 0
  }

  implicit def battingType2Range(bt: BattingType): CalcRange = CalcRange(bt.rangeHR, bt.range3B, bt.range2B, bt.range1B, bt.rangeBB, bt.rangeFO, bt.rangeK, bt.rangePO, bt.rangeRGO, bt.rangeLGO)

  implicit def pitchingType2Range(pt: PitchingType): CalcRange = CalcRange(pt.rangeHR, pt.range3B, pt.range2B, pt.range1B, pt.rangeBB, pt.rangeFO, pt.rangeK, pt.rangePO, pt.rangeRGO, pt.rangeLGO)

  implicit def pitchingBonus2Range(pb: PitchingBonus): CalcRange = CalcRange(pb.rangeHR, pb.range3B, pb.range2B, pb.range1B, pb.rangeBB, pb.rangeFO, pb.rangeK, pb.rangePO, pb.rangeRGO, pb.rangeLGO)

  case class SwingResult(diff: Option[Int], result: String, resultMin: Option[Int], resultMax: Option[Int])

  case class PartialGameAction(playType: PlayType, pitcher: Int, pitch: Option[Int], batter: Int, swing: Option[Int], diff: Option[Int], result: String, runsScored: Int, scorers: Option[String], outsTracked: Int) {
    def toGameAction(game: Game, afterState: Int): GameAction = GameAction(None, None, game.id.get, playType, pitcher, pitch, batter, swing, diff, result, runsScored, scorers, outsTracked, game.state.id.get, afterState, new Timestamp(System.currentTimeMillis()))
  }

}
