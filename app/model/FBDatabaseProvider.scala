package model

import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import javax.sql.DataSource
import org.flywaydb.core.Flyway
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.dbio.{DBIOAction, NoStream}
import slick.jdbc.MySQLProfile
import slick.jdbc.hikaricp.HikariCPJdbcDataSource

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class FBDatabaseProvider @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends HasDatabaseConfigProvider[MySQLProfile] {

  val dbInstance = db

  // need to run migrations here in order to ensure that they run during startup
  Flyway.configure()
    .dataSource(dbInstance.source.asInstanceOf[HikariCPJdbcDataSource].ds)
    .locations("classpath:evolutions")
    .load()
    .migrate()

  def await[R](action: DBIOAction[R, NoStream, Nothing]): R = {
    Await.result(dbInstance.run(action), Duration(15, TimeUnit.SECONDS))
  }

}
