package model.db

import java.sql.Timestamp

import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class TeamTradeBlockNote(teamId: Int, notes: String, lastUpdated: Timestamp = new Timestamp(System.currentTimeMillis())) {

  val team: LazyReference[Team, TeamTradeBlockNote] = lazyReference("team")

}

class TeamTradeBlockNotes(tag: Tag) extends Table[TeamTradeBlockNote](tag, "team_trade_block_notes") with Identified {

  def id = column[Int]("team", O.PrimaryKey)
  def notes = column[String]("notes")
  def lastUpdated = column[Timestamp]("last_updated")

  def team = this.mappedForeignKey("fk_team_trade_block_notes_team", _.id, Teams)(_.team, _.id)

  def * = (id, notes, lastUpdated) <> (TeamTradeBlockNote.tupled, TeamTradeBlockNote.unapply)

}

object TeamTradeBlockNotes extends TableQuery[TeamTradeBlockNotes](tag => new TeamTradeBlockNotes(tag))
