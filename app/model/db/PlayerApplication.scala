package model.db

import java.sql.Timestamp

import model.db.PlayerApplicationStatus.{ColumnMapping, PlayerApplicationStatus}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

object PlayerApplicationStatus extends DatabaseEnumeration {
  type PlayerApplicationStatus = Value
  val PENDING = Value(0)
  val REJECTED = Value(1)
  val ACCEPTED = Value(2)
}

case class PlayerApplication(id: Option[Int] = None, userId: Int, lastName: String, firstName: Option[String], isReturningPlayer: Boolean, isWillingToJoinDiscord: Boolean, discordName: Option[String], positionPrimary: String, positionSecondary: Option[String], isRightHanded: Boolean, battingTypeId: Int, pitchingTypeId: Option[Int], pitchingBonusId: Option[Int], status: PlayerApplicationStatus, rejectionMessage: Option[String], respondedById: Option[Int], respondedAt: Option[Timestamp]) {

  val user: LazyReference[User, PlayerApplication] = lazyReference("user")
  val battingType: LazyReference[BattingType, PlayerApplication] = lazyReference("battingType")
  val pitchingType: LazyReference[Option[PitchingType], PlayerApplication] = lazyReference("pitchingType")
  val pitchingBonus: LazyReference[Option[PitchingBonus], PlayerApplication] = lazyReference("pitchingBonus")
  val respondedBy: LazyReference[Option[User], PlayerApplication] = lazyReference("respondedBy")

  lazy val fullName: String = s"${firstName.getOrElse("")} $lastName".trim

}

class PlayerApplications(tag: Tag) extends Table[PlayerApplication](tag, "player_applications") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def userId = column[Int]("user")
  def lastName = column[String]("last_name")
  def firstName = column[Option[String]]("first_name")
  def isReturningPlayer = column[Boolean]("is_returning_player")
  def isWillingToJoinDiscord = column[Boolean]("is_willing_to_join_discord")
  def discordName = column[Option[String]]("discord_name")
  def positionPrimary = column[String]("position_primary")
  def positionSecondary = column[Option[String]]("position_secondary")
  def isRightHanded = column[Boolean]("is_right_handed")
  def battingTypeId = column[Int]("batting_type")
  def pitchingTypeId = column[Option[Int]]("pitching_type")
  def pitchingBonusId = column[Option[Int]]("pitching_bonus")
  def status = column[PlayerApplicationStatus]("status")
  def rejectionMessage = column[Option[String]]("reject_message")
  def respondedById = column[Option[Int]]("responded_by")
  def respondedAt = column[Option[Timestamp]]("responded_at")

  def user = this.mappedForeignKey("fk_player_applications_user", _.userId, Users)(_.user, _.id)
  def battingType = this.mappedForeignKey("fk_player_applications_batting_type", _.battingTypeId, BattingTypes)(_.battingType, _.id)
  def pitchingType = this.mappedForeignKeyOpt("fk_player_applications_pitching_type", _.pitchingTypeId, PitchingTypes)(_.pitchingType, _.id)
  def pitchingBonus = this.mappedForeignKeyOpt("fk_player_applications_pitching_bonus", _.pitchingBonusId, PitchingBonuses)(_.pitchingBonus, _.id)
  def respondedBy = this.mappedForeignKeyOpt("fk_player_applications_rejecter", _.respondedById, Users)(_.respondedBy, _.id)

  def * = (id.?, userId, lastName, firstName, isReturningPlayer, isWillingToJoinDiscord, discordName, positionPrimary, positionSecondary, isRightHanded, battingTypeId, pitchingTypeId, pitchingBonusId, status, rejectionMessage, respondedById, respondedAt) <> (PlayerApplication.tupled, PlayerApplication.unapply)

}

object PlayerApplications extends TableQuery[PlayerApplications](tag => new PlayerApplications(tag))
