package model.db

import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class UserPreference(userId: Int, umpBatterPing: Option[String] = None) {
  val user: LazyReference[User, UserPreference] = lazyReference("user")

  def withDefaults: UserPreference = this.copy(umpBatterPing = Some(umpBatterPing.getOrElse(UserPreferences.DEFAULT_UMP_BATTER_PING_FORMAT)))
}

class UserPreferences(tag: Tag) extends Table[UserPreference](tag, "user_preferences") {

  def userId = column[Int]("user", O.PrimaryKey)
  def umpBatterPing = column[Option[String]]("ump_batter_ping")

  def user = this.mappedForeignKey("fk_user_preferences_user", _.userId, Users)(_.user, _.id, Cascade, Cascade)

  def * = (userId, umpBatterPing) <> (UserPreference.tupled, UserPreference.unapply)

}

object UserPreferences extends TableQuery[UserPreferences](tag => new UserPreferences(tag)) {
  private[db] val DEFAULT_UMP_BATTER_PING_FORMAT = "%awayTag% %awayScore% - %homeScore% %homeTag% | %basesDiamonds% | %inning% %outs% Out\n\n%pitchingChange%\n\n%batterTeamTag% %batterPosition% %batterPing%: %batterRecord%\n\n"
}
