package model.db

import model.db.json.{Jsonable, PitchingTypeJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class PitchingType(id: Option[Int] = None, shortcode: String, name: String, rangeHR: Int, range3B: Int, range2B: Int, range1B: Int, rangeBB: Int, rangeFO: Int, rangeK: Int, rangePO: Int, rangeRGO: Int, rangeLGO: Int) extends Jsonable[PitchingType] {

  override val jsonConverter = PitchingTypeJsonConverter

  val players: LazyReference[Seq[Player], PitchingType] = lazyReference("players")

}

class PitchingTypes(tag: Tag) extends Table[PitchingType](tag, "pitching_types") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def shortcode = column[String]("shortcode")
  def name = column[String]("name")
  def rangeHR = column[Int]("range_hr")
  def range3B = column[Int]("range_3b")
  def range2B = column[Int]("range_2b")
  def range1B = column[Int]("range_1b")
  def rangeBB = column[Int]("range_bb")
  def rangeFO = column[Int]("range_fo")
  def rangeK = column[Int]("range_k")
  def rangePO = column[Int]("range_po")
  def rangeRGO = column[Int]("range_rgo")
  def rangeLGO = column[Int]("range_lgo")

  def uniqueShortcode = index("shortcode", shortcode, unique = true)
  def uniqueName = index("name", name, unique = true)

  def players = this.mapSeq(pt => Players.mapFilter(_.battingTypeId === pt.id))(_.players)

  def * = (id.?, shortcode, name, rangeHR, range3B, range2B, range1B, rangeBB, rangeFO, rangeK, rangePO, rangeRGO, rangeLGO) <> (PitchingType.tupled, PitchingType.unapply)

}

object PitchingTypes extends TableQuery[PitchingTypes](tag => new PitchingTypes(tag))
