package model.db

import model.db.json.{BattingTypeJsonConverter, Jsonable}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class BattingType(id: Option[Int] = None, shortcode: String, name: String, rangeHR: Int, range3B: Int, range2B: Int, range1B: Int, rangeBB: Int, rangeFO: Int, rangeK: Int, rangePO: Int, rangeRGO: Int, rangeLGO: Int, rangeSB2: Int, rangeSB3: Int, rangeSB4: Int) extends Jsonable[BattingType] {

  override val jsonConverter = BattingTypeJsonConverter

  val players: LazyReference[Seq[Player], BattingType] = lazyReference("players")

}

class BattingTypes(tag: Tag) extends Table[BattingType](tag, "batting_types") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def shortcode = column[String]("shortcode")
  def name = column[String]("name")
  def rangeHR = column[Int]("range_hr")
  def range3B = column[Int]("range_3b")
  def range2B = column[Int]("range_2b")
  def range1B = column[Int]("range_1b")
  def rangeBB = column[Int]("range_bb")
  def rangeFO = column[Int]("range_fo")
  def rangeK = column[Int]("range_k")
  def rangePO = column[Int]("range_po")
  def rangeRGO = column[Int]("range_rgo")
  def rangeLGO = column[Int]("range_lgo")
  def rangeSB2 = column[Int]("range_sb2")
  def rangeSB3 = column[Int]("range_sb3")
  def rangeSB4 = column[Int]("range_sb4")

  def uniqueShortcode = index("shortcode", shortcode, unique = true)
  def uniqueName = index("name", name, unique = true)

  def players = this.mapSeq(p => Players.mapFilter(_.battingTypeId === p.id))(_.players)

  def * = (id.?, shortcode, name, rangeHR, range3B, range2B, range1B, rangeBB, rangeFO, rangeK, rangePO, rangeRGO, rangeLGO, rangeSB2, rangeSB3, rangeSB4) <> (BattingType.tupled, BattingType.unapply)

}

object BattingTypes extends TableQuery[BattingTypes](tag => new BattingTypes(tag))
