package model.db

import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class League(id: Option[Int] = None, name: String, tag: String) {

  val divisions: LazyReference[Seq[Division], League] = lazyReference("divisions")

  override def equals(obj: Any): Boolean = obj match {
    case other: League => other.id == this.id
    case _ => false
  }

}

class Leagues(tableTag: Tag) extends Table[League](tableTag, "leagues") {

  def id = column[Int]("id", O.AutoInc, O.PrimaryKey)
  def name = column[String]("name")
  def tag = column[String]("tag")

  def divisions = this.mapSeq(l => Divisions.filter(_.leagueId === l.id))(_.divisions)

  def * = (id.?, name, tag) <> (League.tupled, League.unapply)

}

object Leagues extends TableQuery[Leagues](tag => new Leagues(tag))
