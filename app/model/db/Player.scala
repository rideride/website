package model.db

import model.{FBDatabaseProvider, PlayerNamed}
import model.db.json.{Jsonable, PlayerJsonConverter}
import model.db.util._
import services.StatCalculatorService
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class Player(id: Option[Int] = None, userId: Int, name: String, firstName: Option[String], lastName: String, teamId: Option[Int] = None, battingTypeId: Int, pitchingTypeId: Option[Int], pitchingBonusId: Option[Int], rightHanded: Boolean, positionPrimary: String, positionSecondary: Option[String] = None, positionTertiary: Option[String] = None, deactivated: Boolean = false) extends PlayerNamed with Jsonable[Player] {

  override val jsonConverter = PlayerJsonConverter

  val user: LazyReference[User, Player] = lazyReference("user")
  val team: LazyReference[Option[Team], Player] = lazyReference("team")
  val battingType: LazyReference[BattingType, Player] = lazyReference("battingType")
  val pitchingType: LazyReference[Option[PitchingType], Player] = lazyReference("pitchingType")
  val pitchingBonus: LazyReference[Option[PitchingBonus], Player] = lazyReference("pitchingBonus")

  val nickname: LazyReference[Option[PlayerNickname], Player] = lazyReference("nickname")
  val teamAssignments: LazyReference[Seq[PlayerTeamAssignment], Player] = lazyReference("teamAssignments")
  val battingStats: LazyReference[Seq[BattingStat], Player] = lazyReference("battingStats")
  val pitchingStats: LazyReference[Seq[PitchingStat], Player] = lazyReference("pitchingStats")
  val battingPlays: LazyReference[Seq[GameAction], Player] = lazyReference("battingPlays")
  val pitchingPlays: LazyReference[Seq[GameAction], Player] = lazyReference("pitchingPlays")

  def canPlay(pos: String): Boolean =
    pos == "DH" ||
    positionPrimary == "PH" ||
    positionPrimary == pos ||
    positionSecondary.contains(pos) ||
    positionTertiary.contains(pos)

  def recalculateBattingStats(season: Int)(implicit db: FBDatabaseProvider): Unit = StatCalculatorService.recalculateBattingStats(id.get, season)
  def recalculatePitchingStats(season: Int)(implicit db: FBDatabaseProvider): Unit = StatCalculatorService.recalculateBattingStats(id.get, season)

  override def fullName: String =
    nickname._value
      .flatten
      .map(nickname => s"${nickname.fullName} (${super.fullName})")
      .getOrElse(super.fullName)

}

class Players(tag: Tag) extends Table[Player](tag, "players") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def userId = column[Int]("user")
  def name = column[String]("name")
  def firstName = column[Option[String]]("first_name")
  def lastName = column[String]("last_name")
  def teamId = column[Option[Int]]("team")
  def battingTypeId = column[Int]("batting_type")
  def pitchingTypeId = column[Option[Int]]("pitching_type")
  def pitchingBonusId = column[Option[Int]]("pitching_bonus")
  def rightHanded = column[Boolean]("right_handed")
  def positionPrimary = column[String]("position_primary")
  def positionSecondary = column[Option[String]]("position_secondary")
  def positionTertiary = column[Option[String]]("position_tertiary")
  def deactivated = column[Boolean]("deactivated")

  def user = this.mappedForeignKey("fk_player_user", _.userId, Users)(_.user, _.id, onUpdate = Cascade)
  def team = this.mappedForeignKeyOpt("fk_player_team", _.teamId, Teams)(_.team, _.id)
  def battingType = this.mappedForeignKey("fk_player_batting_type", _.battingTypeId, BattingTypes)(_.battingType, _.id)
  def pitchingType = this.mappedForeignKeyOpt("fk_player_pitching_type", _.pitchingTypeId, PitchingTypes)(_.pitchingType, _.id)
  def pitchingBonus = this.mappedForeignKeyOpt("fk_player_pitching_bonus", _.pitchingBonusId, PitchingBonuses)(_.pitchingBonus, _.id)

  def nickname = this.mapOpt(_.id.?, PlayerNicknames)(_.nickname, _.id.?)
  def teamAssignments = this.mapSeq(p => PlayerTeamAssignments.filter(_.playerId === p.id))(_.teamAssignments)
  def battingStats = this.mapSeq(p => BattingStats.filter(_.playerId === p.id))(_.battingStats)
  def pitchingStats = this.mapSeq(p => PitchingStats.filter(_.playerId === p.id))(_.pitchingStats)
  def battingPlays = this.mapSeq(p => GameActions.filter(_.batterId === p.id))(_.battingPlays)
  def pitchingPlays = this.mapSeq(p => GameActions.filter(_.pitcherId === p.id))(_.pitchingPlays)

  def uniqueName = index("name", name, unique = true)

  def * = (id.?, userId, name, firstName, lastName, teamId, battingTypeId, pitchingTypeId, pitchingBonusId, rightHanded, positionPrimary, positionSecondary, positionTertiary, deactivated) <> (Player.tupled, Player.unapply)

}

object Players extends TableQuery[Players](tag => new Players(tag)) {

  implicit class PlayersMappedQueryExt[TO, RO](query: MappedQuery[Players, TO, RO]) {
    def withTypes =
      query.withMap(_.battingType).withMapOpt(_.pitchingType).withMapOpt(_.pitchingBonus)

    def byTeam(team: Team): MappedQuery[Players, TO, RO] = query.mapFilter(to => query.unpacker(to).teamId === team.id)
    def byTeam(teamId: Int): MappedQuery[Players, TO, RO] = query.mapFilter(to => query.unpacker(to).teamId === teamId)
  }

  def withTypes = this.wrapped.withTypes
  def byTeam(team: Team): MappedQuery[Players, Players, Player] = this.wrapped.byTeam(team)
  def byTeam(teamId: Int): MappedQuery[Players, Players, Player] = this.wrapped.byTeam(teamId)

  private val POSITIONS_ORDER = Array("PH", "P", "C", "1B", "2B", "3B", "SS", "LF", "CF", "RF")
  def positionSort(a: Player, b: Player): Boolean = {
    if (a.positionPrimary != b.positionPrimary)
      POSITIONS_ORDER.indexOf(a.positionPrimary).compareTo(POSITIONS_ORDER.indexOf(b.positionPrimary)) < 0
    else if (a.positionSecondary != b.positionSecondary)
      POSITIONS_ORDER.indexOf(a.positionSecondary.getOrElse("")).compareTo(POSITIONS_ORDER.indexOf(b.positionSecondary.getOrElse(""))) < 0
    else if (a.positionTertiary != b.positionTertiary)
      POSITIONS_ORDER.indexOf(a.positionTertiary.getOrElse("")).compareTo(POSITIONS_ORDER.indexOf(b.positionTertiary.getOrElse(""))) < 0
    else
      a.fullName.compareTo(b.fullName) < 0
  }

}
