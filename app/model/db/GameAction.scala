package model.db

import java.sql.Timestamp

import model.db.PlayType.{ColumnMapping, PlayType}
import model.db.json.{GameActionJsonConverter, Jsonable}
import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.{Cascade, SetNull}

object PlayType extends DatabaseEnumeration {
  type PlayType = Value
  val SWING = Value(1)
  val AUTO_K = Value(2)
  val AUTO_BB = Value(3)
  val BUNT = Value(4)
  val INFIELD_IN = Value(5)
  val STEAL = Value(6)
  val MULTI_STEAL = Value(7)
  val IBB = Value(8)
}

case class GameAction(id: Option[Int], replacedById: Option[Int] = None, gameId: Int, playType: PlayType, pitcherId: Int, pitch: Option[Int] = None, batterId: Int, swing: Option[Int] = None, diff: Option[Int] = None, result: String, runsScored: Int, scorers: Option[String] = None, outsTracked: Int, beforeStateId: Int, afterStateId: Int, actionStamp: Timestamp) extends Jsonable[GameAction] {

  override val jsonConverter = GameActionJsonConverter

  val replacedBy: LazyReference[Option[GameAction], GameAction] = lazyReference("replacedBy")
  val game: LazyReference[Game, GameAction] = lazyReference("game")
  val pitcher: LazyReference[Player, GameAction] = lazyReference("pitcher")
  val batter: LazyReference[Player, GameAction] = lazyReference("batter")
  val beforeState: LazyReference[GameState, GameAction] = lazyReference("beforeState")
  val afterState: LazyReference[GameState, GameAction] = lazyReference("afterState")

  val scoringPlay: LazyReference[Option[ScoringPlay], GameAction] = lazyReference("scoringPlay")

  lazy val preview = s"Swing: ${swing.getOrElse("x")}  \nPitch: ${pitch.getOrElse("x")}  \nResult: $result"

  def isHit: Boolean = {
    result == "HR" ||
    result == "3B" ||
    result == "2B" ||
    result == "1B" ||
    result == "Bunt 1B"
  }

  def isAB: Boolean = {
    isHit ||
    (result == "FO" && runsScored == 0) ||
    result == "K" ||
    result == "PO" ||
    result == "RGO" ||
    result == "LGO"
  }

}

class GameActions(tag: Tag) extends Table[GameAction](tag, "game_actions") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def replacedById = column[Option[Int]]("replaced_by")
  def gameId = column[Int]("game_id")
  def playType = column[PlayType]("play_type")
  def pitcherId = column[Int]("pitcher")
  def pitch = column[Option[Int]]("pitch")
  def batterId = column[Int]("batter")
  def swing = column[Option[Int]]("swing")
  def diff = column[Option[Int]]("diff")
  def result = column[String]("result")
  def runsScored = column[Int]("runs_scored")
  def scorers = column[Option[String]]("scorers")
  def outsTracked = column[Int]("outs_tracked")
  def beforeStateId = column[Int]("before_state")
  def afterStateId = column[Int]("after_state")
  def actionStamp = column[Timestamp]("action_stamp")

  def replacedBy = this.mappedForeignKeyOpt("fk_game_actions_replaced_by", _.replacedById, GameActions)(_.replacedBy, _.id, Cascade, SetNull)
  def game = this.mappedForeignKey("fk_game_actions_game", _.gameId, Games)(_.game, _.id, Cascade, Cascade)
  def pitcher = this.mappedForeignKey("fk_game_actions_pitcher", _.pitcherId, Players)(_.pitcher, _.id, Cascade, Cascade)
  def batter = this.mappedForeignKey("fk_game_actions_batter", _.batterId, Players)(_.batter, _.id, Cascade, Cascade)
  def beforeState = this.mappedForeignKey("fk_game_actions_before_state", _.beforeStateId, GameStates)(_.beforeState, _.id, Cascade, Cascade)
  def afterState = this.mappedForeignKey("fk_game_actions_after_state", _.afterStateId, GameStates)(_.afterState, _.id, Cascade, Cascade)

  def scoringPlay = this.mapOpt(_.id, ScoringPlays)(_.scoringPlay, _.id)

  def uniquePA = index("unique_game_actions_pa", (gameId, beforeStateId, batterId), unique = true)

  def * = (id.?, replacedById, gameId, playType, pitcherId, pitch, batterId, swing, diff, result, runsScored, scorers, outsTracked, beforeStateId, afterStateId, actionStamp) <> (GameAction.tupled, GameAction.unapply)

}

object GameActions extends TableQuery[GameActions](tag => new GameActions(tag))
