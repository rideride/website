package model.db

import model.db.json.{Jsonable, UmpireAssignmentJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class UmpireAssignment(gameId: Int, umpireId: Int) extends Jsonable[UmpireAssignment] {

  override val jsonConverter = UmpireAssignmentJsonConverter

  val game: LazyReference[Game, UmpireAssignment] = lazyReference("game")
  val umpire: LazyReference[User, UmpireAssignment] = lazyReference("umpire")

}

class UmpireAssignments(tag: Tag) extends Table[UmpireAssignment](tag, "umpire_assignments") {

  def gameId = column[Int]("game")
  def umpireId = column[Int]("umpire")

  def game = this.mappedForeignKey("umpire_assignments_game", _.gameId, Games)(_.game, _.id)
  def umpire = this.mappedForeignKey("umpire_assignments_umpire", _.umpireId, Users)(_.umpire, _.id)

  def pk = primaryKey("PRIMARY", (gameId, umpireId))

  def * = (gameId, umpireId) <> (UmpireAssignment.tupled, UmpireAssignment.unapply)

}

object UmpireAssignments extends TableQuery[UmpireAssignments](tag => new UmpireAssignments(tag))
