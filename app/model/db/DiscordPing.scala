package model.db

import java.sql.Timestamp

import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class DiscordPing(id: Option[Int] = None, pingerId: Int, receiverId: Int, gameId: Int, gameStateId: Int, stamp: Timestamp) {

  val pinger: LazyReference[User, DiscordPing] = lazyReference("pinger")
  val receiver: LazyReference[User, DiscordPing] = lazyReference("receiver")
  val game: LazyReference[Game, DiscordPing] = lazyReference("game")
  val gameState: LazyReference[GameState, DiscordPing] = lazyReference("gameState")

}

class DiscordPings(tag: Tag) extends Table[DiscordPing](tag, "discord_pings") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def pingerId = column[Int]("pinger")
  def receiverId = column[Int]("receiver")
  def gameId = column[Int]("game")
  def gameStateId = column[Int]("game_state")
  def stamp = column[Timestamp]("stamp")

  def pinger = this.mappedForeignKey[Users]("fk_discord_pings_pinger", _.pingerId, Users)(_.pinger, _.id)
  def receiver = this.mappedForeignKey[Users]("fk_discord_pings_receiver", _.receiverId, Users)(_.receiver, _.id)
  def game = this.mappedForeignKey("fk_discord_pings_game", _.gameId, Games)(_.game, _.id)
  def gameState = this.mappedForeignKey("fk_discord_pings_game_state", _.gameStateId, GameStates)(_.gameState, _.id, onUpdate = Cascade, onDelete = Cascade)

  def * = (id.?, pingerId, receiverId, gameId, gameStateId, stamp) <> (DiscordPing.tupled, DiscordPing.unapply)

}

object DiscordPings extends TableQuery[DiscordPings](tag => new DiscordPings(tag))
