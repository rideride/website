package model.db

import model.db.json.{Jsonable, TeamJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class Team(id: Option[Int] = None, tag: String, name: String, parkId: Int, discordRole: Option[String], colorDiscord: Int, colorRoster: Int, colorRosterBG: Int, visible: Boolean = true, allPlayers: Boolean = false, webhook: Option[String] = None, logoURL: Option[String] = None, divisionId: Option[Int] = None) extends Jsonable[Team] {

  override val jsonConverter = TeamJsonConverter

  val park: LazyReference[Park, Team] = lazyReference("park")
  val division: LazyReference[Option[Division], Team] = lazyReference("division")

  val gmAssignment: LazyReference[Option[GMAssignment], Team] = lazyReference("gmAssignment")
  val tradeBlockNote: LazyReference[Option[TeamTradeBlockNote], Team] = lazyReference("tradeBlockNote")
  val tradeBlockEntries: LazyReference[Seq[TradeBlockEntry], Team] = lazyReference("tradeBlockEntries")
  val games: LazyReference[Seq[Game], Team] = lazyReference("games")
  val players: LazyReference[Seq[Player], Team] = lazyReference("players")

  lazy val records: Map[Int, (Int, Int)] = games.filter(g => g.statsCalculated && g.completed)
    .groupBy(_.season)
    .mapValues(games =>
      games.foldLeft((0,0))((record, game) =>
        if (game.winningTeamId == id) (record._1 + 1, record._2) else (record._1, record._2 + 1)
      )
    )

}

class Teams(tableTag: Tag) extends Table[Team](tableTag, "teams") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def tag = column[String]("tag")
  def name = column[String]("name")
  def parkId = column[Int]("park")
  def discordRole = column[Option[String]]("discord_role")
  def colorDiscord = column[Int]("color_discord")
  def colorRoster = column[Int]("color_roster")
  def colorRosterBG = column[Int]("color_roster_bg")
  def visible = column[Boolean]("visible")
  def allPlayers = column[Boolean]("all_players")
  def webhook = column[Option[String]]("webhook")
  def logoURL = column[Option[String]]("logo_url")
  def divisionId = column[Option[Int]]("division")

  def park = this.mappedForeignKey("fk_team_park", _.parkId, Parks)(_.park, _.id)
  def division = this.mappedForeignKeyOpt("fk_teams_division", _.divisionId, Divisions)(_.division, _.id)

  def gmAssignment = this.mapOpt(_.id, GMAssignments.filter(_.endSeason.isEmpty).withMap(_.gm))(_.gmAssignment, _._1.teamId)
  def tradeBlockNote = this.mapOpt(_.id, TeamTradeBlockNotes)(_.tradeBlockNote, _.id)
  def tradeBlockEntries = this.mapSeq(t => TradeBlockEntries.withMap(_.player.nest(_.battingType).nestOpt(_.pitchingType).nestOpt(_.pitchingBonus)).mapFilter(_._2._1._1._1.teamId === t.id))(_.tradeBlockEntries)
  def games = this.mapSeq(t => Games.filter(g => g.awayTeamId === t.id || g.homeTeamId === t.id))(_.games)
  def players = this.mapSeq(t => Players.filter(p => (p.teamId === t.id || t.allPlayers) && !p.deactivated))(_.players)

  def uniqueTag = index("tag", tag, unique = true)
  def uniqueName = index("name", name, unique = true)

  def * = (id.?, tag, name, parkId, discordRole, colorDiscord, colorRoster, colorRosterBG, visible, allPlayers, webhook, logoURL, divisionId) <> (Team.tupled, Team.unapply)

}

object Teams extends TableQuery[Teams](tag => new Teams(tag))
