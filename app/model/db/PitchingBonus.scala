package model.db

import model.db.json.{Jsonable, PitchingBonusJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class PitchingBonus(id: Option[Int] = None, shortcode: String, name: String, rangeHR: Int, range3B: Int, range2B: Int, range1B: Int, rangeBB: Int, rangeFO: Int, rangeK: Int, rangePO: Int, rangeRGO: Int, rangeLGO: Int) extends Jsonable[PitchingBonus] {

  override val jsonConverter = PitchingBonusJsonConverter

  val players: LazyReference[Seq[Player], PitchingBonus] = lazyReference("players")

}

class PitchingBonuses(tag: Tag) extends Table[PitchingBonus](tag, "pitching_bonuses") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def shortcode = column[String]("shortcode")
  def name = column[String]("name")
  def rangeHR = column[Int]("range_hr")
  def range3B = column[Int]("range_3b")
  def range2B = column[Int]("range_2b")
  def range1B = column[Int]("range_1b")
  def rangeBB = column[Int]("range_bb")
  def rangeFO = column[Int]("range_fo")
  def rangeK = column[Int]("range_k")
  def rangePO = column[Int]("range_po")
  def rangeRGO = column[Int]("range_rgo")
  def rangeLGO = column[Int]("range_lgo")

  def uniqueShortcode = index("shortcode", shortcode, unique = true)
  def uniqueName = index("name", name, unique = true)

  def players = this.mapSeq(pb => Players.mapFilter(_.pitchingTypeId === pb.id))(_.players)

  def * = (id.?, shortcode, name, rangeHR, range3B, range2B, range1B, rangeBB, rangeFO, rangeK, rangePO, rangeRGO, rangeLGO) <> (PitchingBonus.tupled, PitchingBonus.unapply)

}

object PitchingBonuses extends TableQuery[PitchingBonuses](tag => new PitchingBonuses(tag))
