package model.db

import model.db.util._

import slick.jdbc.MySQLProfile.api._

case class Division(id: Option[Int] = None, name: String, tag: String, leagueId: Option[Int]) {

  val league: LazyReference[Option[League], Division] = lazyReference("league")

  val teams: LazyReference[Seq[Team], Division] = lazyReference("teams")

  override def equals(obj: Any): Boolean = obj match {
    case other: Division => other.id == this.id
    case _ => false
  }

}

class Divisions(tableTag: Tag) extends Table[Division](tableTag, "divisions") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def tag = column[String]("tag")
  def leagueId = column[Option[Int]]("league")

  def league = this.mappedForeignKeyOpt("fk_divisions_league", _.leagueId, Leagues)(_.league, _.id.?)

  def * = (id.?, name, tag, leagueId) <> (Division.tupled, Division.unapply)

}

object Divisions extends TableQuery[Divisions](tag => new Divisions(tag))
