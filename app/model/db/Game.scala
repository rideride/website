package model.db

import model.db.json.{GameJsonConverter, Jsonable}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class Game(id: Option[Int] = None, season: Int, session: Int, awayTeamId: Int, homeTeamId: Int, id36: Option[String] = None, threadOwnerId: Option[Int] = None, completed: Boolean = false, winningTeamId: Option[Int] = None, awayLineupId: Option[Int] = None, homeLineupId: Option[Int] = None, statsCalculated: Boolean = true, stateId: Int, parkId: Int) extends Jsonable[Game] {

  override val jsonConverter = GameJsonConverter

  val awayTeam: LazyReference[Team, Game] = lazyReference("awayTeam")
  val homeTeam: LazyReference[Team, Game] = lazyReference("homeTeam")
  val threadOwner: LazyReference[Option[User], Game] = lazyReference("threadOwner")
  val state: LazyReference[GameState, Game] = lazyReference("state")
  val park: LazyReference[Park, Game] = lazyReference("park")
  val awayLineup: LazyReference[Option[Lineup], Game] = lazyReference("awayLineup")
  val homeLineup: LazyReference[Option[Lineup], Game] = lazyReference("homeLineup")

  val awards: LazyReference[Option[GameAward], Game] = lazyReference("awards")
  val actions: LazyReference[Seq[GameAction], Game] = lazyReference("actions")
  val scoringPlays: LazyReference[Seq[ScoringPlay], Game] = lazyReference("scoringPlays")
  val umpireAssignments: LazyReference[Seq[UmpireAssignment], Game] = lazyReference("umpires")

  lazy val winningTeam: Option[Team] = winningTeamId.map(id => if (id == awayTeamId) awayTeam else homeTeam)
  lazy val battingLineup: Option[Lineup] = if (state.inning % 2 == 1) awayLineup else homeLineup
  lazy val fieldingLineup: Option[Lineup] = if (state.inning % 2 == 1) homeLineup else awayLineup
  lazy val name: String = s"$season.$session ${awayTeam.tag} @ ${homeTeam.tag}"
  lazy val lastAction: Option[GameAction] = actions.sortBy(_.id).lastOption

}

class Games(tag: Tag) extends Table[Game](tag, "games") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def season = column[Int]("season")
  def session = column[Int]("session")
  def awayTeamId = column[Int]("away_team")
  def homeTeamId = column[Int]("home_team")
  def id36 = column[Option[String]]("id36")
  def threadOwnerId = column[Option[Int]]("thread_owner")
  def completed = column[Boolean]("completed")
  def winningTeamId = column[Option[Int]]("winning_team")
  def awayLineupId = column[Option[Int]]("away_lineup")
  def homeLineupId = column[Option[Int]]("home_lineup")
  def statsCalculated = column[Boolean]("stats_calculated")
  def stateId = column[Int]("state")
  def parkId = column[Int]("park")

  def awayTeam = this.mappedForeignKey("fk_game_away_team", _.awayTeamId, Teams)(_.awayTeam, _.id)
  def homeTeam = this.mappedForeignKey("fk_game_home_team", _.homeTeamId, Teams)(_.homeTeam, _.id)
  def threadOwner = this.mappedForeignKeyOpt("fk_games_thread_owner", _.threadOwnerId, Users)(_.threadOwner, _.id)
  def winningTeam = foreignKey("fk_games_winning_team", winningTeamId, Teams)(_.id)
  def awayLineup = this.mappedForeignKeyOpt("game_away_lineup", _.awayLineupId, Lineups)(_.awayLineup, _.id)
  def homeLineup = this.mappedForeignKeyOpt("game_home_lineup", _.homeLineupId, Lineups)(_.homeLineup, _.id)
  def state = this.mappedForeignKey("fk_games_state", _.stateId, GameStates)(_.state, _.id)
  def park = this.mappedForeignKey("fk_games_park", _.parkId, Parks)(_.park, _.id)

  def awards = this.mapOpt(_.id, GameAwards)(_.awards, _.id)
  def actions = this.mapSeq(g => GameActions.mapFilter(_.gameId === g.id))(_.actions)
  def scoringPlays = this.mapSeq(g => ScoringPlays.mapFilter(_.gameId === g.id))(_.scoringPlays)
  def umpireAssignments = this.mapSeq(g => UmpireAssignments.mapFilter(_.gameId === g.id))(_.umpireAssignments)

  def uniqueSeason = index("season", (season, session, awayTeamId), unique = true)
  def uniqueSeason2 = index("season_2", (season, session, homeTeamId), unique = true)

  def * = (id.?, season, session, awayTeamId, homeTeamId, id36, threadOwnerId, completed, winningTeamId, awayLineupId, homeLineupId, statsCalculated, stateId, parkId) <> (Game.tupled, Game.unapply)

}

object Games extends TableQuery[Games](tag => new Games(tag))
