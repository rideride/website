package model.db.util

import slick.jdbc.MySQLProfile.api._

trait Identified {

  def id: Rep[Int]

}
