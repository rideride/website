package model.db.util

import slick.jdbc.MySQLProfile.api._
import slick.ast.{AnonSymbol, Node}
import slick.lifted.{AbstractTable, ForeignKey, ForeignKeyQuery, OptionLift, ShapedValue}

trait QueryMapping[SourceTable <: AbstractTable[_], TargetTable <: AbstractTable[_], TargetQueryStructure, TargetObjectStructure] {
  private[db] val sourceSelector: SourceTable => Rep[Int]
  private[db] val transformationSelector: SourceTable#TableElementType => LazyReference[TargetTable#TableElementType, _]
  private[db] val targetQuery: MappedQuery[TargetTable, TargetQueryStructure, TargetObjectStructure]
  private[db] val targetSelector: TargetQueryStructure => Rep[Int]

  def nest[OTargetTable <: AbstractTable[_], OTargetQueryStructure, OTargetObjectStructure](mapSelector: TargetTable => QueryMapping[TargetTable, OTargetTable, OTargetQueryStructure, OTargetObjectStructure]): QueryMapping[SourceTable, TargetTable, (TargetQueryStructure, OTargetQueryStructure), (TargetObjectStructure, OTargetObjectStructure)] =
    QueryMappingImpl(sourceSelector, transformationSelector, targetQuery.withMap(mapSelector), t => targetSelector(t._1))
  def nestOpt[OTargetTable <: AbstractTable[_], OTargetQueryStructure, OTargetObjectStructure](mapSelector: TargetTable => QueryMappingOpt[TargetTable, OTargetTable, OTargetQueryStructure, OTargetObjectStructure])(implicit ol: OptionLift[OTargetQueryStructure, Rep[Option[OTargetQueryStructure]]], shape: Shape[FlatShapeLevel, Rep[Option[OTargetQueryStructure]], Option[OTargetObjectStructure], _]): QueryMapping[SourceTable, TargetTable, (TargetQueryStructure, Rep[Option[OTargetQueryStructure]]), (TargetObjectStructure, Option[OTargetObjectStructure])] =
    QueryMappingImpl(sourceSelector, transformationSelector, targetQuery.withMapOpt(mapSelector), t => targetSelector(t._1))
  def nestSeq[OTargetTable <: AbstractTable[_], OTargetQueryStructure, OTargetObjectStructure](mapSelector: TargetTable => QueryMappingSeq[TargetTable, OTargetTable, OTargetQueryStructure, OTargetObjectStructure]): QueryMapping[SourceTable, TargetTable, TargetQueryStructure, TargetObjectStructure] =
    QueryMappingImpl(sourceSelector, transformationSelector, targetQuery.withMapSeq(mapSelector), targetSelector)
}

trait QueryMappingOpt[SourceTable <: AbstractTable[_], TargetTable <: AbstractTable[_], TargetQueryStructure, TargetObjectStructure] {
  private[db] val sourceSelector: SourceTable => Rep[Option[Int]]
  private[db] val transformationSelector: SourceTable#TableElementType => LazyReference[Option[TargetTable#TableElementType], _]
  private[db] val targetQuery: MappedQuery[TargetTable, TargetQueryStructure, TargetObjectStructure]
  private[db] val targetSelector: TargetQueryStructure => Rep[Option[Int]]

  def nest[OTargetTable <: AbstractTable[_], OTargetQueryStructure, OTargetObjectStructure](mapSelector: TargetTable => QueryMapping[TargetTable, OTargetTable, OTargetQueryStructure, OTargetObjectStructure]): QueryMappingOpt[SourceTable, TargetTable, (TargetQueryStructure, OTargetQueryStructure), (TargetObjectStructure, OTargetObjectStructure)] =
    QueryMappingOptImpl(sourceSelector, transformationSelector, targetQuery.withMap(mapSelector), t => targetSelector(t._1))
  def nestOpt[OTargetTable <: AbstractTable[_], OTargetQueryStructure, OTargetObjectStructure](mapSelector: TargetTable => QueryMappingOpt[TargetTable, OTargetTable, OTargetQueryStructure, OTargetObjectStructure])(implicit ol: OptionLift[OTargetQueryStructure, Rep[Option[OTargetQueryStructure]]], shape: Shape[FlatShapeLevel, Rep[Option[OTargetQueryStructure]], Option[OTargetObjectStructure], _]): QueryMappingOpt[SourceTable, TargetTable, (TargetQueryStructure, Rep[Option[OTargetQueryStructure]]), (TargetObjectStructure, Option[OTargetObjectStructure])] =
    QueryMappingOptImpl(sourceSelector, transformationSelector, targetQuery.withMapOpt(mapSelector), t => targetSelector(t._1))
  def nestSeq[OTargetTable <: AbstractTable[_], OTargetQueryStructure, OTargetObjectStructure](mapSelector: TargetTable => QueryMappingSeq[TargetTable, OTargetTable, OTargetQueryStructure, OTargetObjectStructure]): QueryMappingOpt[SourceTable, TargetTable, TargetQueryStructure, TargetObjectStructure] =
    QueryMappingOptImpl(sourceSelector, transformationSelector, targetQuery.withMapSeq(mapSelector), targetSelector)
}

trait QueryMappingSeq[SourceTable <: AbstractTable[_], TargetTable <: AbstractTable[_], TargetQueryStructure, TargetObjectStructure] {
  private[db] val transformationSelector: SourceTable#TableElementType => LazyReference[Seq[TargetTable#TableElementType], _]
  private[db] val targetQuery: SourceTable#TableElementType => MappedQuery[TargetTable, TargetQueryStructure, TargetObjectStructure]

  def nest[OTargetTable <: AbstractTable[_], OTargetQueryStructure, OTargetObjectStructure](mapSelector: TargetTable => QueryMapping[TargetTable, OTargetTable, OTargetQueryStructure, OTargetObjectStructure]): QueryMappingSeq[SourceTable, TargetTable, (TargetQueryStructure, OTargetQueryStructure), (TargetObjectStructure, OTargetObjectStructure)] =
    QueryMappingSeqImpl(transformationSelector, targetQuery(_).withMap(mapSelector))
  def nestOpt[OTargetTable <: AbstractTable[_], OTargetQueryStructure, OTargetObjectStructure](mapSelector: TargetTable => QueryMappingOpt[TargetTable, OTargetTable, OTargetQueryStructure, OTargetObjectStructure])(implicit ol: OptionLift[OTargetQueryStructure, Rep[Option[OTargetQueryStructure]]], shape: Shape[FlatShapeLevel, Rep[Option[OTargetQueryStructure]], Option[OTargetObjectStructure], _]): QueryMappingSeq[SourceTable, TargetTable, (TargetQueryStructure, Rep[Option[OTargetQueryStructure]]), (TargetObjectStructure, Option[OTargetObjectStructure])] =
    QueryMappingSeqImpl(transformationSelector, targetQuery(_).withMapOpt(mapSelector))
  def nestSeq[OTargetTable <: AbstractTable[_], OTargetQueryStructure, OTargetObjectStructure](mapSelector: TargetTable => QueryMappingSeq[TargetTable, OTargetTable, OTargetQueryStructure, OTargetObjectStructure]): QueryMappingSeq[SourceTable, TargetTable, TargetQueryStructure, TargetObjectStructure] =
    QueryMappingSeqImpl(transformationSelector, targetQuery(_).withMapSeq(mapSelector))
}

case class QueryMappingImpl[SourceTable <: AbstractTable[_], TargetTable <: AbstractTable[_], TargetQueryStructure, TargetObjectStructure](private[db] val sourceSelector: SourceTable => Rep[Int], private[db] val transformationSelector: SourceTable#TableElementType => LazyReference[TargetTable#TableElementType, _], private[db] val targetQuery: MappedQuery[TargetTable, TargetQueryStructure, TargetObjectStructure], private[db] val targetSelector: TargetQueryStructure => Rep[Int]) extends QueryMapping[SourceTable, TargetTable, TargetQueryStructure, TargetObjectStructure]
case class QueryMappingOptImpl[SourceTable <: AbstractTable[_], TargetTable <: AbstractTable[_], TargetQueryStructure, TargetObjectStructure](private[db] val sourceSelector: SourceTable => Rep[Option[Int]], private[db] val transformationSelector: SourceTable#TableElementType => LazyReference[Option[TargetTable#TableElementType], _], private[db] val targetQuery: MappedQuery[TargetTable, TargetQueryStructure, TargetObjectStructure], private[db] val targetSelector: TargetQueryStructure => Rep[Option[Int]]) extends QueryMappingOpt[SourceTable, TargetTable, TargetQueryStructure, TargetObjectStructure]
case class QueryMappingSeqImpl[SourceTable <: AbstractTable[_], TargetTable <: AbstractTable[_], TargetQueryStructure, TargetObjectStructure](private[db] val transformationSelector: SourceTable#TableElementType => LazyReference[Seq[TargetTable#TableElementType], _], private[db] val targetQuery: SourceTable#TableElementType => MappedQuery[TargetTable, TargetQueryStructure, TargetObjectStructure]) extends QueryMappingSeq[SourceTable, TargetTable, TargetQueryStructure, TargetObjectStructure]

case class MappedForeignKey[STable <: AbstractTable[_], TTable <: AbstractTable[_]](private[db] val sourceSelector: STable => Rep[Int], private[db] val transformationSelector: STable#TableElementType => LazyReference[TTable#TableElementType, _], private[db] val targetTable: TableQuery[TTable], private[db] val targetSelector: TTable => Rep[Int])(nodeDelegate: Node, base: ShapedValue[_ <: TTable, TTable#TableElementType], fks: IndexedSeq[ForeignKey], generator: AnonSymbol, aliasedValue: TTable) extends ForeignKeyQuery[TTable, TTable#TableElementType](nodeDelegate, base, fks, targetTable, generator, aliasedValue) with QueryMapping[STable, TTable, TTable, TTable#TableElementType] {
  override private[db] val targetQuery = targetTable
}

case class MappedForeignKeyOpt[STable <: AbstractTable[_], TTable <: AbstractTable[_]](private[db] val sourceSelector: STable => Rep[Option[Int]], private[db] val transformationSelector: STable#TableElementType => LazyReference[Option[TTable#TableElementType], _], private[db] val targetTable: TableQuery[TTable], private[db] val targetSelector: TTable => Rep[Option[Int]])(nodeDelegate: Node, base: ShapedValue[_ <: TTable, TTable#TableElementType], fks: IndexedSeq[ForeignKey], generator: AnonSymbol, aliasedValue: TTable) extends ForeignKeyQuery[TTable, TTable#TableElementType](nodeDelegate, base, fks, targetTable, generator, aliasedValue) with QueryMappingOpt[STable, TTable, TTable, TTable#TableElementType] {
  override private[db] val targetQuery = targetTable
}
