package model.db.util

import model.FBDatabaseProvider
import slick.jdbc.MySQLProfile.api._
import slick.lifted.{AbstractTable, CanBeQueryCondition, OptionLift, Query}

class MappedQuery[TableRoot <: AbstractTable[_], QueryStructure, ObjectStructure](private[db] val query: Query[QueryStructure, ObjectStructure, Seq])(private val transformation: (ObjectStructure, FBDatabaseProvider) => TableRoot#TableElementType)(private[db] val unpacker: QueryStructure => TableRoot) {
  def wrapJoin[OTableRoot <: AbstractTable[_], OQueryStructure, OObjectStructure](rightQuery: MappedQuery[OTableRoot, OQueryStructure, OObjectStructure])(predicate: (QueryStructure, OQueryStructure) => Rep[Boolean])(rightTransform: ((TableRoot#TableElementType, OTableRoot#TableElementType), FBDatabaseProvider) => TableRoot#TableElementType) =
    new MappedQuery[TableRoot, (QueryStructure, OQueryStructure), (ObjectStructure, OObjectStructure)](query.join(rightQuery.query).on(predicate))((tuple: (ObjectStructure, OObjectStructure), db) => rightTransform((this.transformation(tuple._1, db), rightQuery.transformation(tuple._2, db)), db))(tuple => unpacker(tuple._1))
  def wrapJoinLeft[OTableRoot <: AbstractTable[_], OQueryStructure, OObjectStructure](rightQuery: MappedQuery[OTableRoot, OQueryStructure, OObjectStructure])(predicate: (QueryStructure, OQueryStructure) => Rep[Option[Boolean]])(rightTransform: ((TableRoot#TableElementType, Option[OTableRoot#TableElementType]), FBDatabaseProvider) => TableRoot#TableElementType)(implicit ol: OptionLift[OQueryStructure, Rep[Option[OQueryStructure]]], shape: Shape[FlatShapeLevel, Rep[Option[OQueryStructure]], Option[OObjectStructure], _]) =
    new MappedQuery[TableRoot, (QueryStructure, Rep[Option[OQueryStructure]]), (ObjectStructure, Option[OObjectStructure])](query.joinLeft(rightQuery.query).on(predicate))((tuple: (ObjectStructure, Option[OObjectStructure]), db) => rightTransform((this.transformation(tuple._1, db), tuple._2.map(rightQuery.transformation(_, db))), db))(tuple => unpacker(tuple._1))
  def wrapSeq[OTableRoot <: AbstractTable[_], OQueryStructure, OObjectStructure](rightQuery: TableRoot#TableElementType => MappedQuery[OTableRoot, OQueryStructure, OObjectStructure])(rightTransform: ((TableRoot#TableElementType, Seq[OTableRoot#TableElementType]), FBDatabaseProvider) => TableRoot#TableElementType) =
    new MappedQuery[TableRoot, QueryStructure, ObjectStructure](query)((rootObject: ObjectStructure, db) => { val outer = transformation(rootObject, db); rightTransform((outer, rightQuery(outer).seq(db)), db) })(unpacker)

  def withMap[OTableRoot <: AbstractTable[_], OQueryStructure, OObjectStructure](mapSelector: TableRoot => QueryMapping[TableRoot, OTableRoot, OQueryStructure, OObjectStructure]): MappedQuery[TableRoot, (QueryStructure, OQueryStructure), (ObjectStructure, OObjectStructure)] = {
    val map = mapSelector(unpacker(query.shaped.value))
    wrapJoin(map.targetQuery)((source, target) => map.sourceSelector(unpacker(source)) === map.targetSelector(target))(lazyMap(map.transformationSelector))
  }
  def withMapOpt[OTableRoot <: AbstractTable[_], OQueryStructure, OObjectStructure](mapSelector: TableRoot => QueryMappingOpt[TableRoot, OTableRoot, OQueryStructure, OObjectStructure])(implicit ol: OptionLift[OQueryStructure, Rep[Option[OQueryStructure]]], shape: Shape[FlatShapeLevel, Rep[Option[OQueryStructure]], Option[OObjectStructure], _]): MappedQuery[TableRoot, (QueryStructure, Rep[Option[OQueryStructure]]), (ObjectStructure, Option[OObjectStructure])] = {
    val map = mapSelector(unpacker(query.shaped.value))
    wrapJoinLeft(map.targetQuery)((source, target) => map.sourceSelector(unpacker(source)) === map.targetSelector(target))(lazyMap(map.transformationSelector))
  }
  def withMapSeq[OTableRoot <: AbstractTable[_], OQueryStructure, OObjectStructure](mapSelector: TableRoot => QueryMappingSeq[TableRoot, OTableRoot, OQueryStructure, OObjectStructure]): MappedQuery[TableRoot, QueryStructure, ObjectStructure] = {
    val map = mapSelector(unpacker(query.shaped.value))
    wrapSeq(map.targetQuery)(lazyMap(map.transformationSelector))
  }

  def mapFilter[P <: Rep[_]](predicate: QueryStructure => P)(implicit wt: CanBeQueryCondition[P]) = new MappedQuery(query.filter(predicate))(transformation)(unpacker)
  def mapFilterNot[P <: Rep[_]](predicate: QueryStructure => P)(implicit wt: CanBeQueryCondition[P]) = new MappedQuery(query.filterNot(predicate))(transformation)(unpacker)

  def seq(implicit db: FBDatabaseProvider): Seq[TableRoot#TableElementType] = query.result.run.map(t => transformation(t, db))
  def single(implicit db: FBDatabaseProvider): TableRoot#TableElementType = transformation(query.singleQ, db)
  def singleOpt(implicit db: FBDatabaseProvider): Option[TableRoot#TableElementType] = query.singleQOpt.map(t => transformation(t, db))
}
