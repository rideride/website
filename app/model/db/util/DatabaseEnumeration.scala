package model.db.util

import slick.jdbc.MySQLProfile.api._

trait DatabaseEnumeration extends Enumeration {

  implicit val ColumnMapping = MappedColumnType.base[Value, Int](
    _.id,
    apply
  )

}
