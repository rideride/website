package model.db

import java.util.concurrent.TimeUnit

import model.FBDatabaseProvider
import model.db.json.Jsonable
import play.api.libs.json.Json
import slick.ast.{AnonSymbol, Filter, Library, Ref}
import slick.dbio.{DBIOAction, NoStream}
import slick.jdbc.MySQLProfile.api._
import slick.lifted.{AbstractTable, ForeignKey, ShapedValue}

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.language.implicitConversions
import scala.reflect.ClassTag

package object util {

  implicit def query2MappedQuery[TR <: AbstractTable[_]](query: Query[TR, TR#TableElementType, Seq]): MappedQuery[TR, TR, TR#TableElementType] = query.wrapped

  implicit class WrappableQueryExtensions[TR <: AbstractTable[_]](query: Query[TR, TR#TableElementType, Seq]) {
    def wrapped = new MappedQuery[TR, TR, TR#TableElementType](query)((tup, _) => tup)(identity)
  }

  implicit class QueryExtensions[TR, RR](query: Query[TR, RR, Seq]) {
    def seqQ(implicit db: FBDatabaseProvider): Seq[RR] = query.result.run
    def singleQ(implicit db: FBDatabaseProvider): RR = query.take(1).result.head.run
    def singleQOpt(implicit db: FBDatabaseProvider): Option[RR] = query.take(1).result.headOption.run
  }

  implicit class ActionExtensions[R](action: DBIOAction[R, NoStream, Nothing]) {
    def run(implicit db: FBDatabaseProvider): R = db.await(action)
  }

  implicit class TableExtensions[STable <: AbstractTable[_]](table: STable) {
    def mappedForeignKey[TTable <: AbstractTable[_]](name: String, sourceSelector: STable => Rep[Int], target: TableQuery[TTable])(transformationSelector: STable#TableElementType => LazyReference[TTable#TableElementType, _], targetSelector: TTable => Rep[Int], onUpdate: ForeignKeyAction = ForeignKeyAction.NoAction, onDelete: ForeignKeyAction = ForeignKeyAction.NoAction)(implicit unpack: Shape[_ <: FlatShapeLevel, TTable, TTable#TableElementType, _], unpackp: Shape[_ <: FlatShapeLevel, Rep[Int], _, _]): MappedForeignKey[STable, TTable] = {
      val q = target.asInstanceOf[Query[TTable, TTable#TableElementType, Seq]]
      val generator = new AnonSymbol
      val aliased = q.shaped.encodeRef(Ref(generator))
      val fv = Library.==.typed[Boolean](unpackp.toNode(targetSelector(aliased.value)), unpackp.toNode(sourceSelector(table)))
      val fk = ForeignKey(name, table.toNode, q.shaped.asInstanceOf[ShapedValue[TTable, _]], target.shaped.value, unpackp, sourceSelector(table), targetSelector, onUpdate, onDelete)
      MappedForeignKey(sourceSelector, transformationSelector, target, targetSelector)(Filter.ifRefutable(generator, q.toNode, fv), q.shaped, IndexedSeq(fk), generator, aliased.value)
    }
    def mappedForeignKeyOpt[TTable <: AbstractTable[_]](name: String, sourceSelector: STable => Rep[Option[Int]], target: TableQuery[TTable])(transformationSelector: STable#TableElementType => LazyReference[Option[TTable#TableElementType], _], targetSelector: TTable => Rep[Option[Int]], onUpdate: ForeignKeyAction = ForeignKeyAction.NoAction, onDelete: ForeignKeyAction = ForeignKeyAction.NoAction)(implicit unpack: Shape[_ <: FlatShapeLevel, TTable, TTable#TableElementType, _], unpackp: Shape[_ <: FlatShapeLevel, Rep[Option[Int]], _, _]): MappedForeignKeyOpt[STable, TTable] = {
      val q = target.asInstanceOf[Query[TTable, TTable#TableElementType, Seq]]
      val generator = new AnonSymbol
      val aliased = q.shaped.encodeRef(Ref(generator))
      val fv = Library.==.typed[Boolean](unpackp.toNode(targetSelector(aliased.value)), unpackp.toNode(sourceSelector(table)))
      val fk = ForeignKey(name, table.toNode, q.shaped.asInstanceOf[ShapedValue[TTable, _]], target.shaped.value, unpackp, sourceSelector(table), targetSelector, onUpdate, onDelete)
      MappedForeignKeyOpt(sourceSelector, transformationSelector, target, targetSelector)(Filter.ifRefutable(generator, q.toNode, fv), q.shaped, IndexedSeq(fk), generator, aliased.value)
    }
    def map[SourceTable <: AbstractTable[_], TargetTable <: AbstractTable[_], TargetQueryStructure, TargetObjectStructure](sourceSelector: STable => Rep[Int], target: MappedQuery[TargetTable, TargetQueryStructure, TargetObjectStructure])(transformationSelector: STable#TableElementType => LazyReference[TargetTable#TableElementType, _], targetSelector: TargetQueryStructure => Rep[Int]): QueryMapping[STable, TargetTable, TargetQueryStructure, TargetObjectStructure] =
      QueryMappingImpl(sourceSelector, transformationSelector, target, targetSelector)
    def mapOpt[SourceTable <: AbstractTable[_], TargetTable <: AbstractTable[_], TargetQueryStructure, TargetObjectStructure](sourceSelector: STable => Rep[Option[Int]], target: MappedQuery[TargetTable, TargetQueryStructure, TargetObjectStructure])(transformationSelector: STable#TableElementType => LazyReference[Option[TargetTable#TableElementType], _], targetSelector: TargetQueryStructure => Rep[Option[Int]]): QueryMappingOpt[STable, TargetTable, TargetQueryStructure, TargetObjectStructure] =
      QueryMappingOptImpl(sourceSelector, transformationSelector, target, targetSelector)
    def mapSeq[SourceTable <: AbstractTable[_], TargetTable <: AbstractTable[_], TargetQueryStructure, TargetObjectStructure](target: STable#TableElementType => MappedQuery[TargetTable, TargetQueryStructure, TargetObjectStructure])(transformationSelector: STable#TableElementType => LazyReference[Seq[TargetTable#TableElementType], _]): QueryMappingSeq[STable, TargetTable, TargetQueryStructure, TargetObjectStructure] =
      QueryMappingSeqImpl(transformationSelector, target)
  }

  def lazyReference[T, C](name: String)(implicit tag: ClassTag[C]): LazyReference[T, C] = LazyReference[T, C](name = name)

  case class LazyReference[T, C](private[db] var _value: Option[T] = None, private val name: String = "unknown_lazy")(implicit tag: ClassTag[C]) {
    def value: T = if (_value.isDefined) _value.get else throw new NoSuchFieldException(s"${tag.runtimeClass.getCanonicalName}.$name was not loaded in this query")
    private[db] def value_=(value: T): Unit = _value = Some(value)
  }

  implicit class LazyReferenceJsonableExt[T <: Jsonable[_]](ref: LazyReference[T, _]) {
    def json: Json.JsValueWrapper = Json.toJsFieldJsValueWrapper(ref._value.map(_.toJson))
    def json(fallback: Int): Json.JsValueWrapper = ref._value.map(v => Json.toJsFieldJsValueWrapper(v.toJson)).getOrElse(fallback)
  }

  implicit class LazyReferenceOptJsonableExt[T <: Jsonable[_]](ref: LazyReference[Option[T], _]) {
    def jsonOpt(fallback: Option[Int] = None): Json.JsValueWrapper = ref._value.map(v => Json.toJsFieldJsValueWrapper(v.map(_.toJson))).getOrElse(Json.toJsFieldJsValueWrapper(fallback))
  }

  implicit def lazyReference2Reference[T](ref: LazyReference[T, _]): T = ref.value

  private[db] implicit class TapExt[A](any: A) {
    def tap(body: A => Unit): A = {
      body(any)
      any
    }
  }

  def lazyMap[A, B](selector: A => LazyReference[B, _]): ((A, B), FBDatabaseProvider) => A = (tuple, _) => tuple._1.tap(t => selector(t)._value = Some(tuple._2))

  implicit class IdentifiedExt[TR <: AbstractTable[_] with Identified](table: TableQuery[TR]) {
    def findById(id: Int)(implicit db: FBDatabaseProvider): Option[TR#TableElementType] = table.filter(_.id === id).singleOpt
  }

  implicit class IdentifiedWrappedQueryExt[TR <: AbstractTable[_] with Identified, TO, RO](wrapped: MappedQuery[TR, TO, RO]) {
    def findById(id: Int)(implicit db: FBDatabaseProvider): Option[TR#TableElementType] = wrapped.mapFilter(row => wrapped.unpacker(row).id === id).singleOpt
  }

}
