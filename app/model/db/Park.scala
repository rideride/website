package model.db

import model.db.json.{Jsonable, ParkJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class Park(id: Option[Int] = None, name: String, factorHR: Double, factor3B: Double, factor2B: Double, factor1B: Double, factorBB: Double) extends Jsonable[Park] {

  override val jsonConverter = ParkJsonConverter

  lazy val slash = f"$factorHR%.3f/$factor3B%.3f/$factor2B%.3f/$factor1B%.3f/$factorBB%.3f"

  val teams: LazyReference[Seq[Team], Park] = lazyReference("teams")

}

class Parks(tag: Tag) extends Table[Park](tag, "parks") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def factorHR = column[Double]("factor_hr")
  def factor3B = column[Double]("factor_3b")
  def factor2B = column[Double]("factor_2b")
  def factor1B = column[Double]("factor_1b")
  def factorBB = column[Double]("factor_bb")

  def uniqueName = index("name", name, unique = true)

  def teams = this.mapSeq(p => Teams.filter(_.parkId === p.id))(_.teams)

  def * = (id.?, name, factorHR, factor3B, factor2B, factor1B, factorBB) <> (Park.tupled, Park.unapply)

}

object Parks extends TableQuery[Parks](tag => new Parks(tag))
