package model.db

import java.sql.Timestamp

import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class RedditPing(id: Option[Int] = None, pingerId: Int, receiverId: Int, gameId: Int, gameStateId: Int, stamp: Timestamp, result: Int, message: String, id36: Option[String]) {

  val pinger: LazyReference[User, RedditPing] = lazyReference("pinger")
  val receiver: LazyReference[User, RedditPing] = lazyReference("receiver")
  val game: LazyReference[Game, RedditPing] = lazyReference("game")
  val gameState: LazyReference[GameState, RedditPing] = lazyReference("gameState")

}

class RedditPings(tag: Tag) extends Table[RedditPing](tag, "reddit_pings") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def pingerId = column[Int]("pinger")
  def receiverId = column[Int]("receiver")
  def gameId = column[Int]("game")
  def gameStateId = column[Int]("game_state")
  def stamp = column[Timestamp]("stamp")
  def result = column[Int]("result")
  def message = column[String]("message")
  def id36 = column[Option[String]]("id36")

  def pinger = this.mappedForeignKey[Users]("fk_reddit_pings_pinger", _.pingerId, Users)(_.pinger, _.id)
  def receiver = this.mappedForeignKey[Users]("fk_reddit_pings_receiver", _.receiverId, Users)(_.receiver, _.id)
  def game = this.mappedForeignKey("fk_reddit_pings_game", _.gameId, Games)(_.game, _.id)
  def gameState = this.mappedForeignKey("fk_reddit_pings_game_state", _.gameStateId, GameStates)(_.gameState, _.id)

  def * = (id.?, pingerId, receiverId, gameId, gameStateId, stamp, result, message, id36) <> (RedditPing.tupled, RedditPing.unapply)

}

object RedditPings extends TableQuery[RedditPings](tag => new RedditPings(tag))
