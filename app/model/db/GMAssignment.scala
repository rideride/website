package model.db

import model.db.json.{GMAssignmentJsonConverter, Jsonable}
import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class GMAssignment(id: Option[Int] = None, teamId: Int, gmId: Int, startSeason: Int, startSession: Int, endSeason: Option[Int] = None, endSession: Option[Int] = None, endReason: Option[String] = None) extends Jsonable[GMAssignment] {

  override val jsonConverter = GMAssignmentJsonConverter

  val team: LazyReference[Team, GMAssignment] = lazyReference("team")
  val gm: LazyReference[Player, GMAssignment] = lazyReference("gm")

}

class GMAssignments(tag: Tag) extends Table[GMAssignment](tag, "gm_assignments") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def teamId = column[Int]("team")
  def gmId = column[Int]("gm")
  def startSeason = column[Int]("start_season")
  def startSession = column[Int]("start_session")
  def endSeason = column[Option[Int]]("end_season")
  def endSession = column[Option[Int]]("end_session")
  def endReason = column[Option[String]]("end_reason")

  def team = this.mappedForeignKey("fk_gm_assignments_team", _.teamId, Teams)(_.team, _.id)
  def gm = this.mappedForeignKey("fk_gm_assignments_gm", _.gmId, Players)(_.gm, _.id, onUpdate = Cascade)

  def * = (id.?, teamId, gmId, startSeason, startSession, endSeason, endSession, endReason) <> (GMAssignment.tupled, GMAssignment.unapply)

}

object GMAssignments extends TableQuery[GMAssignments](tag => new GMAssignments(tag))
