package model.db

import model.PlayerNamed
import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class PlayerNickname(playerId: Int, firstName: Option[String], lastName: String) extends PlayerNamed {

  val player: LazyReference[Player, PlayerNickname] = lazyReference("player")

}

class PlayerNicknames(tag: Tag) extends Table[PlayerNickname](tag, "player_nicknames") with Identified {

  val id = column[Int]("player", O.PrimaryKey)
  val firstName = column[Option[String]]("first_name")
  val lastName = column[String]("last_name")

  val player = this.mappedForeignKey("fk_player_nicknames_player", _.id, Players)(_.player, _.id, onUpdate = Cascade, onDelete = Cascade)

  def * = (id, firstName, lastName) <> (PlayerNickname.tupled, PlayerNickname.unapply)

}

object PlayerNicknames extends TableQuery[PlayerNicknames](tag => new PlayerNicknames(tag))
