package model.db.json

import model.db.BattingStat
import play.api.libs.json.{JsObject, Json}

object BattingStatJsonConverter extends JsonConverter[BattingStat] {
  override def toJson(obj: BattingStat): JsObject = Json.obj(
    "player" -> obj.player.json(obj.playerId),
    "season" -> obj.season,
    "totalPA" -> obj.totalPA,
    "totalAB" -> obj.totalAB,
    "totalHR" -> obj.totalHR,
    "total3B" -> obj.total3B,
    "total2B" -> obj.total2B,
    "total1B" -> obj.total1B,
    "totalBB" -> obj.totalBB,
    "totalFO" -> obj.totalFO,
    "totalK" -> obj.totalK,
    "totalPO" -> obj.totalPO,
    "totalRGO" -> obj.totalRGO,
    "totalLGO" -> obj.totalLGO,
    "totalRBI" -> obj.totalRBI,
    "totalR" -> obj.totalR,
    "totalSB" -> obj.totalSB,
    "totalCS" -> obj.totalCS,
    "totalDP" -> obj.totalDP,
    "totalTP" -> obj.totalTP,
    "totalGP" -> obj.totalGP,
    "totalSac" -> obj.totalSac,
    "ba" -> obj.ba,
    "obp" -> obj.obp,
    "slg" -> obj.slg,
  )
}
