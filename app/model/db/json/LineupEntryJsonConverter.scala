package model.db.json

import model.db.LineupEntry
import play.api.libs.json.{JsObject, Json}

object LineupEntryJsonConverter extends JsonConverter[LineupEntry] {

  override def toJson(obj: LineupEntry): JsObject = Json.obj(
    "id" -> obj.id.get,
    "lineup" -> obj.lineupId,
    "player" -> obj.player.json(obj.playerId),
    "position" -> obj.position,
    "battingPos" -> obj.battingPos,
    "replacedBy" -> obj.replacedById,
  )

}
