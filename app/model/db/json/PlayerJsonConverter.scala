package model.db.json

import model.db.Player
import play.api.libs.json.Json

object PlayerJsonConverter extends JsonConverter[Player] {

  override def toJson(obj: Player) = Json.obj(
    "id" -> obj.id.get,
    "user" -> obj.user.json(obj.userId),
    "name" -> obj.name,
    "firstName" -> obj.firstName,
    "lastName" -> obj.lastName,
    "team" -> obj.team.jsonOpt(obj.teamId),
    "battingType" -> obj.battingType.json(obj.battingTypeId),
    "pitchingType" -> obj.pitchingType.jsonOpt(obj.pitchingTypeId),
    "pitchingBonus" -> obj.pitchingBonus.jsonOpt(obj.pitchingBonusId),
    "rightHanded" -> obj.rightHanded,
    "positionPrimary" -> obj.positionPrimary,
    "positionSecondary" -> obj.positionSecondary,
    "positionTertiary" -> obj.positionTertiary,
    "teamAssignments" -> obj.teamAssignments._value.map(_.toJson),
    "battingStats" -> obj.battingStats._value.map(_.toJson),
    "pitchingStats" -> obj.pitchingStats._value.map(_.toJson)
  )

}
