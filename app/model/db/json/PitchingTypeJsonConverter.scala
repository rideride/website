package model.db.json

import model.db.PitchingType
import play.api.libs.json.Json

object PitchingTypeJsonConverter extends JsonConverter[PitchingType] {

  override def toJson(obj: PitchingType) = Json.obj(
    "id" -> obj.id.get,
    "shortcode" -> obj.shortcode,
    "name" -> obj.name,
    "rangeHR" -> obj.rangeHR,
    "range3B" -> obj.range3B,
    "range2B" -> obj.range2B,
    "range1B" -> obj.range1B,
    "rangeBB" -> obj.rangeBB,
    "rangeFO" -> obj.rangeFO,
    "rangeK" -> obj.rangeK,
    "rangePO" -> obj.rangePO,
    "rangeRGO" -> obj.rangeRGO,
    "rangeLGO" -> obj.rangeLGO,
  )

}
