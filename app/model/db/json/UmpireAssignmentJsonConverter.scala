package model.db.json

import model.db.UmpireAssignment
import play.api.libs.json.Json

object UmpireAssignmentJsonConverter extends JsonConverter[UmpireAssignment] {

  override def toJson(obj: UmpireAssignment) = Json.obj(
    "game" -> obj.game.json(obj.gameId),
    "umpire" -> obj.umpire.json(obj.umpireId)
  )

}
