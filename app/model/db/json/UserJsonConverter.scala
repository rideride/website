package model.db.json

import model.db.User
import play.api.libs.json.{JsObject, Json}

object UserJsonConverter extends JsonConverter[User] {
  override def toJson(obj: User): JsObject = Json.obj(
    "id" -> obj.id.get,
    "redditName" -> obj.redditName,
    "discord" -> obj.discord,
    "google" -> obj.googleId,
    "player" -> obj.player.jsonOpt()
  )
}
