package model.db.json

import model.db.Park
import play.api.libs.json.Json

object ParkJsonConverter extends JsonConverter[Park] {

  override def toJson(obj: Park) = Json.obj(
    "id" -> obj.id.get,
    "name" -> obj.name,
    "factorHR" -> obj.factorHR,
    "factor3B" -> obj.factor3B,
    "factor2B" -> obj.factor2B,
    "factor1B" -> obj.factor1B,
    "factorBB" -> obj.factorBB,
  )

}
