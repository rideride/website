package model.db.json

import model.db.GMAssignment
import play.api.libs.json.Json

object GMAssignmentJsonConverter extends JsonConverter[GMAssignment] {

  override def toJson(obj: GMAssignment) = Json.obj(
    "id" -> obj.id.get,
    "team" -> obj.team.json(obj.teamId),
    "gm" -> obj.gm.json(obj.gmId),
    "startSeason" -> obj.startSeason,
    "startSession" -> obj.startSession,
    "endSeason" -> obj.endSeason,
    "endSession" -> obj.endSession
  )

}
