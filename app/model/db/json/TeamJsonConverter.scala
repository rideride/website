package model.db.json

import model.db.Team
import play.api.libs.json.Json

object TeamJsonConverter extends JsonConverter[Team] {

  override def toJson(obj: Team) = Json.obj(
    "id" -> obj.id.get,
    "tag" -> obj.tag,
    "name" -> obj.name,
    "park" -> obj.park.json(obj.parkId),
    "discordRole" -> obj.discordRole,
    "colorDiscord" -> obj.colorDiscord,
    "colorRoster" -> obj.colorRoster,
    "colorRosterBG" -> obj.colorRosterBG,
    "logoURL" -> obj.logoURL,
    "gmAssignment" -> obj.gmAssignment.jsonOpt(),
    "games" -> obj.games._value.map(_.toJson)
  )

}
