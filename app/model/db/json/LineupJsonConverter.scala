package model.db.json

import model.db.Lineup
import play.api.libs.json.{JsObject, Json}

object LineupJsonConverter extends JsonConverter[Lineup] {

  override def toJson(obj: Lineup): JsObject = Json.obj(
    "id" -> obj.id.get,
    "entries" -> obj.entries._value.map(_.toJson)
  )

}
