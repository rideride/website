package model.db.json

import model.db._
import play.api.libs.json._

object ScoringPlayJsonConverter extends JsonConverter[ScoringPlay] {

  override def toJson(obj: ScoringPlay): JsObject = {
    val t = obj.scorer1._value.map(p => Json.toJsFieldJsValueWrapper(p.map(_.toJson)))
    val q = t.getOrElse(Json.toJsFieldJsValueWrapper(obj.scorer1Id))
    Json.obj(
      "gameAction" -> obj.gameAction._value.map(ga => Json.toJsFieldJsValueWrapper(ga.toJson)).getOrElse(obj.gameActionId),
      "game" -> obj.game._value.map(g => Json.toJsFieldJsValueWrapper(g.toJson)).getOrElse(obj.gameId),
      "scorer1" -> obj.scorer1.jsonOpt(obj.scorer1Id),
      "scorer1Responsibility" -> obj.scorer1Responsibility.jsonOpt(obj.scorer1ResponsibilityId),
      "scorer2" -> obj.scorer2.jsonOpt(obj.scorer2Id),
      "scorer2Responsibility" -> obj.scorer2Responsibility.jsonOpt(obj.scorer2ResponsibilityId),
      "scorer3" -> obj.scorer3.jsonOpt(obj.scorer3Id),
      "scorer3Responsibility" -> obj.scorer3Responsibility.jsonOpt(obj.scorer3ResponsibilityId),
      "scorer4" -> obj.scorer4.jsonOpt(obj.scorer4Id),
      "scorer4Responsibility" -> obj.scorer4Responsibility.jsonOpt(obj.scorer4ResponsibilityId),
      "description" -> obj.description
    )
  }

}
