package model.db

import play.api.libs.json.{JsArray, JsObject}

package object json {

  trait Jsonable[Self <: Jsonable[Self]] { this: Self =>

    val jsonConverter: JsonConverter[Self]

    def toJson: JsObject = jsonConverter.toJson(this)
  }

  trait JsonConverter[T <: Jsonable[T]] {

    def toJson(obj: T): JsObject

    def fromJson(json: JsObject): Option[T] = throw new UnsupportedOperationException()
  }

  implicit class JsonableSeq[T <: Jsonable[T]](seq: Seq[Jsonable[T]]) {
    def toJson: JsArray = JsArray(seq.map(_.toJson))
  }

}
