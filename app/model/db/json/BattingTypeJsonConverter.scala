package model.db.json

import model.db.BattingType
import play.api.libs.json.Json

object BattingTypeJsonConverter extends JsonConverter[BattingType] {

  override def toJson(obj: BattingType) = Json.obj(
    "id" -> obj.id.get,
    "shortcode" -> obj.shortcode,
    "name" -> obj.name,
    "rangeHR" -> obj.rangeHR,
    "range3B" -> obj.range3B,
    "range2B" -> obj.range2B,
    "range1B" -> obj.range1B,
    "rangeBB" -> obj.rangeBB,
    "rangeFO" -> obj.rangeFO,
    "rangeK" -> obj.rangeK,
    "rangePO" -> obj.rangePO,
    "rangeRGO" -> obj.rangeRGO,
    "rangeLGO" -> obj.rangeLGO,
    "rangeSB2" -> obj.rangeSB2,
    "rangeSB3" -> obj.rangeSB3,
    "rangeSB4" -> obj.rangeSB4,
  )

}
