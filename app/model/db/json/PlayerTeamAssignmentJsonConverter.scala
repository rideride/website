package model.db.json

import model.db.PlayerTeamAssignment
import play.api.libs.json.Json

object PlayerTeamAssignmentJsonConverter extends JsonConverter[PlayerTeamAssignment] {

  override def toJson(obj: PlayerTeamAssignment) = Json.obj(
    "player" -> obj.player.json(obj.playerId),
    "team" -> obj.team.jsonOpt(obj.teamId),
    "startSeason" -> obj.startSeason,
    "startSession" -> obj.startSession,
  )

}
