package model.db.json

import model.db.GameAward
import play.api.libs.json.Json

object GameAwardJsonConverter extends JsonConverter[GameAward] {

  override def toJson(obj: GameAward) = Json.obj(
    "game" -> obj.game.json(obj.gameId),
    "winningPitcher" -> obj.winningPitcher.json(obj.winningPitcherId),
    "losingPitcher" -> obj.losingPitcher.json(obj.losingPitcherId),
    "playerOfTheGame" -> obj.playerOfTheGame.json(obj.playerOfTheGameId),
    "save" -> obj.save.jsonOpt(obj.saveId),
  )

}
