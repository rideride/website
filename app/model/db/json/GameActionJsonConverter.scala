package model.db.json

import model.db.GameAction
import play.api.libs.json.{JsObject, Json}

object GameActionJsonConverter extends JsonConverter[GameAction] {

  override def toJson(obj: GameAction): JsObject = Json.obj(
    "id" -> obj.id.get,
    "replacedBy" -> obj.replacedById,
    "game" -> obj.game.json(obj.gameId),
    "playType" -> obj.playType.toString,
    "pitcher" -> obj.pitcher.json(obj.pitcherId),
    "pitch" -> obj.pitch,
    "batter" -> obj.batter.json(obj.batterId),
    "swing" -> obj.swing,
    "diff" -> obj.diff,
    "result" -> obj.result,
    "runsScored" -> obj.runsScored,
    "scoringPlay" -> obj.scoringPlay.jsonOpt(),
    "outsTracked" -> obj.outsTracked,
    "beforeState" -> obj.beforeState.json(obj.beforeStateId),
    "afterState" -> obj.afterState.json(obj.afterStateId)
  )

}
