package model.db.json

import model.db.Game
import play.api.libs.json.Json

object GameJsonConverter extends JsonConverter[Game] {

  override def toJson(obj: Game) = Json.obj(
    "id" -> obj.id.get,
    "name" -> obj.awayTeam._value.flatMap(_ => obj.homeTeam._value.map(_ => obj.name)),
    "season" -> obj.season,
    "session" -> obj.session,
    "awayTeam" -> obj.awayTeam.json(obj.awayTeamId),
    "homeTeam" -> obj.homeTeam.json(obj.homeTeamId),
    "id36" -> obj.id36,
    "completed" -> obj.completed,
    "winningTeam" -> obj.winningTeamId,
    "statsCalculated" -> obj.statsCalculated,
    "state" -> obj.state.json(obj.stateId),
    "park" -> obj.park.json(obj.parkId),
    "awayLineup" -> obj.awayLineup.jsonOpt(obj.awayLineupId),
    "homeLineup" -> obj.homeLineup.jsonOpt(obj.homeLineupId),
    "awards" -> obj.awards.jsonOpt(),
    "actions" -> obj.actions._value.map(_.toJson),
    "scoringPlays" -> obj.scoringPlays._value.map(_.toJson),
  )

}
