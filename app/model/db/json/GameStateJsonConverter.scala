package model.db.json

import model.db.GameState
import play.api.libs.json.Json

object GameStateJsonConverter extends JsonConverter[GameState] {

  override def toJson(obj: GameState) = Json.obj(
    "id" -> obj.id.get,
    "awayBattingPosition" -> obj.awayBattingPosition,
    "homeBattingPosition" -> obj.homeBattingPosition,
    "r1" -> obj.r1.jsonOpt(obj.r1Id),
    "r2" -> obj.r2.jsonOpt(obj.r2Id),
    "r3" -> obj.r3.jsonOpt(obj.r3Id),
    "r1Responsibility" -> obj.r1Responsibility.jsonOpt(obj.r1ResponsibilityId),
    "r2Responsibility" -> obj.r2Responsibility.jsonOpt(obj.r2ResponsibilityId),
    "r3Responsibility" -> obj.r3Responsibility.jsonOpt(obj.r3ResponsibilityId),
    "outs" -> obj.outs,
    "inning" -> obj.inning,
    "inningStr" -> obj.inningStr,
    "scoreAway" -> obj.scoreAway,
    "scoreHome" -> obj.scoreHome
  )

}
