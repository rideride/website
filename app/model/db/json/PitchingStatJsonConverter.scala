package model.db.json

import model.db.PitchingStat
import play.api.libs.json.Json

object PitchingStatJsonConverter extends JsonConverter[PitchingStat] {

  override def toJson(obj: PitchingStat) = Json.obj(
    "player" -> obj.player.json(obj.playerId),
    "season" -> obj.season,
    "totalPAs" -> obj.totalPAs,
    "totalOuts" -> obj.totalOuts,
    "totalER" -> obj.totalER,
    "totalHR" -> obj.totalHR,
    "total3B" -> obj.total3B,
    "total2B" -> obj.total2B,
    "total1B" -> obj.total1B,
    "totalBB" -> obj.totalBB,
    "totalFO" -> obj.totalFO,
    "totalK" -> obj.totalK,
    "totalPO" -> obj.totalPO,
    "totalRGO" -> obj.totalRGO,
    "totalLGO" -> obj.totalLGO,
    "totalSB" -> obj.totalSB,
    "totalCS" -> obj.totalCS,
    "totalDP" -> obj.totalDP,
    "totalTP" -> obj.totalTP,
    "totalGP" -> obj.totalGP,
    "totalSac" -> obj.totalSac,
    "totalIP" -> obj.totalIP,
    "totalGO" -> obj.totalGO,
    "era" -> obj.era,
    "whip" -> obj.whip
  )

}
