package model.db

import model.db.json.{Jsonable, PitchingStatJsonConverter}
import model.db.util._
import services.StatCalculatorService
import shapeless._
import slickless._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class PitchingStat(playerId: Int, season: Int, totalPAs: Int = 0, totalABs: Int = 0, totalOuts: Int = 0, totalER: Int = 0, totalHR: Int = 0, total3B: Int = 0, total2B: Int = 0, total1B: Int = 0, totalBB: Int = 0, totalFO: Int = 0, totalK: Int = 0, totalPO: Int = 0, totalRGO: Int = 0, totalLGO: Int = 0, totalSB: Int = 0, totalCS: Int = 0, totalDP: Int = 0, totalTP: Int = 0, totalGP: Int = 0, totalSac: Int = 0, summedDiff: Int = 0, wins: Int = 0, losses: Int = 0, saves: Int = 0) extends Jsonable[PitchingStat] {

  override val jsonConverter = PitchingStatJsonConverter

  val player: LazyReference[Player, PitchingStat] = lazyReference("player")

  lazy val era: Float = StatCalculatorService.calculateERA(this)
  lazy val whip: Float = StatCalculatorService.calculateWHIP(this)
  lazy val totalIP: String = StatCalculatorService.calculateIP(this)
  lazy val totalGO: Int = StatCalculatorService.calculateGO(this)
  lazy val k6: Float = StatCalculatorService.calculateK6(this)
  lazy val w6: Float = StatCalculatorService.calculateW6(this)
  lazy val dbf: Float = StatCalculatorService.calculateDBF(this)

}

class PitchingStats(tag: Tag) extends Table[PitchingStat](tag, "pitching_stats") {

  def playerId = column[Int]("player")
  def season = column[Int]("season")
  def totalPAs = column[Int]("total_pas")
  def totalABs = column[Int]("total_abs")
  def totalOuts = column[Int]("total_outs")
  def totalER = column[Int]("total_er")
  def totalHR = column[Int]("total_hr")
  def total3B = column[Int]("total_3b")
  def total2B = column[Int]("total_2b")
  def total1B = column[Int]("total_1b")
  def totalBB = column[Int]("total_bb")
  def totalFO = column[Int]("total_fo")
  def totalK = column[Int]("total_k")
  def totalPO = column[Int]("total_po")
  def totalRGO = column[Int]("total_rgo")
  def totalLGO = column[Int]("total_lgo")
  def totalSB = column[Int]("total_sb")
  def totalCS = column[Int]("total_cs")
  def totalDP = column[Int]("total_dp")
  def totalTP = column[Int]("total_tp")
  def totalGP = column[Int]("total_gp")
  def totalSac = column[Int]("total_sac")
  def summedDiff = column[Int]("summed_diff")
  def wins = column[Int]("wins")
  def losses = column[Int]("losses")
  def saves = column[Int]("saves")

  def player = this.mappedForeignKey("fk_pitching_stats_player", _.playerId, Players)(_.player, _.id, onUpdate = Cascade)

  def pk = primaryKey("PRIMARY", (playerId, season))
  def * = (
    playerId ::
      season ::
      totalPAs ::
      totalABs ::
      totalOuts ::
      totalER ::
      totalHR ::
      total3B ::
      total2B ::
      total1B ::
      totalBB ::
      totalFO ::
      totalK ::
      totalPO ::
      totalRGO ::
      totalLGO ::
      totalSB ::
      totalCS ::
      totalDP ::
      totalTP ::
      totalGP ::
      totalSac ::
      summedDiff ::
      wins ::
      losses ::
      saves ::
      HNil
    ).mappedWith(Generic[PitchingStat])

}

object PitchingStats extends TableQuery[PitchingStats](tag => new PitchingStats(tag))
