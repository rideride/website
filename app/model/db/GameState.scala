package model.db

import model.db.json.{GameStateJsonConverter, Jsonable}
import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class GameState(id: Option[Int] = None, awayBattingPosition: Int = 1, homeBattingPosition: Int = 1, r1Id: Option[Int] = None, r2Id: Option[Int] = None, r3Id: Option[Int] = None, r1ResponsibilityId: Option[Int] = None, r2ResponsibilityId: Option[Int] = None, r3ResponsibilityId: Option[Int] = None, outs: Int = 0, inning: Int = 1, scoreAway: Int = 0, scoreHome: Int = 0) extends Jsonable[GameState] {

  override val jsonConverter = GameStateJsonConverter

  val r1: LazyReference[Option[Player], GameState] = lazyReference("r1")
  val r2: LazyReference[Option[Player], GameState] = lazyReference("r2")
  val r3: LazyReference[Option[Player], GameState] = lazyReference("r3")
  val r1Responsibility: LazyReference[Option[Player], GameState] = lazyReference("r1Responsibility")
  val r2Responsibility: LazyReference[Option[Player], GameState] = lazyReference("r2Responsibility")
  val r3Responsibility: LazyReference[Option[Player], GameState] = lazyReference("r3Responsibility")

  lazy val inningStr = s"${if (inning % 2 == 1) "T" else "B"}${(inning + 1) / 2}"
  lazy val nextBatterLineupPos = if (inning % 2 == 1) awayBattingPosition else homeBattingPosition

}

class GameStates(tag: Tag) extends Table[GameState](tag, "game_states") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def awayBattingPosition = column[Int]("away_batting_position")
  def homeBattingPosition = column[Int]("home_batting_position")
  def r1Id = column[Option[Int]]("r1")
  def r2Id = column[Option[Int]]("r2")
  def r3Id = column[Option[Int]]("r3")
  def r1ResponsibilityId = column[Option[Int]]("r1_responsibility")
  def r2ResponsibilityId = column[Option[Int]]("r2_responsibility")
  def r3ResponsibilityId = column[Option[Int]]("r3_responsibility")
  def outs = column[Int]("outs")
  def inning = column[Int]("inning")
  def scoreAway = column[Int]("score_away")
  def scoreHome = column[Int]("score_home")

  def r1 = this.mappedForeignKeyOpt("fk_game_state_r1", _.r1Id, Players)(_.r1, _.id, onUpdate = Cascade)
  def r2 = this.mappedForeignKeyOpt("fk_game_state_r2", _.r2Id, Players)(_.r2, _.id, onUpdate = Cascade)
  def r3 = this.mappedForeignKeyOpt("fk_game_state_r3", _.r3Id, Players)(_.r3, _.id, onUpdate = Cascade)
  def r1Responsibility = this.mappedForeignKeyOpt("fk_game_state_r1_responsibility", _.r1ResponsibilityId, Players)(_.r1Responsibility, _.id, onUpdate = Cascade)
  def r2Responsibility = this.mappedForeignKeyOpt("fk_game_state_r2_responsibility", _.r2ResponsibilityId, Players)(_.r2Responsibility, _.id, onUpdate = Cascade)
  def r3Responsibility = this.mappedForeignKeyOpt("fk_game_state_r3_responsibility", _.r3ResponsibilityId, Players)(_.r3Responsibility, _.id, onUpdate = Cascade)

  def * = (id.?, awayBattingPosition, homeBattingPosition, r1Id, r2Id, r3Id, r1ResponsibilityId, r2ResponsibilityId, r3ResponsibilityId, outs, inning, scoreAway, scoreHome) <> (GameState.tupled, GameState.unapply)

}

object GameStates extends TableQuery[GameStates](tag => new GameStates(tag))
