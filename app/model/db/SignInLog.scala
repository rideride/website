package model.db

import java.sql.Timestamp

import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class SignInLog(id: Option[Int] = None, userId: Int, method: String, ip: String, stamp: Timestamp = new Timestamp(System.currentTimeMillis())) {

  val user: LazyReference[User, SignInLog] = lazyReference("user")

}

class SignInLogs(tag: Tag) extends Table[SignInLog](tag, "signin_logs") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def userId = column[Int]("user")
  def method = column[String]("method")
  def ip = column[String]("ip")
  def stamp = column[Timestamp]("stamp")

  def user = this.mappedForeignKey("fk_signin_history_user", _.userId, Users)(_.user, _.id)

  def * = (id.?, userId, method, ip, stamp) <> (SignInLog.tupled, SignInLog.unapply)

}

object SignInLogs extends TableQuery[SignInLogs](tag => new SignInLogs(tag))
