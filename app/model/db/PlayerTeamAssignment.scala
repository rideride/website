package model.db

import model.db.json.{Jsonable, PlayerTeamAssignmentJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class PlayerTeamAssignment(playerId: Int, teamId: Option[Int], startSeason: Int, startSession: Int) extends Jsonable[PlayerTeamAssignment] {

  override val jsonConverter = PlayerTeamAssignmentJsonConverter

  val player: LazyReference[Player, PlayerTeamAssignment] = lazyReference("player")
  val team: LazyReference[Option[Team], PlayerTeamAssignment] = lazyReference("team")

}

class PlayerTeamAssignments(tag: Tag) extends Table[PlayerTeamAssignment](tag, "player_team_assignments") {

  def playerId = column[Int]("player")
  def teamId = column[Option[Int]]("team")
  def startSeason = column[Int]("start_season")
  def startSession = column[Int]("start_session")

  def pk = primaryKey("PRIMARY", (playerId, startSeason, startSession))

  def player = this.mappedForeignKey("fk_player_team_assignments_player", _.playerId, Players)(_.player, _.id, onUpdate = Cascade)
  def team = this.mappedForeignKeyOpt("fk_player_team_assignments_team", _.teamId, Teams)(_.team, _.id)

  def * = (playerId, teamId, startSeason, startSession) <> (PlayerTeamAssignment.tupled, PlayerTeamAssignment.unapply)

}

object PlayerTeamAssignments extends TableQuery[PlayerTeamAssignments](tag => new PlayerTeamAssignments(tag))
