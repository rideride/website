package model.db

import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class TradeBlockEntry(playerId: Int, notes: String) {

  val player: LazyReference[Player, TradeBlockEntry] = lazyReference("player")

}

class TradeBlockEntries(tag: Tag) extends Table[TradeBlockEntry](tag, "trade_block_entries") with Identified {

  def id = column[Int]("player", O.PrimaryKey)
  def notes = column[String]("notes")

  def player = this.mappedForeignKey("fk_trade_block_entries_player", _.id, Players)(_.player, _.id)

  def * = (id, notes) <> (TradeBlockEntry.tupled, TradeBlockEntry.unapply)

}

object TradeBlockEntries extends TableQuery[TradeBlockEntries](tag => new TradeBlockEntries(tag))
