package model.db

import slick.jdbc.MySQLProfile.api._

case class GlobalSetting(key: String, value: String)

class GlobalSettings(tag: Tag) extends Table[GlobalSetting](tag, "global_settings") {

  def key = column[String]("key", O.PrimaryKey)
  def value = column[String]("value")

  def * = (key, value) <> (GlobalSetting.tupled, GlobalSetting.unapply)

}

object GlobalSettings extends TableQuery[GlobalSettings](tag => new GlobalSettings(tag))
