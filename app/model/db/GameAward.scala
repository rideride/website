package model.db

import model.db.json.{GameAwardJsonConverter, Jsonable}
import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class GameAward(gameId: Int, winningPitcherId: Int, losingPitcherId: Int, playerOfTheGameId: Int, saveId: Option[Int] = None) extends Jsonable[GameAward] {

  override val jsonConverter = GameAwardJsonConverter

  val game: LazyReference[Game, GameAward] = lazyReference("game")
  val winningPitcher: LazyReference[Player, GameAward] = lazyReference("winningPitcher")
  val losingPitcher: LazyReference[Player, GameAward] = lazyReference("losingPitcher")
  val playerOfTheGame: LazyReference[Player, GameAward] = lazyReference("playerOfTheGame")
  val save: LazyReference[Option[Player], GameAward] = lazyReference("save")

}

class GameAwards(tag: Tag) extends Table[GameAward](tag, "game_awards") with Identified {

  def id = column[Int]("game", O.PrimaryKey)
  def winningPitcherId = column[Int]("winning_pitcher")
  def losingPitcherId = column[Int]("losing_pitcher")
  def playerOfTheGameId = column[Int]("player_of_the_game")
  def saveId = column[Option[Int]]("save")

  def game = this.mappedForeignKey("fk_game_awards_game", _.id, Games)(_.game, _.id)
  def winningPitcher = this.mappedForeignKey("fk_game_awards_winning_pitcher", _.winningPitcherId, Players)(_.winningPitcher, _.id, onUpdate = Cascade)
  def losingPitcher = this.mappedForeignKey("fk_game_awards_losing_pitcher", _.losingPitcherId, Players)(_.losingPitcher, _.id, onUpdate = Cascade)
  def playerOfTheGame = this.mappedForeignKey("fk_game_awards_player_of_the_game", _.playerOfTheGameId, Players)(_.playerOfTheGame, _.id, onUpdate = Cascade)
  def save = this.mappedForeignKeyOpt("fk_game_awards_save", _.saveId, Players)(_.save, _.id, onUpdate = Cascade)

  def * = (id, winningPitcherId, losingPitcherId, playerOfTheGameId, saveId) <> (GameAward.tupled, GameAward.unapply)

}

object GameAwards extends TableQuery[GameAwards](tag => new GameAwards(tag))
