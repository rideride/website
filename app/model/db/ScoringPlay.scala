package model.db

import model.db.json.{Jsonable, ScoringPlayJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class ScoringPlay(gameActionId: Int, gameId: Int, scorer1Id: Option[Int], scorer1ResponsibilityId: Option[Int], scorer2Id: Option[Int], scorer2ResponsibilityId: Option[Int], scorer3Id: Option[Int], scorer3ResponsibilityId: Option[Int], scorer4Id: Option[Int], scorer4ResponsibilityId: Option[Int], description: String) extends Jsonable[ScoringPlay] {

  override val jsonConverter = ScoringPlayJsonConverter

  val gameAction: LazyReference[GameAction, ScoringPlay] = lazyReference("gameAction")
  val game: LazyReference[Game, ScoringPlay] = lazyReference("game")
  val scorer1: LazyReference[Option[Player], ScoringPlay] = lazyReference("scorer1")
  val scorer1Responsibility: LazyReference[Option[Player], ScoringPlay] = lazyReference("scorer1Responsibility")
  val scorer2: LazyReference[Option[Player], ScoringPlay] = lazyReference("scorer2")
  val scorer2Responsibility: LazyReference[Option[Player], ScoringPlay] = lazyReference("scorer2Responsibility")
  val scorer3: LazyReference[Option[Player], ScoringPlay] = lazyReference("scorer3")
  val scorer3Responsibility: LazyReference[Option[Player], ScoringPlay] = lazyReference("scorer3Responsibility")
  val scorer4: LazyReference[Option[Player], ScoringPlay] = lazyReference("scorer4")
  val scorer4Responsibility: LazyReference[Option[Player], ScoringPlay] = lazyReference("scorer4Responsibility")

}

class ScoringPlays(tag: Tag) extends Table[ScoringPlay](tag, "scoring_plays") with Identified {

  def id = column[Int]("game_action", O.PrimaryKey)
  def gameId = column[Int]("game_id")
  def scorer1Id = column[Option[Int]]("scorer1")
  def scorer1ResponsibilityId = column[Option[Int]]("scorer1_responsibility")
  def scorer2Id = column[Option[Int]]("scorer2")
  def scorer2ResponsibilityId = column[Option[Int]]("scorer2_responsibility")
  def scorer3Id = column[Option[Int]]("scorer3")
  def scorer3ResponsibilityId = column[Option[Int]]("scorer3_responsibility")
  def scorer4Id = column[Option[Int]]("scorer4")
  def scorer4ResponsibilityId = column[Option[Int]]("scorer4_responsibility")
  def description = column[String]("description")

  def gameAction = this.mappedForeignKey("fk_scoring_plays_game_action", _.id, GameActions)(_.gameAction, _.id, onUpdate = Cascade)
  def game = this.mappedForeignKey("fk_scoring_plays_game_id", _.gameId, Games)(_.game, _.id)
  def scorer1 = this.mappedForeignKeyOpt("fk_scoring_plays_scorer1", _.scorer1Id, Players)(_.scorer1, _.id, onUpdate = Cascade)
  def scorer1Responsibility = this.mappedForeignKeyOpt("fk_scoring_plays_scorer1_responsibility", _.scorer1ResponsibilityId, Players)(_.scorer1Responsibility, _.id, onUpdate = Cascade)
  def scorer2 = this.mappedForeignKeyOpt("fk_scoring_plays_scorer2", _.scorer2Id, Players)(_.scorer2, _.id, onUpdate = Cascade)
  def scorer2Responsibility = this.mappedForeignKeyOpt("fk_scoring_plays_scorer2_responsibility", _.scorer2ResponsibilityId, Players)(_.scorer2Responsibility, _.id, onUpdate = Cascade)
  def scorer3 = this.mappedForeignKeyOpt("fk_scoring_plays_scorer3", _.scorer3Id, Players)(_.scorer3, _.id, onUpdate = Cascade)
  def scorer3Responsibility = this.mappedForeignKeyOpt("fk_scoring_plays_scorer3_responsibility", _.scorer3ResponsibilityId, Players)(_.scorer3Responsibility, _.id, onUpdate = Cascade)
  def scorer4 = this.mappedForeignKeyOpt("fk_scoring_plays_scorer4", _.scorer4Id, Players)(_.scorer4, _.id, onUpdate = Cascade)
  def scorer4Responsibility = this.mappedForeignKeyOpt("fk_scoring_plays_scorer4_responsibility", _.scorer4ResponsibilityId, Players)(_.scorer4Responsibility, _.id, onUpdate = Cascade)

  def * = (id, gameId, scorer1Id, scorer1ResponsibilityId, scorer2Id, scorer2ResponsibilityId, scorer3Id, scorer3ResponsibilityId, scorer4Id, scorer4ResponsibilityId, description) <> (ScoringPlay.tupled, ScoringPlay.unapply)

}

object ScoringPlays extends TableQuery[ScoringPlays](tag => new ScoringPlays(tag))
