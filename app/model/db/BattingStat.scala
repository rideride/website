package model.db

import model.db.json.{BattingStatJsonConverter, Jsonable}
import model.db.util._
import services.StatCalculatorService
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade
import shapeless._
import slickless._

case class BattingStat(playerId: Int, season: Int, totalPA: Int = 0, totalAB: Int = 0, totalHR: Int = 0, total3B: Int = 0, total2B: Int = 0, total1B: Int = 0, totalBB: Int = 0, totalFO: Int = 0, totalK: Int = 0, totalPO: Int = 0, totalRGO: Int = 0, totalLGO: Int = 0, totalRBI: Int = 0, totalR: Int = 0, totalSB: Int = 0, totalCS: Int = 0, totalDP: Int = 0, totalTP: Int = 0, totalGP: Int = 0, totalSac: Int = 0, summedDiff: Int = 0) extends Jsonable[BattingStat] {

  override val jsonConverter = BattingStatJsonConverter

  val player: LazyReference[Player, BattingStat] = lazyReference("player")

  lazy val ba: Float = StatCalculatorService.calculateBA(this)
  lazy val obp: Float = StatCalculatorService.calculateOBP(this)
  lazy val slg: Float = StatCalculatorService.calculateSLG(this)
  lazy val ops: Float = StatCalculatorService.calculateOPS(this)
  lazy val tb: Float = StatCalculatorService.calculateTB(this)
  lazy val dpa: Float = StatCalculatorService.calculateDPA(this)

}

class BattingStats(tag: Tag) extends Table[BattingStat](tag, "batting_stats") {

  def playerId = column[Int]("player")
  def season = column[Int]("season")
  def totalPAs = column[Int]("total_pas")
  def totalABs = column[Int]("total_abs")
  def totalHR = column[Int]("total_hr")
  def total3B = column[Int]("total_3b")
  def total2B = column[Int]("total_2b")
  def total1B = column[Int]("total_1b")
  def totalBB = column[Int]("total_bb")
  def totalFO = column[Int]("total_fo")
  def totalK = column[Int]("total_k")
  def totalPO = column[Int]("total_po")
  def totalRGO = column[Int]("total_rgo")
  def totalLGO = column[Int]("total_lgo")
  def totalRBI = column[Int]("total_rbi")
  def totalR = column[Int]("total_r")
  def totalSB = column[Int]("total_sb")
  def totalCS = column[Int]("total_cs")
  def totalDP = column[Int]("total_dp")
  def totalTP = column[Int]("total_tp")
  def totalGP = column[Int]("total_gp")
  def totalSac = column[Int]("total_sac")
  def summedDiff = column[Int]("summed_diff")

  def player = this.mappedForeignKey("fk_batting_stats_player", _.playerId, Players)(_.player, _.id, onUpdate = Cascade)

  def pk = primaryKey("PRIMARY", (playerId, season))
  def * = (
    playerId ::
      season ::
      totalPAs ::
      totalABs ::
      totalHR ::
      total3B ::
      total2B ::
      total1B ::
      totalBB ::
      totalFO ::
      totalK ::
      totalPO ::
      totalRGO ::
      totalLGO ::
      totalRBI ::
      totalR ::
      totalSB ::
      totalCS ::
      totalDP ::
      totalTP ::
      totalGP ::
      totalSac ::
      summedDiff ::
      HNil
    ).mappedWith(Generic[BattingStat])

}

object BattingStats extends TableQuery[BattingStats](tag => new BattingStats(tag))
