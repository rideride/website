package model.db

import java.sql.Date

import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class UniqueAccessPoint(requestingUserId: Int, srcIP: String, reqDate: Date) {

  val requestingUser: LazyReference[User, UniqueAccessPoint] = lazyReference("requestingUser")

}

class UniqueAccessPoints(tag: Tag) extends Table[UniqueAccessPoint](tag, "unique_access_points") {

  def requestingUserId = column[Int]("requesting_user")
  def srcIP = column[String]("src_ip")
  def reqDate = column[Date]("req_date")

  def requestingUser = this.mappedForeignKey("fk_unique_access_points_requesting_user", _.requestingUserId, Users)(_.requestingUser, _.id)

  def pk = primaryKey("PRIMARY", (requestingUserId, srcIP, reqDate))

  def * = (requestingUserId, srcIP, reqDate) <> (UniqueAccessPoint.tupled, UniqueAccessPoint.unapply)

}

object UniqueAccessPoints extends TableQuery[UniqueAccessPoints](tag => new UniqueAccessPoints(tag))
