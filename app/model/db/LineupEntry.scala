package model.db

import java.sql.Timestamp

import model.db.json.{Jsonable, LineupEntryJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._
import slick.model.ForeignKeyAction.Cascade

case class LineupEntry(id: Option[Int] = None, lineupId: Int, playerId: Int, position: String, battingPos: Int, replacedById: Option[Int] = None, enteredAt: Timestamp = new Timestamp(System.currentTimeMillis())) extends Jsonable[LineupEntry] {

  override val jsonConverter = LineupEntryJsonConverter

  val player: LazyReference[Player, LineupEntry] = lazyReference("player")
  val replacedBy: LazyReference[Option[LineupEntry], LineupEntry] = lazyReference("replacedBy")

}

class LineupEntries(tag: Tag) extends Table[LineupEntry](tag, "lineup_entries") {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def lineupId = column[Int]("lineup")
  def playerId = column[Int]("player")
  def position = column[String]("position")
  def battingPos = column[Int]("batting_pos")
  def replacedById = column[Option[Int]]("replaced_by")
  def enteredAt = column[Timestamp]("entered_at")

  def lineup = foreignKey("lineup_substitutions_lineup", lineupId, Lineups)(_.id)
  def player = this.mappedForeignKey("lineup_substitutions_player", _.playerId, Players)(_.player, _.id, onUpdate = Cascade)
  def replacedBy = this.mappedForeignKeyOpt("lineup_substitutions_replaced_by", _.replacedById, LineupEntries)(_.replacedBy, _.id, onUpdate = Cascade)

  def * = (id.?, lineupId, playerId, position, battingPos, replacedById, enteredAt) <> (LineupEntry.tupled, LineupEntry.unapply)

}

object LineupEntries extends TableQuery[LineupEntries](tag => new LineupEntries(tag))
