package model.db

import model.FBDatabaseProvider
import model.db.json.{Jsonable, UserJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class User(id: Option[Int] = None, redditName: String, discord: Option[String] = None, googleId: Option[String] = None, isPlayer: Boolean = false, isUmpire: Boolean = false, isCommissioner: Boolean = false, isGM: Boolean = false, refreshToken: Option[String] = None) extends Jsonable[User] {

  override val jsonConverter = UserJsonConverter

  val player: LazyReference[Option[Player], User] = lazyReference("player")
  val preferences: LazyReference[UserPreference, User] = lazyReference("preferences")

  lazy val name: String = player._value.flatten.map(_.fullName + s" ($redditName)").getOrElse(redditName)

}

class Users(tag: Tag) extends Table[User](tag, "users") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def redditName = column[String]("reddit_name")
  def discord = column[Option[String]]("discord")
  def googleId = column[Option[String]]("google_id")
  def isPlayer = column[Boolean]("is_player")
  def isUmpire = column[Boolean]("is_umpire")
  def isCommissioner = column[Boolean]("is_commissioner")
  def isGM = column[Boolean]("is_gm")
  def refreshToken = column[Option[String]]("refresh_token")

  def player = this.mapOpt(_.id, Players)(_.player, _.userId)
  def preferences = this.map(_.id, UserPreferences)(_.preferences, _.userId)

  def uniqueDiscord = index("discord", discord, unique = true)
  def uniqueRedditName = index("reddit_name", redditName, unique = true)

  def * = (id.?, redditName, discord, googleId, isPlayer, isUmpire, isCommissioner, isGM, refreshToken) <> (User.tupled, User.unapply)

}

object Users extends TableQuery[Users](tag => new Users(tag)) {

  def findByRedditName(reddit: String)(implicit db: FBDatabaseProvider): Option[User] = Users.filter(_.redditName === reddit).singleQOpt

}
