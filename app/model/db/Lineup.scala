package model.db

import model.db.json.{Jsonable, LineupJsonConverter}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class Lineup(id: Option[Int] = None) extends Jsonable[Lineup] {

  override val jsonConverter = LineupJsonConverter

  val entries: LazyReference[Seq[LineupEntry], Lineup] = lazyReference("entries")

}

class Lineups(tag: Tag) extends Table[Lineup](tag, "lineups") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

  def entries = this.mapSeq(l => LineupEntries.mapFilter(_.lineupId === l.id))(_.entries)

  def * = id.? <> (Lineup, Lineup.unapply)

}

object Lineups extends TableQuery[Lineups](tag => new Lineups(tag))
