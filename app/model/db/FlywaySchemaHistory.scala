package model.db

import java.sql.Timestamp

import slick.jdbc.MySQLProfile.api._

case class FlywaySchemaHistory(installedRank: Int, version: Option[String], description: String, `type`: String, script: String, checksum: Option[Int], installedBy: String, installedOn: Timestamp, executionTime: Int, success: Boolean)

class FlywaySchemaHistories(tag: Tag) extends Table[FlywaySchemaHistory](tag, "flyway_schema_history") {

  def installedRank = column[Int]("installed_rank", O.PrimaryKey)
  def version = column[Option[String]]("version")
  def description = column[String]("description")
  def `type` = column[String]("type")
  def script = column[String]("script")
  def checksum = column[Option[Int]]("checksum")
  def installedBy = column[String]("installed_by")
  def installedOn = column[Timestamp]("installed_on")
  def executionTime = column[Int]("execution_time")
  def success = column[Boolean]("success")

  def successIndex = index("flyway_schema_history_s_idx", success)

  def * = (installedRank, version, description, `type`, script, checksum, installedBy, installedOn, executionTime, success) <> (FlywaySchemaHistory.tupled, FlywaySchemaHistory.unapply)

}

object FlywaySchemaHistories extends TableQuery[FlywaySchemaHistories](tag => new FlywaySchemaHistories(tag))
