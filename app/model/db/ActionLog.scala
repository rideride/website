package model.db

import java.sql.Timestamp

import model.db.util._
import slick.jdbc.MySQLProfile.api._

case class ActionLog(var id: Option[Int] = None, var requestPath: String, var requestMethod: String, var requestStamp: Timestamp, var responseStamp: Timestamp, var responseCode: Int, var requestingUserId: Option[Int] = None, var srcIp: String, var queryString: Option[String] = None) {

  val requestingUser: LazyReference[Option[User], ActionLog] = lazyReference("requestingUser")

}

class ActionLogs(tag: Tag) extends Table[ActionLog](tag, "action_logs") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def requestPath = column[String]("request_path")
  def requestMethod = column[String]("request_method")
  def requestStamp = column[Timestamp]("request_stamp")
  def responseStamp = column[Timestamp]("response_stamp")
  def responseCode = column[Int]("response_code")
  def requestingUserId = column[Option[Int]]("requesting_user")
  def requestIP = column[String]("src_ip")
  def queryString = column[Option[String]]("query_string")

  def actingUser = this.mappedForeignKeyOpt("fk_action_log_req_user", _.requestingUserId, Users)(_.requestingUser, _.id)

  def * = (id.?, requestPath, requestMethod, requestStamp, responseStamp, responseCode, requestingUserId, requestIP, queryString) <> (ActionLog.tupled, ActionLog.unapply)

}

object ActionLogs extends TableQuery[ActionLogs](tag => new ActionLogs(tag))
