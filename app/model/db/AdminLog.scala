package model.db

import model.db.AdminLogActionType.{AdminLogActionType, ColumnMapping}
import model.db.util._
import slick.jdbc.MySQLProfile.api._

object AdminLogActionType extends DatabaseEnumeration {
  type AdminLogActionType = Value
  val MANAGE_GAMES = Value(0)
  val MANAGE_UMPIRES = Value(1)
  val MANAGE_TEAMS = Value(2)
  val MANAGE_PLAYERS = Value(3)
  val MANAGE_PARKS = Value(4)
}

case class AdminLog(id: Option[Int] = None, actingUserId: Int, actionType: AdminLogActionType, logMessage: String) {

  val actingUser: LazyReference[User, AdminLog] = lazyReference("actingUser")

}

class AdminLogs(tag: Tag) extends Table[AdminLog](tag, "admin_logs") with Identified {

  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def actingUserId = column[Int]("acting_user")
  def actionTypeId = column[AdminLogActionType]("action_type")
  def logMessage = column[String]("log_message")

  def actingUser = this.mappedForeignKey("fk_admin_logs_acting_user", _.actingUserId, Users)(_.actingUser, _.id)

  def * = (id.?, actingUserId, actionTypeId, logMessage) <> (AdminLog.tupled, AdminLog.unapply)

}

object AdminLogs extends TableQuery[AdminLogs](tag => new AdminLogs(tag))
