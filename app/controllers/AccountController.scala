package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import model.forms.UserPreferencesForm
import model.forms.UserPreferencesForm.userPreferencesForm
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder, MessagesRequest, Result}
import slick.jdbc.MySQLProfile.api._

@Singleton
class AccountController @Inject()(implicit db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def myAccount: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    Redirect(routes.AccountController.viewAccount(activeUser.id.get))
  }

  private def targetUser(id: Int)(action: User => Result)(implicit db: FBDatabaseProvider, req: MessagesRequest[AnyContent], activeUser: User): Result =
    Users.withMap(_.preferences)
      .withMapOpt(_.player)
      .findById(id)
      .map(action)
      .getOrElse(UnauthorizedPage)

  def viewAccount(id: Int): Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    targetUser(id) { targetUser =>
      val prefs = targetUser.preferences.withDefaults
      val currentPrefs = UserPreferencesForm(prefs.umpBatterPing)
      Ok(views.html.account.view(targetUser, userPreferencesForm.fill(currentPrefs).discardingErrors))
    }
  }

  def submitPreferences(id: Int): Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    targetUser(id) { targetUser =>
      userPreferencesForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.account.view(targetUser, formWithErrors)),
        formData => {
          UserPreferences.filter(_.userId === targetUser.id.get)
            .map(_.umpBatterPing)
            .update(formData.umpBatterPing)
            .run
          Redirect(routes.AccountController.viewAccount(id))
        }
      )
    }
  }

  def clearDiscord: Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    Users.filter(_.id === activeUser.id.get)
      .map(_.discord)
      .update(None)
      .run

    Redirect(routes.AccountController.viewAccount(activeUser.id.get))
  }

  def clearRedditRefresh: Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    Users.filter(_.id === activeUser.id.get)
      .map(_.refreshToken)
      .update(None)
      .run

    Redirect(routes.AccountController.viewAccount(activeUser.id.get))
  }

  def clearGoogle: Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    Users.filter(_.id === activeUser.id.get)
      .map(_.googleId)
      .update(None)
      .run

    Redirect(routes.AccountController.viewAccount(activeUser.id.get))
  }

}
