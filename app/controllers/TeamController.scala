package controllers

import javax.inject.{Inject, Singleton}
import model.db._
import model.db.util._
import model.FBDatabaseProvider
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider
import services.standings.StandingsService
import slick.jdbc.MySQLProfile.api._

@Singleton
class TeamController @Inject()(implicit settingsProvider: GlobalSettingsProvider,
                               db: FBDatabaseProvider,
                               messagesActionBuilder: MessagesActionBuilder,
                               cc: ControllerComponents,
                               standingsService: StandingsService)
  extends AuthenticatedController {

  def listTeams: Action[AnyContent] = messagesActionBuilder { implicit req =>
    val doStandings: Boolean = settingsProvider.STANDINGS_ENABLED

    val maxSeason = Games.map(_.season).max.result.run.getOrElse(1)
    val teams: Seq[Team] = Teams.filter(_.visible).withMapSeq(_.games.nest(_.awayTeam).nest(_.homeTeam)).seq
    val divisions: Seq[(Division, Seq[Team])] = Divisions.seq
      .map(div => {
        val divTeams = teams.filter(_.divisionId.contains(div.id.get))
        if (doStandings)
          div -> divTeams.sortWith(standingsService.divisionTiebreak(maxSeason))
        else
          div -> divTeams
      })
    val standings: Seq[(League, Seq[(Division, Seq[Team])])] = Leagues.seq
        .map(league => league -> divisions.filter(_._1.leagueId.contains(league.id.get)))

    Ok(views.html.teams.list(teams, standings, maxSeason, doStandings)(req, activeUserOpt))
  }

  def viewTeam(tag: String): Action[AnyContent] = messagesActionBuilder { implicit req =>
    Teams.filter(_.tag === tag)
      .withMap(_.park)
      .withMapOpt(_.gmAssignment)
      .withMapSeq(_.games)
      .withMapSeq(_.players.nest(_.battingType).nestOpt(_.pitchingType).nestOpt(_.pitchingBonus))
      .singleOpt
      .map { team =>

        Ok(views.html.teams.view(team)(req, activeUserOpt))
      } getOrElse {
      NotFound("That team does not exist.")
    }
  }

}
