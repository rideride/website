package controllers

import akka.util.ByteString
import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import play.api.http.HttpEntity
import play.api.mvc._
import services.{GameStateImageGeneratorService, GlobalSettingsProvider}
import slick.jdbc.MySQLProfile.api._

@Singleton
class GameController @Inject()(gameStateImageGenerator: GameStateImageGeneratorService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def index: Action[AnyContent] = messagesActionBuilder { implicit req =>
    val maxSeason = Games.map(_.season).max.result.run.getOrElse(1)
    val maxSession = Games.filter(_.season === maxSeason).map(_.session).max.result.run.getOrElse(1)
    Redirect(routes.GameController.list(maxSeason, maxSession))
  }

  def list(season: Int, session: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    val games = Games.filter(g => g.season === season && g.session === session).withMap(_.state).withMap(_.awayTeam).withMap(_.homeTeam).seq
    val seasonOptions = Games.map(_.season).distinct.seqQ
    val sessionOptions = Games.filter(_.season === season).map(_.session).distinct.seqQ
    Ok(views.html.games.list(games, (season, session), seasonOptions, sessionOptions))
  }

  def viewGame(gameId: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMap(_.state)
      .withMap(_.park)
      .withMapOpt(_.awards)
      .withMapOpt(_.awayLineup.nestSeq(_.entries.nest(_.player.nestOpt(_.nickname).nestSeq(_.battingStats).nestSeq(_.pitchingStats))))
      .withMapOpt(_.homeLineup.nestSeq(_.entries.nest(_.player.nestOpt(_.nickname).nestSeq(_.battingStats).nestSeq(_.pitchingStats))))
      .withMapOpt(_.awards.nest(_.winningPitcher).nest(_.losingPitcher).nest(_.playerOfTheGame).nestOpt(_.save))
      .withMapSeq(_.umpireAssignments.nest(_.umpire.nestOpt(_.player)))
      .withMapSeq(_.actions.nest(_.beforeState).nest(_.afterState).nest(_.batter).nest(_.pitcher))
      .withMapSeq(_.scoringPlays.nest(_.gameAction.nest(_.beforeState).nest(_.afterState)))
      .singleOpt
      .map { game =>
        val editable = activeUserOpt.exists(u => u.isCommissioner || game.umpireAssignments.exists(_.umpire.id.get == u.id.get))

        Ok(views.html.games.view(game, editable)(req, activeUserOpt))
      } getOrElse {
      BadRequest("This game does not exist.")
    }
  }

  def getGameImage(gameId: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.state)
      .withMapOpt(_.awayLineup.nestSeq(_.entries.nest(_.player)))
      .withMapOpt(_.homeLineup.nestSeq(_.entries.nest(_.player)))
      .singleOpt
      .map { game =>
        Result(
          header = ResponseHeader(200),
          body = HttpEntity.Strict(ByteString(gameStateImageGenerator.generateImage(game)), Some("image/svg+xml"))
        )
      } getOrElse {
      BadRequest("This game does not exist.")
    }
  }

  def getGamePreviewImage(gameId: Int): Action[AnyContent] = messagesActionBuilder {
    Games.filter(_.id === gameId)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .singleOpt
      .map { game =>
        Result(
          header = ResponseHeader(200),
          body = HttpEntity.Strict(ByteString(gameStateImageGenerator.generateBasicImage(game)), Some("image/png"))
        )
      } getOrElse {
      BadRequest("This game does not exist.")
    }
  }

}
