package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import model.forms.EditBlockEntryForm._
import model.forms.EditBlockNotesForm
import model.forms.EditBlockNotesForm._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider
import slick.jdbc.MySQLProfile.api._

@Singleton
class GMController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def index: Action[AnyContent] = UserAuthenticatedAction(SCOPE_GM || SCOPE_COMMISSIONER) { implicit ru =>
    val freeAgentCount = Players
      .filter(p => !p.deactivated && p.teamId.isEmpty)
      .size
      .result
      .run

    Ok(views.html.gm.index(freeAgentCount))
  }

  def freeAgents: Action[AnyContent] = UserAuthenticatedAction(SCOPE_GM || SCOPE_COMMISSIONER) { implicit ru =>
    val freeAgents = Players
        .mapFilter(p => !p.deactivated && p.teamId.isEmpty)
        .withTypes
        .seq

    Ok(views.html.gm.freeagents(freeAgents))
  }

  def tradeBlock: Action[AnyContent] = UserAuthenticatedAction(SCOPE_GM || SCOPE_COMMISSIONER) { implicit ru =>
    val teams = Teams.filter(_.visible)
      .sortBy(_.tag)
      .withMapOpt(_.tradeBlockNote)
      .withMapSeq(_.tradeBlockEntries)
      .seq

    Ok(views.html.gm.tradeblock(teams))
  }

  private def teamWithNotesOfCallingUser(implicit user: User, db: FBDatabaseProvider): Option[Team] =
    Users
      .filter(_.id === user.id.get)
      .withMapOpt(_.player.nestOpt(_.team.nestOpt(_.tradeBlockNote).nestSeq(_.tradeBlockEntries)))
      .singleOpt
      .flatMap(_.player.flatMap(_.team))

  def showEditBlockForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_GM || SCOPE_COMMISSIONER) { implicit ru =>
    teamWithNotesOfCallingUser
      .map { team =>

        val notesForm = editBlockNotesForm
          .fill(EditBlockNotesForm(team.tradeBlockNote.map(_.notes)))

        Ok(views.html.gm.editblock(team, Players.byTeam(team).seq, notesForm, editBlockEntryForm))

      } getOrElse {
      UnauthorizedPage
    }
  }

  def submitEditNotesForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_GM || SCOPE_COMMISSIONER) { implicit ru =>
    teamWithNotesOfCallingUser
      .map { team =>

        editBlockNotesForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.gm.editblock(team, Players.byTeam(team).seq, formWithErrors, editBlockEntryForm)),

          formData => {
            TeamTradeBlockNotes.insertOrUpdate(TeamTradeBlockNote(team.id.get, formData.newNotes.getOrElse(""))).run
            Redirect(routes.GMController.showEditBlockForm())
          }

        )

      } getOrElse {
      UnauthorizedPage
    }
  }

  def submitEditBlockEntryForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_GM || SCOPE_COMMISSIONER) { implicit ru =>
    teamWithNotesOfCallingUser
      .map { team =>
        editBlockEntryForm.bindFromRequest.fold(
          formWithErrors => {
            val notesForm = editBlockNotesForm
              .fill(EditBlockNotesForm(team.tradeBlockNote.map(_.notes)))
            BadRequest(views.html.gm.editblock(team, Players.byTeam(team).seq, notesForm, formWithErrors))
          },

          formData => {
            TeamTradeBlockNotes.insertOrUpdate(TeamTradeBlockNote(team.id.get, team.tradeBlockNote.map(_.notes).getOrElse(""))).run
            if (formData.removal)
              TradeBlockEntries.filter(_.id === formData.player.id.get).delete.run
            else
              TradeBlockEntries.insertOrUpdate(TradeBlockEntry(formData.player.id.get, formData.notes)).run

            Redirect(routes.GMController.showEditBlockForm())
          }
        )
      } getOrElse {
      UnauthorizedPage
    }
  }

}
