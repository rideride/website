package controllers

import java.sql.Timestamp

import javax.inject.{Inject, Singleton}
import model.forms.DeleteActionForm._
import model.forms.LineupForm._
import model.forms.NewGameActionForm._
import model.forms.SetID36Form._
import model.forms.EditScoringPlayForm._
import model.forms.GameAwardsForm._
import model.{FBDatabaseProvider, PartialGameAction}
import model.db._
import model.db.util._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.{DiscordMessageService, GameFeedService, GameFlowService, GlobalSettingsProvider, MLRCalculatorService, RedditAuthService, RedditFormattingService, RedditPostService, StatCalculatorService}
import slick.jdbc.MySQLProfile.api._

import scala.util.Try

@Singleton
class UmpireController @Inject()(calc: MLRCalculatorService, globalSettingsProvider: GlobalSettingsProvider, gameFeedService: GameFeedService, discordMessageService: DiscordMessageService, redditFormattingService: RedditFormattingService, redditPostService: RedditPostService, redditAuthService: RedditAuthService, gameFlowService: GameFlowService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def index: Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit req =>
    val games = UmpireAssignments.filter(_.umpireId === activeUser.id.get).withMap(_.game.nest(_.state).nest(_.awayTeam).nest(_.homeTeam)).seq.map(_.game.value)
    Ok(views.html.umpires.index(games))
  }

  private def fullyLoadGame(gameId: Int): Option[Game] =
    Games.filter(_.id === gameId)
      .withMap(_.park)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMap(_.state.nestOpt(_.r1.nestOpt(_.nickname).nest(_.battingType)).nestOpt(_.r2.nestOpt(_.nickname).nest(_.battingType)).nestOpt(_.r3.nestOpt(_.nickname).nest(_.battingType)).nestOpt(_.r1Responsibility).nestOpt(_.r2Responsibility).nestOpt(_.r3Responsibility))
      .withMapOpt(_.awayLineup.nestSeq(_.entries.nest(_.player.nest(_.user).nestOpt(_.nickname).nest(_.battingType).nestOpt(_.pitchingType).nestOpt(_.pitchingBonus).nestSeq(_.battingStats).nestSeq(_.pitchingStats))))
      .withMapOpt(_.homeLineup.nestSeq(_.entries.nest(_.player.nest(_.user).nestOpt(_.nickname).nest(_.battingType).nestOpt(_.pitchingType).nestOpt(_.pitchingBonus).nestSeq(_.battingStats).nestSeq(_.pitchingStats))))
      .withMapOpt(_.threadOwner)
      .withMapOpt(_.awards.nest(_.winningPitcher.nestOpt(_.nickname)).nest(_.losingPitcher.nestOpt(_.nickname)).nest(_.playerOfTheGame.nestOpt(_.nickname)).nestOpt(_.save.nestOpt(_.nickname)))
      .withMapSeq(_.umpireAssignments)
      .withMapSeq(_.actions.nest(_.beforeState).nest(_.afterState))
      .withMapSeq(_.scoringPlays.nest(_.gameAction.nest(_.beforeState).nest(_.afterState)))
      .singleOpt

  def showNewGameActionForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit req =>
    fullyLoadGame(gameId)
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
      .map { game =>
      game.fieldingLineup.flatMap { fieldingLineup =>
        game.battingLineup.map { battingLineup =>

          // Get the current batter and pitcher to show on form
          val nextBatter = battingLineup.entries.find(e => e.battingPos == game.state.nextBatterLineupPos && e.replacedById.isEmpty).get
          val pitcher = fieldingLineup.entries.find(e => e.battingPos == 0 && e.replacedById.isEmpty).get

          // Generate the batter ping
          val pingFormat = Users.withMap(_.preferences).findById(activeUser.id.get).get.preferences.withDefaults.umpBatterPing.get
          val batterPing = redditFormattingService.generateBatterPing(game, nextBatter, pitcher, pingFormat)

          // Display form
          val potentialStealers = Seq(game.state.r1, game.state.r2, game.state.r3)
            .filter(_.isDefined)
            .map(_.get)

          val nBatter = nextBatter.player.value
          val nPitcher = pitcher.player.value

          val discordPingMessage =
            if (game.id36.isEmpty) Some("Set Reddit Thread First")
            else if (nextBatter.player.user.discord.isEmpty) Some("Player Is Not Discord Verified")
            else if (DiscordPings.filter(dp => dp.receiverId === nextBatter.player.userId && dp.gameId === game.id.get && dp.gameStateId === game.state.id).singleOpt.isDefined) Some("Already Pinged")
            else None

          val existingPing = RedditPings.filter(rp => rp.receiverId === nextBatter.player.userId && rp.gameId === game.id.get && rp.gameStateId === game.state.id).singleOpt
          val redditPingMessage: Option[(String, String)] =
            if (game.id36.isEmpty) Some("secondary", "Set Reddit Thread First")
            else if (activeUser.refreshToken.isEmpty) Some("secondary", "Grant Post Permissions First")
            else if (existingPing.exists(_.result == 200)) Some("secondary", existingPing.get.message)
            else if (existingPing.isDefined) Some("danger", existingPing.get.message)
            else None

          var availablePlayTypes = PlayType.values.toSeq.sortBy(identity)(PlayType.ValueOrdering)
          if (potentialStealers.size < 2)
            availablePlayTypes = availablePlayTypes.filterNot(_ == PlayType.MULTI_STEAL)
          if (potentialStealers.isEmpty)
            availablePlayTypes = availablePlayTypes.filterNot(_ == PlayType.STEAL)
          if (game.state.r3.isEmpty)
            availablePlayTypes = availablePlayTypes.filterNot(_ == PlayType.INFIELD_IN)

          Ok(views.html.umpires.newaction(game, discordPingMessage, redditPingMessage, nBatter, potentialStealers, nPitcher, availablePlayTypes, newGameActionForm, "", batterPing, None))

        }
      } getOrElse {
        BadRequest("Please submit the lineups before starting the game.")
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitNewGameActionForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit req =>
    fullyLoadGame(gameId)
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
      .map { game =>
      game.fieldingLineup.flatMap { fieldingLineup =>
        game.battingLineup.map { battingLineup =>

          // Get the current batter and pitcher to show on form
          val nextBatter = battingLineup.entries.find(e => e.battingPos == game.state.nextBatterLineupPos && e.replacedById.isEmpty).get
          val pitcher = fieldingLineup.entries.find(e => e.battingPos == 0 && e.replacedById.isEmpty).get

          // Generate the batter ping
          val pingFormat = Users.withMap(_.preferences).findById(activeUser.id.get).get.preferences.withDefaults.umpBatterPing.get
          val batterPing = redditFormattingService.generateBatterPing(game, nextBatter, pitcher, pingFormat)

          // Display form
          val potentialStealers = Seq(game.state.r1, game.state.r2, game.state.r3)
            .filter(_.isDefined)
            .map(_.get)

          val nBatter = nextBatter.player.value
          val nPitcher = pitcher.player.value

          val discordPingMessage =
            if (game.id36.isEmpty) Some("Set Reddit Thread First")
            else if (nextBatter.player.user.discord.isEmpty) Some("Player Is Not Discord Verified")
            else if (DiscordPings.filter(dp => dp.receiverId === nextBatter.player.userId && dp.gameId === game.id.get && dp.gameStateId === game.state.id).singleOpt.isDefined) Some("Already Pinged")
            else None

          val existingPing = RedditPings.filter(rp => rp.receiverId === nextBatter.player.userId && rp.gameId === game.id.get && rp.gameStateId === game.state.id).singleOpt
          val redditPingMessage: Option[(String, String)] =
            if (game.id36.isEmpty) Some("secondary", "Set Reddit Thread First")
            else if (activeUser.refreshToken.isEmpty) Some("secondary", "Grant Post Permissions First")
            else if (existingPing.exists(_.result == 200)) Some("secondary", existingPing.get.message)
            else if (existingPing.isDefined) Some("danger", existingPing.get.message)
            else None

          var availablePlayTypes = PlayType.values.toSeq.sortBy(identity)(PlayType.ValueOrdering)
          if (potentialStealers.size < 2)
            availablePlayTypes = availablePlayTypes.filterNot(_ == PlayType.MULTI_STEAL)
          if (potentialStealers.isEmpty)
            availablePlayTypes = availablePlayTypes.filterNot(_ == PlayType.STEAL)
          if (game.state.r3.isEmpty)
            availablePlayTypes = availablePlayTypes.filterNot(_ == PlayType.INFIELD_IN)

          newGameActionForm.bindFromRequest.fold(
            formWithErrors => BadRequest(views.html.umpires.newaction(game, discordPingMessage, redditPingMessage, nBatter, potentialStealers, nPitcher, availablePlayTypes, formWithErrors, "", batterPing, None)),
            formData => {
              // Make sure batter and pitcher are correct (if submitted wrong, the form may have been submitted by another umpire)
              if (formData.playType != PlayType.STEAL && formData.playType != PlayType.MULTI_STEAL && formData.batter.id.get != nextBatter.player.id.get)
                BadRequest(views.html.umpires.newaction(game, discordPingMessage, redditPingMessage, nBatter, potentialStealers, nPitcher, availablePlayTypes, newGameActionForm.withGlobalError("This is not the current batter. The game log may have been updated while you were on this page. Please return and try again."), "", batterPing, None))
              else if ((formData.playType == PlayType.STEAL || formData.playType == PlayType.MULTI_STEAL) && formData.batter.id.get == nextBatter.player.id.get)
                BadRequest(views.html.umpires.newaction(game, discordPingMessage, redditPingMessage, nBatter, potentialStealers, nPitcher, availablePlayTypes, newGameActionForm.withGlobalError("The batter cannot be the stealer. The game log may have been updated while you were on this page. Please return and try again."), "", batterPing, None))
              else if (formData.pitcher.id.get != pitcher.player.id.get)
                BadRequest(views.html.umpires.newaction(game, discordPingMessage, redditPingMessage, nBatter, potentialStealers, nPitcher, availablePlayTypes, newGameActionForm.withGlobalError("This is not the current pitcher. The game log may have been updated while you were on this page. Please return and try again."), "", batterPing, None))
              else {
                var (newAction, newState, scoringPlay, minRange, maxRange): (PartialGameAction, GameState, Option[ScoringPlay], Option[Int], Option[Int]) = formData.playType match {
                  case PlayType.SWING | PlayType.INFIELD_IN => calc.handleSwing(game.state, nPitcher, formData.pitch.get, nBatter, formData.swing.get, game.park, formData.playType == PlayType.INFIELD_IN, formData.playResult)
                  case PlayType.AUTO_K => calc.handleAutoK(game.state, nPitcher, nBatter)
                  case PlayType.AUTO_BB => calc.handleAutoBB(game.state, nPitcher, nBatter)
                  case PlayType.BUNT => calc.handleBunt(game.state, nPitcher, formData.pitch.get, nBatter, formData.swing.get)
                  case PlayType.STEAL => calc.handleSteal(game.state, nPitcher, formData.pitch.get, potentialStealers.find(_.id.contains(formData.batter.id.get)).get, formData.swing.get)
                  case PlayType.MULTI_STEAL => calc.handleMultiSteal(game.state, nPitcher, formData.pitch.get, potentialStealers.find(_.id.contains(formData.batter.id.get)).get, formData.swing.get)
                  case PlayType.IBB => calc.handleIBB(game.state, nPitcher, nBatter)
                }

                if (formData.save) {
                  // Save the updated play in the database
                  val newStateId = (GameStates returning GameStates.map(_.id) += newState).run
                  val newGameAction = newAction.toGameAction(game, newStateId)
                  val newGameActionId = (GameActions returning GameActions.map(_.id) += newGameAction).run
                  Games.filter(_.id === game.id).map(_.stateId).update(newStateId).run
                  GameActions.filter(ga => ga.gameId === game.id && ga.replacedById.isEmpty && ga.id =!= newGameActionId).map(_.replacedById).update(Some(newGameActionId)).run
                  scoringPlay.foreach { sp => (ScoringPlays += sp.copy(gameId = game.id.get, gameActionId = newGameActionId)).run }

                  val updatedGame = fullyLoadGame(gameId).get

                  nBatter.recalculateBattingStats(game.season)
                  scoringPlay.flatMap(_.scorer1Id).foreach(StatCalculatorService.recalculateBattingStats(_, game.season))
                  scoringPlay.flatMap(_.scorer2Id).foreach(StatCalculatorService.recalculateBattingStats(_, game.season))
                  scoringPlay.flatMap(_.scorer3Id).foreach(StatCalculatorService.recalculateBattingStats(_, game.season))
                  scoringPlay.flatMap(_.scorer4Id).foreach(StatCalculatorService.recalculateBattingStats(_, game.season))

                  Set(
                    Some(nPitcher.id.get), scoringPlay.flatMap(_.scorer1ResponsibilityId), scoringPlay.flatMap(_.scorer2ResponsibilityId), scoringPlay.flatMap(_.scorer3ResponsibilityId)).filter(_.isDefined)
                    .map(_.get)
                    .foreach(StatCalculatorService.recalculatePitchingStats(_, game.season))

                  // Post to webhooks
                  if (game.season > 4) {
                    val loadedAction = GameActions.filter(_.id === newGameActionId)
                        .withMap(_.batter)
                        .withMap(_.pitcher)
                        .withMap(_.beforeState)
                        .withMap(_.afterState)
                        .single
                    gameFeedService.postResult(game, loadedAction)
                  }

                  // Update the box score
                  game.id36.filter(_ => game.threadOwner.isDefined).foreach { id36 =>
                    val owner = game.threadOwner.get
                    owner.refreshToken.foreach { refreshToken =>
                      // Generate
                      val boxScore = redditFormattingService.generateBoxScore(updatedGame)

                      // Post new box score
                      val accessToken = redditAuthService.getAccessTokenFromRefresh(refreshToken)
                      redditPostService.editGameThread(accessToken, id36, boxScore)
                      redditAuthService.revokeToken(accessToken, "access_token")
                    }
                  }

                  // Check if we are ending the game or going to extras
                  var shouldEndGame = false
                  val inningFlipped = newState.inning != game.state.inning
                  val maxInnings = globalSettingsProvider.RULES_INNINGS * 2
                  if (newState.inning > maxInnings - 1) {
                    if (newState.inning % 2 == 0) { // Going into bottom
                      shouldEndGame = newState.scoreHome > newState.scoreAway
                    } else { // Going into top
                      shouldEndGame = inningFlipped && newState.scoreHome != newState.scoreAway
                    }

                    if (inningFlipped && !shouldEndGame && newState.inning > maxInnings) { // Going into extras

                      // Determine runners
                      val awayBatting = newState.inning % 2 == 1
                      val nextBattingLineup = fieldingLineup // Have to flip for the inning switch not being reflected yet
                      var bPosBack1 = ((if (awayBatting) newState.awayBattingPosition else newState.homeBattingPosition) + 7) % 9 + 1
                      var bPosBack2 = ((if (awayBatting) newState.awayBattingPosition else newState.homeBattingPosition) + 6) % 9 + 1
                      var bPosBack3 = ((if (awayBatting) newState.awayBattingPosition else newState.homeBattingPosition) + 5) % 9 + 1
                      if (bPosBack1 < 1)
                        bPosBack1 = 9 - bPosBack1
                      if (bPosBack2 < 1)
                        bPosBack2 = 9 - bPosBack2
                      if (bPosBack3 < 1)
                        bPosBack3 = 9 - bPosBack3
                      val (onFirst, onSecond, onThird): (Option[Player], Option[Player], Option[Player]) = if (newState.inning > maxInnings + 4) { // Going into 9th, add three runners
                        (
                          Some(nextBattingLineup.entries.find(e => e.battingPos == bPosBack3 && e.replacedById.isEmpty).get.player),
                          Some(nextBattingLineup.entries.find(e => e.battingPos == bPosBack2 && e.replacedById.isEmpty).get.player),
                          Some(nextBattingLineup.entries.find(e => e.battingPos == bPosBack1 && e.replacedById.isEmpty).get.player),
                        )
                      } else if (newState.inning > maxInnings + 2) { // Going into 8th, add two runners
                        (
                          None,
                          Some(nextBattingLineup.entries.find(e => e.battingPos == bPosBack2 && e.replacedById.isEmpty).get.player),
                          Some(nextBattingLineup.entries.find(e => e.battingPos == bPosBack1 && e.replacedById.isEmpty).get.player),
                        )
                      } else { // Going into 7th, add one runner
                        (
                          None,
                          Some(nextBattingLineup.entries.find(e => e.battingPos == bPosBack1 && e.replacedById.isEmpty).get.player),
                          None
                        )
                      }

                      // Add new runner-placing dummy action
                      GameStates.filter(_.id === newStateId)
                        .map(s => (s.r1Id, s.r2Id, s.r3Id))
                        .update((onFirst.flatMap(_.id), onSecond.flatMap(_.id), onThird.flatMap(_.id)))
                        .run
                    }
                  }

                  if (shouldEndGame) {
                    Games.filter(_.id === gameId).map(g => (g.completed, g.winningTeamId)).update((true, if (newState.scoreAway > newState.scoreHome) game.awayTeam.id else game.homeTeam.id)).run
                    Redirect(routes.GameController.viewGame(gameId))
                  } else {
                    Redirect(routes.UmpireController.showNewGameActionForm(gameId))
                  }

                } else {
                  // Generate the preview and return to the form
                  var preview = formData.playType match {
                    case PlayType.AUTO_K => "Auto-K"
                    case PlayType.AUTO_BB => "Auto-BB"
                    case PlayType.STEAL | PlayType.MULTI_STEAL => "Steal"
                    case PlayType.BUNT => "Bunt"
                    case PlayType.IBB => "IBB"
                    case _ => "Swing"
                  }

                  preview += s": ${newAction.swing.map(_.toString).getOrElse("x")}  \n"
                  preview += s"Pitch: ${newAction.pitch.map(_.toString).getOrElse("x")}  \n"
                  preview += s"Diff: ${newAction.diff.map(_.toString).getOrElse("x")} -> "
                  if (newAction.result.contains("FO") && newAction.runsScored == 1) {
                    preview += "Sac"
                  } else if (newAction.outsTracked == 2) {
                    preview += s"${newAction.result} (DP)"
                  } else if (newAction.outsTracked == 3) {
                    preview += s"${newAction.result} (TP)"
                  } else {
                    preview += newAction.result
                  }

                  val rangeString = minRange.map(minRange => s"${newAction.result}: $minRange - ${maxRange.get}")
                  Ok(views.html.umpires.newaction(game, discordPingMessage, redditPingMessage, nBatter, potentialStealers, nPitcher, availablePlayTypes, newGameActionForm.fill(formData), preview, batterPing, rangeString))
                }
              }
            }
          )
        }
      } getOrElse {
        UnauthorizedPage
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showLineupForm(gameId: Int, team: String): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit req =>
    Games.filter(_.id === gameId)
        .withMap(_.awayTeam.nestSeq(_.players))
        .withMap(_.homeTeam.nestSeq(_.players))
        .withMapOpt(_.awayLineup.nestSeq(_.entries))
        .withMapOpt(_.homeLineup.nestSeq(_.entries))
        .withMapSeq(_.umpireAssignments)
        .singleOpt
        .filter(g => g.umpireAssignments.exists(_.umpireId == activeUser.id.get) && !g.completed)
        .flatMap { game =>
      (if (game.awayTeam.tag == team) Some(game.awayTeam) else if (game.homeTeam.tag == team) Some(game.homeTeam) else None).map { team =>
        var form = lineupForm(team, game.season == 2)
        (if (game.awayTeam.id == team.id) game.awayLineup.map(_.entries) else if (game.homeTeam.id == team.id) game.homeLineup.map(_.entries) else None).foreach { lineup =>
          form = form.bind(
            lineup
              .filter(e => e.replacedById.isEmpty)
              .flatMap { entry =>
                if (entry.battingPos == 0)
                  Map("pitcher" -> entry.playerId.toString)
                else
                  Map(
                    s"player${entry.battingPos}" -> entry.playerId.toString,
                    s"p${entry.battingPos}Pos" -> entry.position
                  )
              }.toMap
          )
        }
        Ok(views.html.umpires.editlineup(game, team, if (game.season == 2) Players.seq else team.players, form))
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitLineupForm(gameId: Int, team: String): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.state)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapOpt(_.awayLineup.nestSeq(_.entries))
      .withMapOpt(_.homeLineup.nestSeq(_.entries))
      .withMapSeq(_.umpireAssignments)
      .singleOpt
      .filter(g => g.umpireAssignments.exists(_.umpireId == activeUser.id.get) && !g.completed)
      .flatMap { game =>
        (if (game.awayTeam.tag == team) Some(game.awayTeam) else if (game.homeTeam.tag == team) Some(game.homeTeam) else None).map { team =>
          lineupForm(team, game.season == 2).bindFromRequest().fold(
            formWithErrors => BadRequest(views.html.umpires.editlineup(game, team, if (game.season == 2) Players.seq else Players.byTeam(team).seq, formWithErrors)),
            formData => {
              val currentLineup = if (game.awayTeam.id == team.id) game.awayLineup.map(_.entries) else if (game.homeTeam.id == team.id) game.homeLineup.map(_.entries) else None
              if (currentLineup.isDefined) {
                currentLineup.foreach { lineup =>
                  // Update lineup
                  val tupled = formData.tupled
                  val oldPitcher = lineup.find(e => e.battingPos == 0 && e.replacedById.isEmpty).get
                  if (oldPitcher.playerId != formData.pitcher.id.get) {
                    val newId = (LineupEntries returning LineupEntries.map(_.id) += LineupEntry(None, oldPitcher.lineupId, formData.pitcher.id.get, "P", 0)).run
                    LineupEntries.filter(_.id === oldPitcher.id).map(_.replacedById).update(Some(newId)).run
                  }
                  for (i <- tupled.indices) {
                    val oldEntry = lineup.find(e => e.battingPos == (i + 1) && e.replacedById.isEmpty).get
                    if (oldEntry.position == "PH" && oldEntry.playerId == tupled(i)._1.id.get) {
                      LineupEntries.filter(_.id === oldEntry.id).map(_.position).update(tupled(i)._2).run
                    } else if (oldEntry.playerId != tupled(i)._1.id.get || oldEntry.position != tupled(i)._2) {
                      val newId = (LineupEntries returning LineupEntries.map(_.id) += LineupEntry(None, oldEntry.lineupId, tupled(i)._1.id.get, tupled(i)._2, i + 1)).run
                      LineupEntries.filter(_.id === oldEntry.id).map(_.replacedById).update(Some(newId)).run
                    }

                    // check for existing baserunner
                    if (oldEntry.playerId != tupled(i)._1.id.get) {
                      if (game.state.r1Id.contains(oldEntry.playerId)) {
                        GameStates.filter(_.id === game.stateId).map(_.r1Id).update(Some(tupled(i)._1.id.get)).run
                      } else if (game.state.r2Id.contains(oldEntry.playerId)) {
                        GameStates.filter(_.id === game.stateId).map(_.r2Id).update(Some(tupled(i)._1.id.get)).run
                      } else if (game.state.r3Id.contains(oldEntry.playerId)) {
                        GameStates.filter(_.id === game.stateId).map(_.r3Id).update(Some(tupled(i)._1.id.get)).run
                      }
                    }
                  }
                }
              } else {
                // New lineup
                val newLineup = (Lineups returning Lineups.map(_.id) += Lineup()).run
                val tupled = formData.tupled
                (LineupEntries ++= tupled.indices.map(i => LineupEntry(None, newLineup, tupled(i)._1.id.get, tupled(i)._2, i + 1))).run
                (LineupEntries += LineupEntry(None, newLineup, formData.pitcher.id.get, "P", 0)).run
                Games.filter(_.id === gameId).map(g => if (game.awayTeam.id == team.id) g.awayLineupId else g.homeLineupId).update(Some(newLineup)).run
              }
              Redirect(routes.GameController.viewGame(gameId))
            }
          )
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showDeleteRecentPlayForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.state)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapSeq(_.actions.nest(_.batter).nest(_.pitcher))
      .withMapSeq(_.umpireAssignments)
      .singleOpt
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
      .map { game =>
        Ok(views.html.umpires.deleteaction(game, deleteActionForm))
      } getOrElse {
      UnauthorizedPage
    }
  }

  def deleteRecentPlay(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.state)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapSeq(_.actions.nest(_.batter).nest(_.pitcher))
      .withMapSeq(_.umpireAssignments)
      .singleOpt
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
      .map { game =>
        deleteActionForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.umpires.deleteaction(game, formWithErrors)),
          formData =>
            if (formData.id != game.lastAction.get.id.get)
              BadRequest(views.html.umpires.deleteaction(game, deleteActionForm))
            else {
              val ga = GameActions.filter(_.id === formData.id).singleQOpt
              ga.foreach { ga =>
                Games.filter(_.id === gameId).map(_.stateId).update(ga.beforeStateId).run
                GameActions.filter(_.replacedById === ga.id).map(_.replacedById).update(None).run
                ScoringPlays.filter(_.id === ga.id).delete.run
                GameActions.filter(_.id === ga.id).delete.run
              }
              Redirect(routes.GameController.viewGame(gameId))
            }
        )
      } getOrElse {
      UnauthorizedPage
    }
  }

  def generateBoxScore(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.state)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapOpt(_.awayLineup.nestSeq(_.entries.nest(_.player.nest(_.user).nestOpt(_.nickname).nestSeq(_.battingStats).nestSeq(_.pitchingStats))))
      .withMapOpt(_.homeLineup.nestSeq(_.entries.nest(_.player.nest(_.user).nestOpt(_.nickname).nestSeq(_.battingStats).nestSeq(_.pitchingStats))))
      .withMapSeq(_.actions.nest(_.beforeState).nest(_.afterState))
      .withMapSeq(_.scoringPlays.nest(_.gameAction.nest(_.beforeState).nest(_.afterState)))
      .withMapOpt(_.awards.nest(_.winningPitcher.nestOpt(_.nickname)).nest(_.losingPitcher.nestOpt(_.nickname)).nest(_.playerOfTheGame.nestOpt(_.nickname)).nestOpt(_.save.nestOpt(_.nickname)))
      .singleOpt
      .map { game =>
      // Generate
      val boxScore = redditFormattingService.generateBoxScore(game)
      Ok(views.html.umpires.boxscore(game, boxScore))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def postPing(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE) { implicit req =>
    req.body.asFormUrlEncoded.flatMap { body =>
      body.get("batter-ping").flatMap(_.headOption).flatMap { batterPing =>
        body.get("mode").flatMap(_.headOption).flatMap(m => Try(m.toInt).toOption).flatMap { mode =>
          Games.filter(_.id === gameId)
              .withMapOpt(_.awayLineup.nestSeq(_.entries.nest(_.player.nest(_.user))))
              .withMapOpt(_.homeLineup.nestSeq(_.entries.nest(_.player.nest(_.user))))
              .withMap(_.state)
              .withMapSeq(_.umpireAssignments.nest(_.umpire))
              .singleOpt
              .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
              .flatMap { game =>
            game.id36.flatMap { id36 =>
              game.battingLineup.map(_.entries).map { battingLineup =>
                val nextBatter = battingLineup.find(e => e.battingPos == game.state.nextBatterLineupPos && e.replacedById.isEmpty).get.player

                // Post to thread if needed
                if ((mode & 2) == 2) {
                  activeUser.refreshToken.foreach { refreshToken =>
                    val accessToken = redditAuthService.getAccessTokenFromRefresh(refreshToken)
                    val result = redditPostService.postReplyToThread(accessToken, id36, batterPing)
                    redditAuthService.revokeToken(accessToken, "access_token")

                    // Track result
                    val postedId36 = if (result.status == 200) (result.body \ "jquery" \ 18 \ 3 \ 0 \ 0 \ "data" \ "link_id").asOpt[String].map(_.substring(3)) else None
                    val message = if (result.status == 200) "At-Bat posted." else "Something went wrong."
                    val receiver = nextBatter.user.id.get
                    (RedditPings += RedditPing(None, activeUser.id.get, receiver, gameId, game.state.id.get, new Timestamp(System.currentTimeMillis()), result.status, message, postedId36)).run
                  }
                }

                // Post to discord if needed
                if ((mode & 1) == 1) {
                  nextBatter.user.discord.foreach { _ =>
                    discordMessageService.sendABAlert(nextBatter, game, globalSettingsProvider.LEAGUE_NAME.getOrElse("MLR"))
                    (DiscordPings += DiscordPing(None, activeUser.id.get, nextBatter.user.id.get, gameId, game.state.id.get, new Timestamp(System.currentTimeMillis()))).run
                  }
                }

                Redirect(routes.UmpireController.showNewGameActionForm(gameId))
              }
            }
          }
        }
      }
    } getOrElse {
      UnauthorizedPage
    }
  }

  def createGameThread(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.state)
      .withMap(_.awayTeam.nestSeq(_.games))
      .withMap(_.homeTeam.nestSeq(_.games))
      .withMapOpt(_.awayLineup.nestSeq(_.entries.nest(_.player.nest(_.user).nestOpt(_.nickname).nestSeq(_.battingStats).nestSeq(_.pitchingStats))))
      .withMapOpt(_.homeLineup.nestSeq(_.entries.nest(_.player.nest(_.user).nestOpt(_.nickname).nestSeq(_.battingStats).nestSeq(_.pitchingStats))))
      .withMapOpt(_.awards.nest(_.winningPitcher).nest(_.losingPitcher).nest(_.playerOfTheGame).nestOpt(_.save))
      .withMapSeq(_.actions.nest(_.beforeState).nest(_.afterState))
      .withMapSeq(_.scoringPlays.nest(_.gameAction.nest(_.beforeState).nest(_.afterState)))
      .withMapSeq(_.umpireAssignments.nest(_.umpire.nestOpt(_.player)))
      .singleOpt
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
      .flatMap { game =>
        activeUser.refreshToken.map { _ =>
          if (gameFlowService.createRedditThread(game, activeUser))
            Redirect(routes.GameController.viewGame(game.id.get))
          else
            InternalServerError("Something went wrong creating the thread. Please check reddit to see if it was created, and try again if necessary.")
        }
      } getOrElse {
      UnauthorizedPage
    }
  }

  def showRedditThreadForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapSeq(_.umpireAssignments.nest(_.umpire.nestOpt(_.player)))
      .singleOpt
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
      .map { game =>
        val form = game.id36.map { id36 =>
          setID36Form
            .bind(Map(
              "id36" -> id36,
              "owner" -> game.threadOwner.map(_.toString).getOrElse("")
            )).discardingErrors
        } getOrElse setID36Form
        Ok(views.html.umpires.setid36(game, form))
      } getOrElse {
      UnauthorizedPage
    }
  }

  def submitRedditThreadForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapSeq(_.umpireAssignments.nest(_.umpire.nestOpt(_.player)))
      .singleOpt
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
      .map { game =>
        setID36Form.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.umpires.setid36(game, formWithErrors)),
          formData => {
            Games.filter(_.id === gameId).map(g => (g.id36, g.threadOwnerId)).update((Some(formData.id36), formData.owner)).run
            Redirect(routes.GameController.viewGame(game.id.get))
          }
        )
      } getOrElse {
      UnauthorizedPage
    }
  }

  def showEditScoringPlaysForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapSeq(_.umpireAssignments.nest(_.umpire.nestOpt(_.player)))
      .withMapSeq(_.scoringPlays.nest(_.gameAction.nest(_.beforeState).nest(_.afterState)))
      .singleOpt
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
      .map { game =>
      Ok(views.html.umpires.editscoringplays(game, editScoringPlayForm))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitEditScoringPlayForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapSeq(_.umpireAssignments.nest(_.umpire.nestOpt(_.player)))
      .withMapSeq(_.scoringPlays.nest(_.gameAction.nest(_.beforeState).nest(_.afterState)))
      .singleOpt
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get))
      .map { game =>
      editScoringPlayForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.umpires.editscoringplays(game, formWithErrors)),
        formData => {
          ScoringPlays.filter(_.id === formData.gameAction).map(_.description).update(formData.newDescription).run
          Redirect(routes.UmpireController.showEditScoringPlaysForm(gameId))
        }
      )
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showGameAwardsForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapOpt(_.awards.nest(_.winningPitcher).nest(_.losingPitcher).nest(_.playerOfTheGame).nestOpt(_.save))
      .withMapOpt(_.awayLineup.nestSeq(_.entries.nest(_.player)))
      .withMapOpt(_.homeLineup.nestSeq(_.entries.nest(_.player)))
      .withMapSeq(_.umpireAssignments.nest(_.umpire.nestOpt(_.player)))
      .singleOpt
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get) || activeUser.isCommissioner)
      .map { game =>
        val form = gameAwardsForm
          .bind(Map(
            "winningPitcher" -> game.awards.map(_.winningPitcherId.toString).getOrElse(""),
            "losingPitcher" -> game.awards.map(_.losingPitcherId.toString).getOrElse(""),
            "playerOfTheGame" -> game.awards.map(_.playerOfTheGameId.toString).getOrElse(""),
            "save" -> game.awards.flatMap(_.saveId.map(_.toString)).getOrElse(""),
          ))
          .discardingErrors
        Ok(views.html.umpires.gameawards(game, form, game.awayLineup.get.entries ++ game.homeLineup.get.entries))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitGameAwardsForm(gameId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_UMPIRE || SCOPE_COMMISSIONER) { implicit req =>
    Games.filter(_.id === gameId)
      .withMap(_.awayTeam)
      .withMap(_.homeTeam)
      .withMapOpt(_.awards.nest(_.winningPitcher).nest(_.losingPitcher).nest(_.playerOfTheGame).nestOpt(_.save))
      .withMapOpt(_.awayLineup.nestSeq(_.entries.nest(_.player)))
      .withMapOpt(_.homeLineup.nestSeq(_.entries.nest(_.player)))
      .withMapSeq(_.umpireAssignments.nest(_.umpire.nestOpt(_.player)))
      .singleOpt
      .filter(_.umpireAssignments.exists(_.umpireId == activeUser.id.get) || activeUser.isCommissioner)
      .map { game =>
        gameAwardsForm.bindFromRequest.fold(
          formWithErrors => BadRequest(views.html.umpires.gameawards(game, formWithErrors, game.awayLineup.get.entries ++ game.homeLineup.get.entries)),
          formData => {
            val newAwards = GameAward(gameId, formData.winningPitcher.id.get, formData.losingPitcher.id.get, formData.playerOfTheGame.id.get, formData.save.flatMap(_.id))
            if (game.awards.isDefined)
              GameAwards.filter(_.id === gameId).update(newAwards).run
            else
              (GameAwards += newAwards).run
            Redirect(routes.GameController.viewGame(gameId))
          }
        )
    } getOrElse {
      UnauthorizedPage
    }
  }

}
