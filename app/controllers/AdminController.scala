package controllers

import java.text.SimpleDateFormat
import java.util.Date

import buildinfo.BuildInfo
import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import model.forms.AssignPlayerToTeamForm._
import model.forms.CreatePlayerForm._
import model.forms.DeleteGameForm._
import model.forms.EditParkForm._
import model.forms.EditPlayerForm._
import model.forms.EditTeamForm._
import model.forms.EndGameForm._
import model.forms.NewGameForm._
import model.forms.ScoreboardSessionForm
import model.forms.ScoreboardSessionForm._
import model.forms.SetGmForm._
import model.forms.UmpireAssignmentForm._
import model.forms.UmpireStatusForm._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import play.api.{Environment, Mode}
import services.{DiscordMessageService, GlobalSettingsProvider, ScoreboardUpdateService}
import slick.jdbc.MySQLProfile.api._

import scala.util.Try

@Singleton
class AdminController @Inject()(env: Environment, globalSettingsProvider: GlobalSettingsProvider, scoreboardUpdateService: ScoreboardUpdateService, discordMessageService: DiscordMessageService)(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def versions: (String, String, String) = (
    (if (env.mode == Mode.Dev) "Dev-" else "") + BuildInfo.gitSha,
    FlywaySchemaHistories.sortBy(_.installedRank.desc).singleQOpt.flatMap(_.version).getOrElse("Unknown"),
    new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(BuildInfo.buildTime))
  )

  def adminPage: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val pendingAppCount = PlayerApplications.filter(_.status === PlayerApplicationStatus.PENDING).size.result.run
    Ok(views.html.admin.index(pendingAppCount, versions))
  }

  def listPendingApplications: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val pendingApps = PlayerApplications.filter(_.status === PlayerApplicationStatus.PENDING).seq
    Ok(views.html.admin.applications(pendingApps))
  }

  def showNewGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.newgame(Teams.seqQ, newGameForm, Parks.seqQ, None))
  }

  def submitNewGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    newGameForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.newgame(Teams.seqQ, formWithErrors, Parks.seqQ, None)),
      formData => {
        val newStateId = (GameStates returning GameStates.map(_.id) += GameState()).run
        val newGameId = (Games returning Games.map(_.id) += Game(
          season = formData.season,
          session = formData.session,
          awayTeamId = formData.awayTeam.id.get,
          homeTeamId = formData.homeTeam.id.get,
          statsCalculated = formData.statsCalculation,
          stateId = newStateId,
          parkId = formData.park.getOrElse(formData.homeTeam.parkId)
        )).run

        val createdGame = Games.withMap(_.awayTeam).withMap(_.homeTeam).findById(newGameId).get
        (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_GAMES, s"Created game ${createdGame.name}")).run

        Created(views.html.admin.newgame(Teams.result.run, newGameForm, Parks.result.run, Some(createdGame)))
      }
    )
  }

  private def activeUmpires: Seq[User] = Users.filter(_.isUmpire).withMapOpt(_.player).seq
  def showUmpireStatusForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.manageumpires(activeUmpires, umpireStatusForm))
  }

  def submitUmpireStatusForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    umpireStatusForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.manageumpires(activeUmpires, formWithErrors)),
      formData => {
        Users.filter(_.redditName === formData.user)
          .map(_.isUmpire)
          .update(formData.newUmpStatus)
          .run

        AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_UMPIRES, if (formData.newUmpStatus) s"Added ${formData.user} to umpire list" else s"Removed ${formData.user} from umpire list")

        Redirect(routes.AdminController.showUmpireStatusForm())
      }
    )
  }

  private def activeAssignments(implicit db: FBDatabaseProvider) = UmpireAssignments.withMap(_.game.nest(_.awayTeam).nest(_.homeTeam))
    .withMap(_.umpire.nestOpt(_.player))
    .mapFilterNot(_._1._2._1._1.completed)
    .seq
  private def unfinishedGames = Games.withMap(_.awayTeam).withMap(_.homeTeam).withMap(_.state).mapFilterNot(_._1._1._1.completed).seq
  def showUmpireAssignmentForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.umpireassignments(activeUmpires, unfinishedGames, activeAssignments, umpireAssignmentForm))
  }

  def submitUmpireAssignmentForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    umpireAssignmentForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.umpireassignments(activeUmpires, unfinishedGames, activeAssignments, formWithErrors)),
      formData => {
        if (formData.addition) {
          (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_UMPIRES, s"Added umpire ${formData.umpire.redditName} to game ${formData.game.id}")).run
          (UmpireAssignments += UmpireAssignment(formData.game.id.get, formData.umpire.id.get)).run
        } else {
          (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_UMPIRES, s"Remove umpire ${formData.umpire.redditName} from game ${formData.game.id}")).run
          UmpireAssignments.filter(ua => ua.gameId === formData.game.id.get && ua.umpireId === formData.umpire.id.get).delete.run
        }
        Redirect(routes.AdminController.showUmpireAssignmentForm())
      }
    )
  }

  def viewEndGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.endgame(unfinishedGames, Teams.seq, endGameForm))
  }

  def submitEndGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    endGameForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.endgame(unfinishedGames, Teams.seq, formWithErrors)),
      formData => {
        Games.filter(_.id === formData.game.id)
            .map(g => (g.completed, g.winningTeamId))
            .update((true, formData.winningTeam.id))
            .run

        (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_GAMES, s"Closed game ${formData.game.id.get}")).run
        Redirect(routes.AdminController.adminPage())
      }
    )
  }

  private def allPlayerNames = Players.map(p => (p.id, p.name)).seqQ
  def showEditPlayerDirectory: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.editplayerlist(allPlayerNames))
  }

  def showEditPlayerForm(id: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    id.flatMap(Players.findById).map { player =>
      Ok(views.html.admin.editplayer(player, BattingTypes.seq, PitchingTypes.seq, PitchingBonuses.seq, editPlayerForm))
    } getOrElse {
      NotFound("This player does not exist.")
    }
  }

  def submitEditPlayerForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    editPlayerForm.bindFromRequest.fold(
      formWithErrors => {
        formWithErrors.data.get("player").flatMap(idStr => Try(idStr.toInt).toOption).flatMap { id =>
          Players.findById(id).map { player =>
            BadRequest(views.html.admin.editplayer(player, BattingTypes.seq, PitchingTypes.seq, PitchingBonuses.seq, formWithErrors))
          }
        } getOrElse {
          BadRequest(views.html.admin.editplayerlist(allPlayerNames))
        }
      },
      formData => {
        // Edit player in DB
        (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_PLAYERS, s"Edited player ${formData.player.id.get} => $formData")).run
        Players.filter(_.id === formData.player.id.get)
            .map(p => (p.name, p.firstName, p.lastName, p.battingTypeId, p.pitchingTypeId, p.pitchingBonusId, p.rightHanded, p.positionPrimary, p.positionSecondary, p.positionTertiary))
            .update((formData.fullName, formData.firstName, formData.lastName, formData.battingType.id.get, formData.pitchingType.flatMap(_.id), formData.pitchingBonus.flatMap(_.id), formData.rightHanded, formData.positionPrimary, formData.positionSecondary, formData.positionTertiary))
            .run

        // Return to main admin page
        Redirect(routes.AdminController.adminPage())
      }
    )
  }

  def deactivatePlayer(playerId: Int, newStatus: Boolean): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_PLAYERS, s"${if (newStatus) "Deactivated" else "Reactivated"} player $playerId")).run
    Players.filter(_.id === playerId)
        .map(_.deactivated)
        .update(newStatus)
        .run

    Redirect(routes.AdminController.showEditPlayerForm(Some(playerId)))
  }

  def showCreatePlayerForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.createplayer(createPlayerForm, BattingTypes.seq, PitchingTypes.seq, PitchingBonuses.seq))
  }

  def submitCreatePlayerForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    createPlayerForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.createplayer(formWithErrors, BattingTypes.seq, PitchingTypes.seq, PitchingBonuses.seq)),
      formData => {
        val user = Users.filter(_.redditName === formData.redditName).singleOpt
          .getOrElse((Users returning Users.map(_.id) into ((obj, id) => obj.copy(id = Some(id))) += User(redditName = formData.redditName, isPlayer = true)).run)
        val id = (Players returning Players.map(_.id) += Player(
          userId = user.id.get,
          name = formData.fullName,
          firstName = formData.firstName,
          lastName = formData.lastName,
          battingTypeId = formData.battingType.id.get,
          pitchingTypeId = formData.pitchingType.flatMap(_.id),
          pitchingBonusId = formData.pitchingBonus.flatMap(_.id),
          rightHanded = formData.rightHanded,
          positionPrimary = formData.positionPrimary,
          positionSecondary = formData.positionSecondary
        )).run
        (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_PLAYERS, s"Created player $id $formData")).run

        Redirect(routes.PlayerController.showPlayer(id))
      }
    )
  }

  def showEditParkSelector: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.editparkselector(Parks.seq))
  }

  def showEditParkForm(parkId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val park = parkId.flatMap(Parks.findById)
    Ok(views.html.admin.editpark(park, editParkForm))
  }

  def submitEditParkForm(parkId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val park = parkId.flatMap(Parks.findById)
    editParkForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.editpark(park, formWithErrors)),
      formData => {
        val newOrEditedPark = park.map { park =>
          (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_PARKS, s"Edited park $parkId $formData")).run
          Parks.filter(_.id === park.id)
              .map(p => (p.name, p.factorHR, p.factor3B, p.factor2B, p.factor1B, p.factorBB))
              .update((formData.name, formData.factorHR, formData.factor3B, formData.factor2B, formData.factor1B, formData.factorBB))
              .run

          park
        } getOrElse {
          val newPark = (Parks returning Parks.map(_.id) into ((obj, id) => obj.copy(id = Some(id))) += Park(
            name = formData.name,
            factorHR = formData.factorHR,
            factor3B = formData.factor3B,
            factor2B = formData.factor2B,
            factor1B = formData.factor1B,
            factorBB = formData.factorBB
          )).run
          (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_PARKS, s"Created park ${newPark.id.get} $formData")).run

          newPark
        }
        Redirect(routes.AdminController.showEditParkForm(Some(newOrEditedPark.id.get)))
      }
    )
  }

  def showEditTeamSelector: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.teamselector(Teams.seq, routes.AdminController.showEditTeamForm(None), showNew = true))
  }

  def showEditTeamForm(teamId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val team = teamId.flatMap(Teams.findById)
    Ok(views.html.admin.editteam(team, editTeamForm, Parks.seq))
  }

  def submitEditTeamForm(teamId: Option[Int]): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val team = teamId.flatMap(Teams.findById)
    editTeamForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.editteam(team, formWithErrors, Parks.seq)),
      formData => {
        val newOrEditedTeam = team.map { team =>
          (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_TEAMS, s"Edited team $teamId $formData")).run
          Teams.filter(_.id === team.id)
            .map(t => (t.tag, t.name, t.parkId, t.discordRole, t.colorDiscord, t.colorRoster, t.colorRosterBG, t.visible, t.allPlayers, t.webhook, t.logoURL))
            .update((formData.tag, formData.name, formData.park.id.get, formData.discordRole, formData.colorDiscord, formData.colorRoster, formData.colorRosterBG, formData.visible, formData.allPlayers, formData.webhook, formData.logoURL))
            .run

          team
        } getOrElse {
          val newTeam = (Teams returning Teams.map(_.id) into ((obj, id) => obj.copy(id = Some(id))) += Team(
            tag = formData.tag,
            name = formData.name,
            parkId = formData.park.id.get,
            discordRole = formData.discordRole,
            colorDiscord = formData.colorDiscord,
            colorRoster = formData.colorRoster,
            colorRosterBG = formData.colorRosterBG,
            visible = formData.visible,
            allPlayers = formData.allPlayers,
            webhook = formData.webhook,
            logoURL = formData.logoURL,
          )).run
          (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_TEAMS, s"Created team ${newTeam.id.get} $formData")).run

          newTeam
        }
        Redirect(routes.AdminController.showEditTeamForm(Some(newOrEditedTeam.id.get)))
      }
    )
  }

  def redirectToCurrentGameHealthTable: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val maxSeason = Games.map(_.season).max.result.run.getOrElse(1)
    val maxSession = Games.filter(_.season === maxSeason).map(_.session).max.result.run.getOrElse(1)
    Redirect(routes.AdminController.showGameHealthTable(maxSeason, maxSession))
  }

  def showGameHealthTable(season: Int, session: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val games = Games.filter(g => g.season === season && g.session === session)
        .withMap(_.state)
        .withMap(_.awayTeam)
        .withMap(_.homeTeam)
        .withMapOpt(_.awards)
        .withMapSeq(_.actions)
        .seq
    val seasonOptions = Games.map(_.season).distinct.seqQ
    val sessionOptions = Games.filter(_.season === season).map(_.session).distinct.seqQ
    Ok(views.html.admin.umpcheck(games, (season, session), seasonOptions, sessionOptions))
  }

  def showSetGmTeamSelector: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.teamselector(Teams.seq, routes.AdminController.showSetGmForm(0)))
  }

  def showSetGmForm(teamId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Teams.withMapOpt(_.gmAssignment).findById(teamId).map { team =>
      Ok(views.html.admin.setgm(team, setGmForm, Players.byTeam(team).seq))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitSetGmForm(teamId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Teams.withMapOpt(_.gmAssignment).findById(teamId).map { team =>
      setGmForm.bindFromRequest().fold(
        formWithErrors => BadRequest(views.html.admin.setgm(team, formWithErrors, Players.byTeam(team).seq)),
        formData => {
          (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_TEAMS, s"Set player ${formData.newGM.id.get} as GM of team ${team.id.get}")).run

          // remove old gm
          GMAssignments.filter(gma => gma.teamId === team.id && gma.endSeason.isEmpty)
              .map(gma => (gma.endSeason, gma.endSession, gma.endReason))
              .update((Some(formData.season), Some(formData.session), formData.oldGMReason))
              .run
          Users.filter(_.id === team.gmAssignment.map(_.gmId)).map(_.isGM).update(false)

          // add new gm
          (GMAssignments += GMAssignment(
            teamId = team.id.get,
            gmId = formData.newGM.id.get,
            startSeason = formData.season,
            startSession = formData.session)
          ).run
          Users.filter(_.id === formData.newGM.id).map(_.isGM).update(true).run

          Redirect(routes.AdminController.showSetGmForm(teamId))
        }
      )
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showTeamAssignmentForm(playerId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Players.findById(playerId).map { player =>
      Ok(views.html.admin.assigntoteam(player, Teams.seq, assignPlayerToTeamForm))
    } getOrElse {
      UnauthorizedPage
    }
  }

  def submitTeamAssignmentForm(playerId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Players.withMap(_.user).withMapOpt(_.team).findById(playerId).map { player =>
      assignPlayerToTeamForm.bindFromRequest().fold(
        formWithErrors => BadRequest(views.html.admin.assigntoteam(player, Teams.seq, formWithErrors)),
        formData => {
          // set team in db
          (AdminLogs += AdminLog(None, activeUser.id.get, AdminLogActionType.MANAGE_PLAYERS, s"Assigned player ${player.id.get} to team ${formData.team.flatMap(_.id).getOrElse(0)}")).run
          TradeBlockEntries.filter(_.id === player.id).delete.run
          PlayerTeamAssignments.insertOrUpdate(PlayerTeamAssignment(player.id.get, formData.team.flatMap(_.id), formData.startSeason, formData.startSession)).run
          Players.filter(_.id === player.id).map(_.teamId).update(formData.team.flatMap(_.id)).run

          // Update discord roles if team changed
          if (player.teamId != formData.team.flatMap(_.id)) {
            player.user.discord.foreach { discordSnowflake =>
              // Remove old role
              val oldRole = player.team
                .flatMap(_.discordRole)
                .getOrElse(settingsProvider.DISCORD_ROLE_FREE_AGENT.getOrElse("0"))
              discordMessageService.roleUser(discordSnowflake, oldRole, remove = true)
              // Add new role
              val newRole = formData.team
                .flatMap(_.discordRole)
                .getOrElse(settingsProvider.DISCORD_ROLE_FREE_AGENT.getOrElse("0"))
              discordMessageService.roleUser(discordSnowflake, newRole, remove = false)
            }
          }

          Redirect(routes.PlayerController.showPlayer(player.id.get))
        }
      )
    } getOrElse {
      UnauthorizedPage
    }
  }

  def showScoreboardSessionForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    val currentSession = globalSettingsProvider.SCOREBOARD_SESSION.map(_.split("\\.").map(_.toInt))
    val form = currentSession.map { currentSession =>
      scoreboardSessionForm.fill(ScoreboardSessionForm(currentSession.head, currentSession(1)))
    } getOrElse {
      scoreboardSessionForm
    }

    Ok(views.html.admin.scoreboardsession(form))
  }

  def submitScoreboardSessionForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    scoreboardSessionForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.scoreboardsession(formWithErrors)),
      formData => {
        globalSettingsProvider.setScoreboardThread(None)
        globalSettingsProvider.setScoreboardSession((formData.season, formData.session))
        scoreboardUpdateService.updateScoreboard().map { id36 =>
          Redirect(s"https://redd.it/$id36")
        } getOrElse {
          InternalServerError("Something went wrong talking to Reddit. Please check the subreddit to see if the scoreboard was created.")
        }
      }
    )
  }

  private def deletableGames = Games.withMap(_.awayTeam).withMap(_.homeTeam).withMap(_.state)
    .mapFilter(gs => {
      val state = gs._2
      state.inning === 1 && state.awayBattingPosition === 1 && state.homeBattingPosition === 1 && state.scoreAway === 0 && state.scoreHome === 0
    }).seq
  def showDeleteGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>
    Ok(views.html.admin.deletegame(deletableGames, deleteGameForm))
  }

  def submitDeleteGameForm: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit ru =>

    deleteGameForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.admin.deletegame(deletableGames, formWithErrors)),
      formData => {
        UmpireAssignments.filter(_.gameId === formData.game.id.get).delete.run
        Games.filter(_.id === formData.game.id.get).delete.run

        Redirect(routes.AdminController.adminPage())
      }
    )
  }

  def showGlobalSettingsPage: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit req =>
    Ok(views.html.admin.settings(globalSettingsProvider.all.toSeq.sortBy(_._1)))
  }

  def submitGlobalSettingsPage: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit req =>
    req.body.asFormUrlEncoded.flatMap { body =>
      body.get("key").flatMap(_.headOption).flatMap { key =>
        body.get("value").flatMap(_.headOption).map { value =>
          globalSettingsProvider.setSetting(key, value)

          Redirect(routes.AdminController.showGlobalSettingsPage())
        }
      }
    } getOrElse {
      BadRequest("Malformed request.")
    }
  }

}
