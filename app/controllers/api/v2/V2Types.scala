package controllers.api.v2

import play.api.libs.json._
import play.api.libs.json.Json._
import play.api.libs.functional.syntax._
import model._

object V2Types {

  implicit val CalcRangeWrites: Writes[CalcRange] = (
    (__ \ "rangeHR").write[Int] and
    (__ \ "range3B").write[Int] and
    (__ \ "range2B").write[Int] and
    (__ \ "range1B").write[Int] and
    (__ \ "rangeBB").write[Int] and
    (__ \ "rangeFO").write[Int] and
    (__ \ "rangeK").write[Int] and
    (__ \ "rangePO").write[Int] and
    (__ \ "rangeRGO").write[Int] and
    (__ \ "rangeLGO").write[Int]
  )(t => (t.rangeHR, t.range3B, t.range2B, t.range1B, t.rangeBB, t.rangeFO, t.rangeK, t.rangePO, t.rangeRGO, t.rangeLGO))

}
