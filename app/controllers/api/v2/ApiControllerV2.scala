package controllers.api.v2

import controllers._
import controllers.api.v2.V2Types._
import javax.inject.{Inject, Singleton}
import model.db._
import model.db.util._
import model.FBDatabaseProvider
import play.api.libs.json.{Json, Writes}
import play.api.mvc._
import services.{GlobalSettingsProvider, MLRCalculatorService}
import slick.jdbc.MySQLProfile.api._

@Singleton
class ApiControllerV2 @Inject()(implicit db: FBDatabaseProvider, cc: ControllerComponents, calcService: MLRCalculatorService, globalSettingsProvider: GlobalSettingsProvider) extends AbstractController(cc) {

  private def apiAction(body: Request[AnyContent] => Result): Action[AnyContent] = Action { implicit req =>
    body(req)
      .enableCors
  }

  private def apiAction(body: => Result): Action[AnyContent] = apiAction(_ => body)

  def ranges(batterId: Int, pitcherId: Int, parkId: Option[Int], infieldIn: Boolean): Action[AnyContent] = apiAction { implicit req =>
    val batter = Players.withTypes.withMap(_.user).withMapOpt(_.team).findById(batterId)

    val pitcher = Players.withTypes.withMap(_.user).withMapOpt(_.team).findById(pitcherId)

    val park = Parks.findById(parkId.getOrElse(1))

    batter.map { batter =>
      pitcher.map { pitcher =>
        park.map { park =>
          val range = calcService
            .calculateRanges(batter, pitcher, park, infieldIn)
            .absolute

          Ok(Json.obj("batter" -> Json.toJsFieldJsValueWrapper(batter)(PlayerWithTypesWrite), "pitcher" -> Json.toJsFieldJsValueWrapper(pitcher)(PlayerWithTypesWrite), "park" -> Json.toJsFieldJsValueWrapper(park)(ParkWrites), "range" -> range))
        } getOrElse {
          BadRequest(Json.obj("error" -> "invalid park id"))
        }
      } getOrElse {
        BadRequest(Json.obj("error" -> "invalid pitcher id"))
      }
    } getOrElse {
      BadRequest(Json.obj("error" -> "invalid batter id"))
    }
  }

  private def newResultToLegacyResult(action: GameAction): String =
    if (action.outsTracked == 3)
      "TP"
    else if (action.outsTracked == 2)
      "DP"
    else if (action.result.contains("FO") && action.runsScored == 1)
      "Sac"
    else if (action.result.startsWith("SB"))
      "SB"
    else if (action.result.startsWith("CS"))
      "CS"
    else
      action.result

  private def gameStateToLegacyObc(state: GameState): Int =
    if (state.r1Id.isEmpty && state.r2Id.isEmpty && state.r3Id.isEmpty)
      0
    else if (state.r1Id.isDefined && state.r2Id.isEmpty && state.r3Id.isEmpty)
      1
    else if (state.r1Id.isEmpty && state.r2Id.isDefined && state.r3Id.isEmpty)
      2
    else if (state.r1Id.isEmpty && state.r2Id.isEmpty && state.r3Id.isDefined)
      3
    else if (state.r1Id.isDefined && state.r2Id.isDefined && state.r3Id.isEmpty)
      4
    else if (state.r1Id.isDefined && state.r2Id.isEmpty && state.r3Id.isDefined)
      5
    else if (state.r1Id.isEmpty && state.r2Id.isDefined && state.r3Id.isDefined)
      6
    else
      7

  private def actionsToCsv(actions: Seq[GameAction]): String =
    "Game,Inning,Play ID,Batter,Batter ID,Swing,Pitcher,Pitcher ID,Pitch,Classic Result,Exact Result,Diff,Outs,OBC\n" +
      actions.map(a => s"${a.game.id.get},${a.beforeState.inningStr},${a.id.get},${a.batter.fullName},${a.batterId},${a.swing.map(_.toString).getOrElse("x")},${a.pitcher.fullName},${a.pitcherId},${a.pitch.map(_.toString).getOrElse("x")},${newResultToLegacyResult(a)},${a.result},${a.diff.map(_.toString).getOrElse("x")},${a.beforeState.outs},${gameStateToLegacyObc(a.beforeState)}")
        .mkString("\n")

  def playerPitchingPlays(id: Int, csv: Boolean): Action[AnyContent] = apiAction { implicit req =>
    val actions = GameActions.filter(_.pitcherId === id)
      .withMap(_.game.nest(_.awayTeam).nest(_.homeTeam))
      .withMap(_.batter.nest(_.user))
      .withMap(_.pitcher.nest(_.user))
      .withMap(_.beforeState)
      .withMap(_.afterState)
      .seq

    if (csv) {
      Ok(actionsToCsv(actions))
    } else {
      Ok(Json.toJson(actions))
    }
  }

  def playerBattingPlays(id: Int, csv: Boolean): Action[AnyContent] = apiAction { implicit req =>
    val actions = GameActions.filter(_.batterId === id)
      .withMap(_.game.nest(_.awayTeam).nest(_.homeTeam))
      .withMap(_.batter.nest(_.user))
      .withMap(_.pitcher.nest(_.user))
      .withMap(_.beforeState)
      .withMap(_.afterState)
      .seq

    if (csv) {
      Ok(actionsToCsv(actions))
    } else {
      Ok(Json.toJson(actions))
    }
  }

  case class LeagueInfo(bot_official: Option[String], channel_ab_ping: Option[String], channel_complaints: Option[String], channel_draft: Option[String], channel_game_threads: Option[String], channel_proposals: Option[String], channel_scoreboard: Option[String], channel_shitpost: Option[Seq[String]], channel_ump_ping: Option[String], channel_webhook: Option[String], draft_timer_seconds: Option[Int], league_admins: Option[Seq[String]], league_discord: Option[String], league_name: Option[String], league_umpires: Option[Seq[String]], link_rulebook: Option[String], link_umpire_handbook: Option[String], max_season: Int, max_session: Int, role_free_agent: Option[String], scoreboard_session: Option[String], website: String)
  implicit val LeagueInfoWrites: Writes[LeagueInfo] = Json.writes[LeagueInfo]

  def configInfo: Action[AnyContent] = apiAction { implicit req =>
    val maxSeason = Games.map(_.season).max.result.run.getOrElse(1)
    val maxSession = Games.filter(_.season === maxSeason).map(_.session).max.result.run.getOrElse(1)
    Ok(
      Json.toJson(
        LeagueInfo(
          bot_official = globalSettingsProvider.DISCORD_BOT_OFFICIAL,
          channel_ab_ping = globalSettingsProvider.DISCORD_CHANNEL_AB_PING,
          channel_complaints = globalSettingsProvider.DISCORD_CHANNEL_COMPLAINTS,
          channel_draft = globalSettingsProvider.DISCORD_CHANNEL_DRAFT,
          channel_game_threads = globalSettingsProvider.DISCORD_CHANNEL_GAME_THREADS,
          channel_proposals = globalSettingsProvider.DISCORD_CHANNEL_PROPOSALS,
          channel_scoreboard = globalSettingsProvider.DISCORD_CHANNEL_SCOREBOARD,
          channel_shitpost = globalSettingsProvider.DISCORD_CHANNEL_SHITPOST.map(_.split(",")),
          channel_ump_ping = globalSettingsProvider.DISCORD_CHANNEL_UMP_PING,
          channel_webhook = globalSettingsProvider.DISCORD_CHANNEL_WEBHOOK,
          draft_timer_seconds = globalSettingsProvider.DISCORD_CHANNEL_DRAFT_SECONDS.map(_.toInt),
          league_admins = Some(Users.filter(u => u.isCommissioner && u.discord.isDefined).map(_.discord.get).seqQ),
          league_discord = globalSettingsProvider.DISCORD_SERVER,
          league_name = globalSettingsProvider.LEAGUE_NAME,
          league_umpires = Some(Users.filter(u => u.isUmpire && u.discord.isDefined).map(_.discord.get).seqQ),
          link_rulebook = globalSettingsProvider.DISCORD_LINK_RULEBOOK,
          link_umpire_handbook = globalSettingsProvider.DISCORD_LINK_UMPIRE_HANDBOOK,
          max_season = maxSeason,
          max_session = maxSession,
          role_free_agent = globalSettingsProvider.DISCORD_ROLE_FREE_AGENT,
          scoreboard_session = globalSettingsProvider.SCOREBOARD_SESSION,
          website = s"https://${req.host}",
        )
      )
    )
  }

}
