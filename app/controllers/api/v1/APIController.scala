package controllers.api.v1

import controllers.{AuthenticatedController, _}
import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import play.api.libs.json._
import play.api.mvc._
import services.GlobalSettingsProvider
import slick.jdbc.MySQLProfile.api._

@Singleton
class APIController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  private def apiAction(body: Request[AnyContent] => Result): Action[AnyContent] = Action { implicit req =>
    body(req)
      .enableCors
  }

  private def apiAction(body: => Result): Action[AnyContent] = apiAction(_ => body)

  def gamesInSession(season: Int, session: Int): Action[AnyContent] = apiAction {
    Ok(Json.toJson(Games.filter(g => g.season === season && g.session === session).withMap(_.state).withMap(_.awayTeam).withMap(_.homeTeam).seq)(Writes.seq(GameWithTeamsAndStateWrites)))
  }

  def getActiveGameForTeam(teamQuery: String): Action[AnyContent] = apiAction {
    Teams.filter(t => if (teamQuery.length == 3) t.tag === teamQuery else t.name like "%teamQuery%").singleOpt.flatMap { team =>
      Games.filter(g => (g.awayTeamId === team.id.get || g.homeTeamId === team.id.get) && !g.completed)
        .sortBy(g => (g.season, g.session))
        .withMap(_.state)
        .withMap(_.awayTeam)
        .withMap(_.homeTeam)
        .seq
        .lastOption
        .map { game =>
          Ok(Json.toJson(game)(GameWithTeamsAndStateWrites))
        }
    } getOrElse {
      NotFound("{\"error\": \"No game was found.\"}")
    }
  }

  def getGameLog(game: Int): Action[AnyContent] = apiAction {
    Ok(Json.toJson(
      GameActions.filter(_.gameId === game)
        .withMap(_.game.nest(_.park).nest(_.awayTeam).nest(_.homeTeam))
        .withMap(_.beforeState)
        .withMap(_.afterState)
        .withMap(_.batter.nest(_.user).nest(_.battingType).nestOpt(_.pitchingType).nestOpt(_.pitchingBonus))
        .withMap(_.pitcher.nest(_.user).nest(_.battingType).nestOpt(_.pitchingType).nestOpt(_.pitchingBonus))
        .seq
        .sortBy(_.id)
        .reverse
    ))
  }

  def searchPlayer(name: String): Action[AnyContent] = apiAction {
    val players = Players.withMap(_.user)
      .mapFilter(p => (p._1.name like s"%$name%") || (p._2.redditName like s"%$name%"))
      .withTypes
      .withMapOpt(_.team)
      .seq
    Ok(Json.toJson(players)(Writes.seq(PlayerWithTypesWrite)))
  }

  def getTeam(id: Int): Action[AnyContent] = apiAction {
    Teams.filter(_.id === id).singleOpt.map { team =>
      Ok(Json.toJson(team)(TeamWrites))
    } getOrElse {
      NotFound("{\"error\": \"No game was found.\"}")
    }
  }

  def searchTeam(query: String): Action[AnyContent] = apiAction {
    val teams = if (query.length == 3) Teams.filter(_.tag === query).withMap(_.park).seq else Teams.filter(t => t.name like s"%$query%").withMap(_.park).seq
    Ok(Json.toJson(teams)(Writes.seq(TeamWithParkWrites)))
  }

  def searchPark(query: String): Action[AnyContent] = apiAction {
    val parks = Parks.joinLeft(Teams).on(_.id === _.parkId).filter(p => if (query.length == 3) p._2.map(_.tag === query) else (p._1.name like s"%$query%") || (p._2.map(_.name) like s"%$query%")).seqQ.map(_._1)
    Ok(Json.toJson(parks)(Writes.seq(ParkWrites)))
  }

  def playerBattingPlays(id: Int): Action[AnyContent] = apiAction {
    val plays: Seq[GameAction] = Players.withMapSeq(_.battingPlays.nest(_.game.nest(_.state).nest(_.awayTeam).nest(_.homeTeam)).nest(_.batter.nest(_.user)).nest(_.pitcher.nest(_.user)).nest(_.beforeState).nest(_.afterState)).findById(id).map(_.battingPlays.value).getOrElse(Seq())
    Ok(Json.toJson(plays.sortBy(_.id)))
  }

  def playerPitchingPlays(id: Int): Action[AnyContent] = apiAction {
    val plays: Seq[GameAction] = Players.withMapSeq(_.pitchingPlays.nest(_.game.nest(_.state).nest(_.awayTeam).nest(_.homeTeam)).nest(_.batter.nest(_.user)).nest(_.pitcher.nest(_.user)).nest(_.beforeState).nest(_.afterState)).findById(id).map(_.pitchingPlays.value).getOrElse(Seq())
    Ok(Json.toJson(plays.sortBy(_.id)))
  }

  def playerFromSnowflake(snowflake: String): Action[AnyContent] = apiAction {
    Players.withMap(_.user)
      .mapFilter(_._2.discord === snowflake)
      .withTypes
      .withMapOpt(_.team)
      .singleOpt
      .map { player =>
      Ok(Json.toJson(player)(PlayerWithTypesWrite))
    } getOrElse {
      NotFound("{\"error\": \"No player was found.\"}")
    }
  }

  def playersOnTeam(team: String): Action[AnyContent] = apiAction {
    Teams.filter(_.tag === team)
      .withMapSeq(_.players.nest(_.user).nestOpt(_.team).nest(_.battingType).nestOpt(_.pitchingType).nestOpt(_.pitchingBonus))
      .singleOpt
      .map { team =>
      Ok(Json.toJson(team.players.sortWith(Players.positionSort))(Writes.seq(PlayerWithTypesWrite)))
    } getOrElse {
      Ok(Json.toJson(Seq[String]()))
    }
  }

  def playerWithStats(id: Int, season: Option[Int]): Action[AnyContent] = apiAction {
    Players
        .withMap(_.user)
        .withMapSeq(_.battingStats)
        .withMapSeq(_.pitchingStats)
        .findById(id)
        .map { player =>
          val statSeason = season.getOrElse(Games.map(_.season).max.result.run.getOrElse(1))
      Ok(Json.toJson(player)(PlayerWithStatsWrite(statSeason)))
    } getOrElse {
      NotFound("{\"error\": \"No player was found.\"}")
    }
  }

}
