package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider
import slick.jdbc.MySQLProfile.api._

@Singleton
class HomeController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def index: Action[AnyContent] = messagesActionBuilder { implicit req =>
    val message = settingsProvider.INDEX_MESSAGE.getOrElse("No message yet.")
    val newPlayer = activeUserOpt.exists(u => !Players.filter(_.userId === u.id).exists.result.run && !PlayerApplications.filter(_.userId === u.id).exists.result.run)
    val discordInvite = settingsProvider.DISCORD_INVITE
    Ok(views.html.index(message, newPlayer, discordInvite))
  }

}
