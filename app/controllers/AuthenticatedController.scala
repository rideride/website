package controllers

import filters.AuthenticationFilter.ReqExtensions
import model.FBDatabaseProvider
import model.db._
import play.api.mvc._

import scala.language.implicitConversions

abstract class AuthenticatedController(implicit db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AbstractController(cc) {

  type Scope = User => Boolean
  val SCOPE_PLAYER: Scope = _.isPlayer
  val SCOPE_UMPIRE: Scope = _.isUmpire
  val SCOPE_COMMISSIONER: Scope = _.isCommissioner
  val SCOPE_GM: Scope = _.isGM

  implicit class ScopeExtensions(scope: Scope) {
    def ||(otherScope: Scope): Scope = u => scope(u) || otherScope(u)
    def &&(otherScope: Scope): Scope = u => scope(u) && otherScope(u)
  }

  implicit def activeUserOpt(implicit req: Request[AnyContent]): Option[User] = req.user
  implicit def activeUser(implicit req: Request[AnyContent]): User = req.user.get

  def UnauthorizedPage(implicit req: MessagesRequest[AnyContent]): Result = Unauthorized(views.html.errors.unauthorized())

  def UserAuthenticatedAction(requiredScopes: Scope*)(body: MessagesRequest[AnyContent] => Result): Action[AnyContent] = messagesActionBuilder { implicit req: MessagesRequest[AnyContent] =>
    activeUserOpt.map { user =>
      if (requiredScopes.forall(_ (user)))
        body(req)
      else
        UnauthorizedPage
    } getOrElse {
      Redirect(routes.AuthController.redirectToSignIn(req.path, mobile = true))
    }
  }

}
