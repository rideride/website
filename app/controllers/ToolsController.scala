package controllers

import akka.util.ByteString
import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import play.api.http.HttpEntity
import play.api.libs.json.{Json, Writes}
import play.api.mvc._
import services.GlobalSettingsProvider

@Singleton
class ToolsController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def calculator: Action[AnyContent] = messagesActionBuilder { implicit req =>
    Ok(views.html.tools.calc(Players.seq, BattingTypes.seq, PitchingTypes.seq, PitchingBonuses.seq, Parks.withMapSeq(_.teams).seq)(req, activeUserOpt))
  }

  def calcData: Action[AnyContent] = messagesActionBuilder { implicit req =>
    val allPlayers = Json.toJson(Players.withMap(_.user).seq)(Writes.seq(PlayerWithUserIncludingTypeIdsWrite)).toString()
    val allBattingTypes = Json.toJson(BattingTypes.seq).toString()
    val allPitchingTypes = Json.toJson(PitchingTypes.seq).toString()
    val allPitchingBonuses = Json.toJson(PitchingBonuses.seq).toString()
    val allParks = Json.toJson(Parks.seq).toString()
    val resp =
      s"""
         |window.fakebaseball = {};
         |window.fakebaseball.players = $allPlayers;
         |window.fakebaseball.battingTypes = $allBattingTypes;
         |window.fakebaseball.pitchingTypes = $allPitchingTypes;
         |window.fakebaseball.pitchingBonuses = $allPitchingBonuses;
         |window.fakebaseball.parks = $allParks;
       """.stripMargin
    Result(ResponseHeader(200), HttpEntity.Strict(ByteString(resp), Some("application/javascript")))
  }

}
