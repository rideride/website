package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.{GlobalSettingsProvider, StatCalculatorService}
import slick.jdbc.MySQLProfile.api._

@Singleton
class PlayerController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def myPlayer: Action[AnyContent] = UserAuthenticatedAction() { implicit ru =>
    Players.filter(_.userId === activeUser.id.get).singleOpt.map { player =>
      Redirect(routes.PlayerController.showPlayer(player.id.get))
    } getOrElse {
      PlayerApplications.filter(_.userId === activeUser.id.get).singleOpt.map { app =>
        Redirect(routes.PlayerApplicationController.showApplication(app.id.get))
      } getOrElse {
        Redirect(routes.PlayerApplicationController.showNewPlayerForm())
      }
    }
  }

  def showPlayer(id: Int): Action[AnyContent] = messagesActionBuilder { implicit req =>
    Players
      .filter(_.id === id)
      .withMapOpt(_.nickname)
      .withMapOpt(_.team)
      .withTypes
      .withMap(_.user)
      .withMapSeq(_.battingStats)
      .withMapSeq(_.pitchingStats)
      .singleOpt
      .map { player =>
        Ok(views.html.players.view(player)(req, activeUserOpt))
      } getOrElse {
      NotFound("This player does not exist.")
    }
  }

}
