package controllers

import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.{DiscordMessageService, GlobalSettingsProvider}

@Singleton
class VoteController @Inject()(implicit settingsProvider: GlobalSettingsProvider, discordMessageService: DiscordMessageService, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {

  def showVoteForm: Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    Ok(views.html.vote.closed())
//    if ({val p = db.getPlayerForUser(ru); p.isEmpty || p.exists(_.deactivated)})
//      UnauthorizedPage
//    val form = db.getVote(ru._2.id).map { vote =>
//      voteForm
//        .bind(Map(
//          "option1" -> vote.option1.toString,
//          "option2" -> vote.option2.toString,
//          "option3" -> vote.option3.toString,
//          "option4" -> vote.option4.toString,
//          "option5" -> vote.option5.toString,
//          "option6" -> vote.option6.toString,
//          "option7" -> vote.option7.toString,
//          "option8" -> vote.option8.toString,
//        ))
//    } getOrElse voteForm
//    Ok(views.html.vote.vote(form))
  }

  def submitVoteForm: Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    Ok(views.html.vote.closed())
//    voteForm.bindFromRequest.fold(
//      formWithErrors => BadRequest(views.html.vote.vote(formWithErrors)),
//      formData => {
//        db.submitVote(ru._2.id, formData)
//
//        ru._2.discord.foreach { snowflake =>
//          discordMessageService.roleUser(snowflake, "656271021434863628", remove = false)
//        }
//
//        Redirect(routes.VoteController.showPostVote())
//      }
//    )
  }

  def showPostVote: Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    Ok(views.html.vote.closed())
//    db.getVote(activeUser.id.get).map { vote =>
//      Ok(views.html.vote.post(vote))
//    } getOrElse {
//      UnauthorizedPage
//    }
  }

  def showTally: Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit req =>
    Ok(views.html.vote.closed())
//    val (votes, count) = db.getVoteTally
//    Ok(views.html.vote.tally(votes.toSeq.sortBy(_._2).reverse, count))
  }

}
