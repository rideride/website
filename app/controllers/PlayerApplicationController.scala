package controllers

import java.sql.Timestamp

import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import model.forms.NewPlayerForm
import model.forms.NewPlayerForm._
import model.forms.PlayerApplicationResponseForm._
import play.api.mvc.{Action, AnyContent, ControllerComponents, MessagesActionBuilder}
import services.GlobalSettingsProvider

@Singleton
class PlayerApplicationController @Inject()(implicit settingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, messagesActionBuilder: MessagesActionBuilder, cc: ControllerComponents) extends AuthenticatedController {
  import slick.jdbc.MySQLProfile.api._

  def showNewPlayerForm: Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    val form = PlayerApplications.filter(_.userId === activeUser.id.get).withMap(_.battingType).withMapOpt(_.pitchingType).withMapOpt(_.pitchingBonus).singleOpt.map { app =>
      newPlayerForm.fill(NewPlayerForm(
        app.isReturningPlayer,
        app.firstName,
        app.lastName,
        app.isWillingToJoinDiscord,
        app.discordName,
        app.positionPrimary,
        app.positionSecondary,
        app.isRightHanded,
        app.battingType,
        app.pitchingType,
        app.pitchingBonus,
        agreeToTerms = false
      )).discardingErrors
    } getOrElse {
      newPlayerForm
    }

    val btList = BattingTypes.seq
    val ptList = PitchingTypes.seq
    val pbList = PitchingBonuses.seq

    Ok(views.html.players.newplayer(form, btList, ptList, pbList))
  }

  def createOrEditNewPlayerApplication: Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    newPlayerForm.bindFromRequest.fold(
      formWithErrors => {
        val btList = BattingTypes.seq
        val ptList = PitchingTypes.seq
        val pbList = PitchingBonuses.seq
        BadRequest(views.html.players.newplayer(formWithErrors, btList, ptList, pbList))
      },
      formData => {
        val constructedApp = PlayerApplication(
          None,
          activeUser.id.get,
          formData.lastName,
          formData.firstName,
          formData.returning,
          formData.willingToJoinDiscord,
          formData.discordName,
          formData.positionPrimary,
          formData.positionSecondary,
          formData.rightHanded,
          formData.battingType.id.get,
          formData.pitchingType.flatMap(_.id),
          formData.pitchingBonus.flatMap(_.id),
          PlayerApplicationStatus.PENDING,
          None, None, None
        )
        val appId = PlayerApplications.filter(_.userId === activeUser.id.get).singleOpt.map { currentApp =>
          PlayerApplications.filter(_.userId === activeUser.id.get).update(constructedApp.copy(id = currentApp.id)).run
          currentApp.id.get
        } getOrElse {
          (PlayerApplications returning PlayerApplications.map(_.id) += constructedApp).run
        }
        Redirect(routes.PlayerApplicationController.showApplication(appId))
      }
    )
  }

  private def playerAppById(appId: Int): Option[PlayerApplication] =
    PlayerApplications
      .withMap(_.user)
      .withMap(_.battingType)
      .withMapOpt(_.pitchingType)
      .withMapOpt(_.pitchingBonus)
      .findById(appId)

  def showApplication(appId: Int): Action[AnyContent] = UserAuthenticatedAction() { implicit req =>
    playerAppById(appId)
      .filter(_.userId == activeUser.id.get || activeUser.isCommissioner)
      .map { app =>
      Ok(views.html.players.viewapplication(app, playerApplicationResponseForm))
    } getOrElse {
      BadRequest("You don't have permission to view that.")
    }
  }

  def respondToApplication(appId: Int): Action[AnyContent] = UserAuthenticatedAction(SCOPE_COMMISSIONER) { implicit req =>
    playerAppById(appId)
      .filter(_.userId == activeUser.id.get || activeUser.isCommissioner)
      .map { app =>
      playerApplicationResponseForm.bindFromRequest.fold(
        formWithErrors => Ok(views.html.players.viewapplication(app, formWithErrors)),
        formData => {
          if (!formData.approval) {
            PlayerApplications.filter(_.id === appId)
              .map(app => (app.respondedById, app.respondedAt, app.rejectionMessage, app.status))
              .update((Some(activeUser.id.get), Some(new Timestamp(System.currentTimeMillis())), formData.rejectionMessage, PlayerApplicationStatus.REJECTED))
              .run
          } else {
            PlayerApplications.filter(_.id === appId)
              .map(app => (app.respondedById, app.respondedAt, app.status))
              .update((Some(activeUser.id.get), Some(new Timestamp(System.currentTimeMillis())), PlayerApplicationStatus.ACCEPTED))
              .run
            (Players += Player(
              id = None,
              userId = app.user.id.get,
              name = app.fullName,
              firstName = app.firstName,
              lastName = app.lastName,
              battingTypeId = app.battingType.id.get,
              pitchingTypeId = app.pitchingType.flatMap(_.id),
              pitchingBonusId = app.pitchingBonus.flatMap(_.id),
              rightHanded = app.isRightHanded,
              positionPrimary = app.positionPrimary,
              positionSecondary = app.positionSecondary
            )).run
          }
          Redirect(routes.PlayerApplicationController.showApplication(appId))
        }
      )
    } getOrElse {
      BadRequest("This application does not exist.")
    }
  }

}
