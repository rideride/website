import model.db._
import model.db.util._
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Writes}
import play.api.mvc.Result

package object controllers {

  implicit class ResultExtensions(result: Result) {
    def enableCors: Result = result.withHeaders(
      "Access-Control-Allow-Origin" -> "*",
      "Access-Control-Allow-Methods" -> "GET, POST, PUT, PATCH, DELETE",
      "Access-Control-Allow-Headers" -> "Accept, Content-Type, Origin, X-Json, X-Prototype-Version, X-Requested-With",
      "Access-Control-Allow-Credentials" -> "true"
    )
  }

  // region JSON converters

  implicit val TeamWrites: Writes[Team] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "tag").write[String] and
      (JsPath \ "park").write[Int] and
      (JsPath \ "colorDiscord").write[Int] and
      (JsPath \ "colorRoster").write[Int] and
      (JsPath \ "colorRosterBG").write[Int] and
      (JsPath \ "discordRole").write[Option[String]] and
      (JsPath \ "logoURL").write[Option[String]]
    ) ((t: Team) => (t.id.get, t.name, t.tag, t.parkId, t.colorDiscord, t.colorRoster, t.colorRosterBG, t.discordRole, t.logoURL))
  implicit val ParkWrites: Writes[Park] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "factorHR").write[Double] and
      (JsPath \ "factor3B").write[Double] and
      (JsPath \ "factor2B").write[Double] and
      (JsPath \ "factor1B").write[Double] and
      (JsPath \ "factorBB").write[Double]
    ) ((p: Park) => (p.id.get, p.name, p.factorHR, p.factor3B, p.factor2B, p.factor1B, p.factorBB))
  implicit val ParkWithTeamWrites: Writes[(Park, Option[Team])] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "factorHR").write[Double] and
      (JsPath \ "factor3B").write[Double] and
      (JsPath \ "factor2B").write[Double] and
      (JsPath \ "factor1B").write[Double] and
      (JsPath \ "factorBB").write[Double] and
      (JsPath \ "team").write[Option[Team]](Writes.optionWithNull(TeamWrites))
    ) ((p: (Park, Option[Team])) => (p._1.id.get, p._1.name, p._1.factorHR, p._1.factor3B, p._1.factor2B, p._1.factor1B, p._1.factorBB, p._2))
  implicit val TeamWithParkWrites: Writes[Team] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "tag").write[String] and
      (JsPath \ "park").write[Park] and
      (JsPath \ "colorDiscord").write[Int] and
      (JsPath \ "colorRoster").write[Int] and
      (JsPath \ "colorRosterBG").write[Int] and
      (JsPath \ "discordRole").write[Option[String]] and
      (JsPath \ "logoURL").write[Option[String]]
    ) ((t: Team) => (t.id.get, t.name, t.tag, t.park, t.colorDiscord, t.colorRoster, t.colorRosterBG, t.discordRole, t.logoURL))
  implicit val GameWithTeamsAndStateWrites: Writes[Game] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "season").write[Int] and
      (JsPath \ "session").write[Int] and
      (JsPath \ "awayTeam").write[Team](TeamWrites) and
      (JsPath \ "homeTeam").write[Team](TeamWrites) and
      (JsPath \ "awayScore").write[Int] and
      (JsPath \ "homeScore").write[Int] and
      (JsPath \ "completed").write[Boolean] and
      (JsPath \ "outs").write[Int] and
      (JsPath \ "inning").write[String] and
      (JsPath \ "firstOccupied").write[Boolean] and
      (JsPath \ "secondOccupied").write[Boolean] and
      (JsPath \ "thirdOccupied").write[Boolean] and
      (JsPath \ "id36").write[Option[String]]
    ) ((g: Game) => (g.id.get, g.season, g.session, g.awayTeam, g.homeTeam, g.state.scoreAway, g.state.scoreHome, g.completed, g.state.outs, g.state.inningStr, g.state.r1Id.isDefined, g.state.r2Id.isDefined, g.state.r3Id.isDefined, g.id36))
  implicit val BattingTypeWrites: Writes[BattingType] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "shortcode").write[String] and
      (JsPath \ "rangeHR").write[Int] and
      (JsPath \ "range3B").write[Int] and
      (JsPath \ "range2B").write[Int] and
      (JsPath \ "range1B").write[Int] and
      (JsPath \ "rangeBB").write[Int] and
      (JsPath \ "rangeFO").write[Int] and
      (JsPath \ "rangeK").write[Int] and
      (JsPath \ "rangePO").write[Int] and
      (JsPath \ "rangeRGO").write[Int] and
      (JsPath \ "rangeLGO").write[Int]
    ) (bt => (bt.id.get, bt.name, bt.shortcode, bt.rangeHR, bt.range3B, bt.range2B, bt.range1B, bt.rangeBB, bt.rangeFO, bt.rangeK, bt.rangePO, bt.rangeRGO, bt.rangeLGO))
  implicit val PitchingTypeWrites: Writes[PitchingType] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "shortcode").write[String] and
      (JsPath \ "rangeHR").write[Int] and
      (JsPath \ "range3B").write[Int] and
      (JsPath \ "range2B").write[Int] and
      (JsPath \ "range1B").write[Int] and
      (JsPath \ "rangeBB").write[Int] and
      (JsPath \ "rangeFO").write[Int] and
      (JsPath \ "rangeK").write[Int] and
      (JsPath \ "rangePO").write[Int] and
      (JsPath \ "rangeRGO").write[Int] and
      (JsPath \ "rangeLGO").write[Int]
    ) (pt => (pt.id.get, pt.name, pt.shortcode, pt.rangeHR, pt.range3B, pt.range2B, pt.range1B, pt.rangeBB, pt.rangeFO, pt.rangeK, pt.rangePO, pt.rangeRGO, pt.rangeLGO))
  implicit val PitchingBonusWrites: Writes[PitchingBonus] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "shortcode").write[String] and
      (JsPath \ "rangeHR").write[Int] and
      (JsPath \ "range3B").write[Int] and
      (JsPath \ "range2B").write[Int] and
      (JsPath \ "range1B").write[Int] and
      (JsPath \ "rangeBB").write[Int] and
      (JsPath \ "rangeFO").write[Int] and
      (JsPath \ "rangeK").write[Int] and
      (JsPath \ "rangePO").write[Int] and
      (JsPath \ "rangeRGO").write[Int] and
      (JsPath \ "rangeLGO").write[Int]
    ) (pb => (pb.id.get, pb.name, pb.shortcode, pb.rangeHR, pb.range3B, pb.range2B, pb.range1B, pb.rangeBB, pb.rangeFO, pb.rangeK, pb.rangePO, pb.rangeRGO, pb.rangeLGO))
  implicit val PlayerWithTypesWrite: Writes[Player] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "firstName").write[Option[String]] and
      (JsPath \ "lastName").write[String] and
      (JsPath \ "redditName").write[String] and
      (JsPath \ "discordSnowflake").write[Option[String]] and
      (JsPath \ "team").write[Option[Team]](Writes.OptionWrites(TeamWrites)) and
      (JsPath \ "battingType").write[BattingType] and
      (JsPath \ "pitchingType").write[Option[PitchingType]] and
      (JsPath \ "pitchingBonus").write[Option[PitchingBonus]] and
      (JsPath \ "rightHanded").write[Boolean] and
      (JsPath \ "positionPrimary").write[String] and
      (JsPath \ "positionSecondary").write[Option[String]] and
      (JsPath \ "positionTertiary").write[Option[String]]
    ) (p => (p.id.get, p.name, p.firstName, p.lastName, p.user.redditName, p.user.discord, p.team, p.battingType, p.pitchingType, p.pitchingBonus, p.rightHanded, p.positionPrimary, p.positionSecondary, p.positionTertiary))
  implicit val PlayerWithUserWrite: Writes[Player] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "firstName").write[Option[String]] and
      (JsPath \ "lastName").write[String] and
      (JsPath \ "redditName").write[String]
    ) (p => (p.id.get, p.name, p.firstName, p.lastName, p.user.redditName))
  val PlayerWithUserIncludingTypeIdsWrite: Writes[Player] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "firstName").write[Option[String]] and
      (JsPath \ "lastName").write[String] and
      (JsPath \ "redditName").write[String] and
      (JsPath \ "battingType").write[Int] and
      (JsPath \ "pitchingType").write[Option[Int]] and
      (JsPath \ "pitchingBonus").write[Option[Int]] and
      (JsPath \ "rightHanded").write[Boolean]
  ) (p => (p.id.get, p.name, p.firstName, p.lastName, p.user.redditName, p.battingTypeId, p.pitchingTypeId, p.pitchingBonusId, p.rightHanded))
  implicit val GameWithTeamsWrite: Writes[Game] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "season").write[Int] and
      (JsPath \ "session").write[Int] and
      (JsPath \ "awayTeam").write[Team](TeamWrites) and
      (JsPath \ "homeTeam").write[Team](TeamWrites) and
      (JsPath \ "id36").write[Option[String]]
    ) (t => (t.id.get, t.season, t.session, t.awayTeam, t.homeTeam, t.id36))
  implicit val GameStateWrite: Writes[GameState] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "awayScore").write[Int] and
      (JsPath \ "homeScore").write[Int] and
      (JsPath \ "outs").write[Int] and
      (JsPath \ "inning").write[String] and
      (JsPath \ "firstOccupied").write[Boolean] and
      (JsPath \ "secondOccupied").write[Boolean] and
      (JsPath \ "thirdOccupied").write[Boolean]
    ) ((gs: GameState) => (gs.id.get, gs.scoreAway, gs.scoreHome, gs.outs, gs.inningStr, gs.r1Id.isDefined, gs.r2Id.isDefined, gs.r3Id.isDefined))
  implicit val GameActionWithExtendedInfoWrite: Writes[GameAction] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "game").write[Game](GameWithTeamsWrite) and
      (JsPath \ "batter").write[Player](PlayerWithUserWrite) and
      (JsPath \ "swing").write[Option[Int]] and
      (JsPath \ "pitcher").write[Player](PlayerWithUserWrite) and
      (JsPath \ "pitch").write[Option[Int]] and
      (JsPath \ "diff").write[Option[Int]] and
      (JsPath \ "result").write[String] and
      (JsPath \ "beforeState").write[GameState] and
      (JsPath \ "afterState").write[GameState] and
      (JsPath \ "outsTracked").write[Int] and
      (JsPath \ "runsScored").write[Int] and
      (JsPath \ "scorers").write[Option[String]]
    ) (ga => (ga.id.get, ga.game, ga.batter, ga.swing, ga.pitcher, ga.pitch, ga.diff, ga.result, ga.beforeState, ga.afterState, ga.outsTracked, ga.runsScored, ga.scorers))
  implicit lazy val BattingStatSetWrite: Writes[BattingStat] = obj => obj.toJson
  implicit lazy val PitchingStatSetWrite: Writes[PitchingStat] = obj => obj.toJson
  def PlayerWithStatsWrite(season: Int): Writes[Player] = (
    (JsPath \ "id").write[Int] and
      (JsPath \ "name").write[String] and
      (JsPath \ "firstName").write[Option[String]] and
      (JsPath \ "lastName").write[String] and
      (JsPath \ "redditName").write[String] and
      (JsPath \ "battingStats").write[Option[BattingStat]] and
      (JsPath \ "pitchingStats").write[Option[PitchingStat]]
    ) (p => (p.id.get, p.name, p.firstName, p.lastName, p.user.redditName, p.battingStats.find(_.season == season), p.pitchingStats.find(_.season == season)))

  // endregion

}
