package services

import javax.inject.{Inject, Singleton}
import model.db._
import play.api.mvc.{AnyContent, Request}

@Singleton
class RedditFormattingService @Inject()() {

  def generateBoxScore(game: Game)(implicit req: Request[AnyContent]): String = {
    val awayScore = game.state.scoreAway
    val homeScore = game.state.scoreHome

    // Generate the harder parts
    val line = generateLineScore(game)
    val box = generateLineupBox(game)
    val pitchers = generatePitcherBox(game)
    val scoringPlaysBox = generateScoringPlaysBox(game.scoringPlays)
    val awards = game.awards.map(generateAwardsSection)

    // Put it all together
   s"## ${game.awayTeam.tag} $awayScore - $homeScore ${game.homeTeam.tag}\n\n[View on Redditball](https://${req.host}/games/${game.id.get})\n\n${if (awards.isDefined) s"## AWARDS  \n${awards.get}\n\n" else ""}## LINE\n$line\n\n## BOX\n$box\n\n## PITCHERS\n$pitchers\n\n## SCORING PLAYS\n$scoringPlaysBox"
  }

  def generateAwardsSection(awards: GameAward): String = {
    var ret = s"**WP**: ${awards.winningPitcher.fullName}  \n" +
    s"**LP**: ${awards.losingPitcher.fullName}  \n" +
    s"**PotG**: ${awards.playerOfTheGame.fullName}  \n"
    awards.save.foreach { save =>
      ret += s"**SV**: ${save.fullName}"
    }
    ret
  }

  def generateLineScore(game: Game): String = {

    // Always present
    var line1 = "||"
    var line2 = "|:--|"
    var line3 = s"|**${game.awayTeam.tag}**|"
    var line4 = s"|**${game.homeTeam.tag}**|"

    // Track max inning played
    val maxInning = if (game.actions.isEmpty) 0 else game.actions.map(_.beforeState.inning).max

    // Loop all innings played, but at least 6
    for (i <- 1 to Math.max(maxInning, 12)) {
      // Determine if the current inning being added is the active inning
      val italics = i == maxInning && !game.completed
      // Add away score if odd, home score if even
      if (i % 2 == 1) {
        line1 += s"${(i + 1) / 2}|"
        line2 += ":--|"

        // Only add score content if we have reached that inning
        if (i <= maxInning)
          line3 += s"${if (italics) "*" else ""}${game.actions.filter(_.beforeState.inning == i).map(_.runsScored).sum}${if (italics) "*" else ""}|"
        else
          line3 += "|"
      } else {
        if (i <= maxInning)
          line4 += s"${if (italics) "*" else ""}${game.actions.filter(_.beforeState.inning == i).map(_.runsScored).sum}${if (italics) "*" else ""}|"
        else
          line4 += "|"
      }
    }

    line1 += "R|H|"
    line2 += ":--|:--|"
    line3 += s"${game.state.scoreAway}|${game.actions.count(p => p.beforeState.inning % 2 == 1 && p.isHit)}|"
    line4 += s"${game.state.scoreHome}|${game.actions.count(p => p.beforeState.inning % 2 == 0 && p.isHit)}|"

    // Return lines combined
    s"\n$line1\n$line2\n$line3\n$line4"
  }

  def generateLineupBox(game: Game): String = {
    val header = s"|\\#|${game.awayTeam.tag}|Pos|AB|R|H|RBI|BB|K|BA|\\#|${game.homeTeam.tag}|Pos|AB|R|H|RBI|BB|K|BA|\n" +
      "|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|"

    var lineupBox = ""
    for (i <- 1 to 9) {
      // Figure out how many rows we need for this batting position
      val aways = game.awayLineup.map(_.entries).map(_.filter(_.battingPos == i).sortBy(_.id))
      val homes = game.homeLineup.map(_.entries).map(_.filter(_.battingPos == i).sortBy(_.id))
      val rows = Math.max(aways.map(_.size).getOrElse(1), homes.map(_.size).getOrElse(1))

      // Now start putting in the players, or blank lines if none
      for (j <- 0 until rows) {
        val awayPlayer = aways.flatMap(_.lift(j))
        lineupBox += awayPlayer.map(entry => {
          s"|${if (entry.replacedById.isDefined) "~~" else ""}**$i**${if (entry.replacedById.isDefined) "~~" else ""}|${if (entry.replacedById.isDefined) "~~" else ""}[${entry.player.fullName}](${entry.player.user.redditName})${if (entry.replacedById.isDefined) "~~" else ""}|" +
            s"${entry.position}|${game.actions.count(p => p.batterId == entry.player.id.get && p.isAB)}|"+
            s"${game.actions.count(_.scorers.exists(_.split(",").map(_.toInt).contains(entry.player.id.get)))}|" +
            s"${game.actions.count(p => p.batterId == entry.player.id.get && p.isHit)}|" +
            s"${game.actions.filter(p => p.batterId == entry.player.id.get && p.outsTracked < 2).foldLeft(0)(_ + _.runsScored)}|" +
            s"${game.actions.count(p => p.batterId == entry.player.id.get && (p.playType == PlayType.IBB || p.playType == PlayType.AUTO_BB || p.result == "BB"))}|" +
            s"${game.actions.count(p => p.batterId == entry.player.id.get && (p.result == "K" || p.result == "Auto K" || p.result == "Bunt K"))}|" +
            s"${"%.3f".format(entry.player.battingStats.find(_.season == game.season).map(_.ba).getOrElse(0f))}|"
        }).getOrElse("|--|--|--|--|--|--|--|--|--|--|")
        val homePlayer = homes.flatMap(_.lift(j))
        lineupBox += homePlayer.map(entry => {
          s"${if (entry.replacedById.isDefined) "~~" else ""}**$i**${if (entry.replacedById.isDefined) "~~" else ""}|${if (entry.replacedById.isDefined) "~~" else ""}[${entry.player.fullName}](${entry.player.user.redditName})${if (entry.replacedById.isDefined) "~~" else ""}|" +
            s"${entry.position}|${game.actions.count(p => p.batterId == entry.player.id.get && p.isAB)}|"+
            s"${game.actions.count(_.scorers.exists(_.split(",").map(_.toInt).contains(entry.player.id.get)))}|" +
            s"${game.actions.count(p => p.batterId == entry.player.id.get && p.isHit)}|" +
            s"${game.actions.filter(p => p.batterId == entry.player.id.get && p.outsTracked < 2).foldLeft(0)(_ + _.runsScored)}|" +
            s"${game.actions.count(p => p.batterId == entry.player.id.get && (p.playType == PlayType.IBB || p.playType == PlayType.AUTO_BB || p.result == "BB"))}|" +
            s"${game.actions.count(p => p.batterId == entry.player.id.get && (p.result == "K" || p.result == "Auto K" || p.result == "Bunt K"))}|" +
            s"${"%.3f".format(entry.player.battingStats.find(_.season == game.season).map(_.ba).getOrElse(0f))}|\n"
        }).getOrElse("|--|--|--|--|--|--|--|--|--|--|\n")
      }
    }

    // Return the combined string
    s"$header\n$lineupBox"
  }

  def generatePitcherBox(game: Game): String = {
    val header = s"|${game.awayTeam.tag}|IP|H|ER|BB|K|ERA|${game.homeTeam.tag}|IP|H|ER|BB|K|ERA|\n" +
      s"|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|:--|"

    var pitcherBox = ""

    // Figure out how many rows we need for this table
    val aways = game.awayLineup.map(_.entries).map(_.filter(_.battingPos == 0).sortBy(_.id))
    val homes = game.homeLineup.map(_.entries).map(_.filter(_.battingPos == 0).sortBy(_.id))
    val rows = Math.max(aways.map(_.size).getOrElse(1), homes.map(_.size).getOrElse(1))

    // Now start putting in the players, or blank lines if none
    for (j <- 0 until rows) {
      val awayPlayer = aways.flatMap(_.lift(j))
      pitcherBox += awayPlayer.map(entry => {
        s"|${if (entry.replacedById.isDefined) "~~" else ""}[${entry.player.fullName}](${entry.player.user.redditName})${if (entry.replacedById.isDefined) "~~" else ""}|" +
          s"${"%.1f".format(game.actions.filter(p => p.pitcherId == entry.player.id.get).foldLeft(0)(_ + _.outsTracked) / 3.0).replace(".3", ".1").replace(".7", ".2")}|" +
          s"${game.actions.count(p => p.pitcherId == entry.player.id.get && p.isHit)}|" +
          s"${game.scoringPlays.count(_.scorer1ResponsibilityId.contains(entry.player.id.get)) + game.scoringPlays.count(_.scorer2ResponsibilityId.contains(entry.player.id.get)) + game.scoringPlays.count(_.scorer3ResponsibilityId.contains(entry.player.id.get)) + game.scoringPlays.count(_.scorer4ResponsibilityId.contains(entry.player.id.get))}|" +
          s"${game.actions.count(p => p.pitcherId == entry.player.id.get && (p.result == "BB" || p.result == "Auto BB"))}|" +
          s"${game.actions.count(p => p.pitcherId == entry.player.id.get && (p.result == "K" || p.result == "Auto K" || p.result == "Bunt K"))}|" +
          s"${"%.2f".format(entry.player.pitchingStats.find(_.season == game.season).map(_.era).getOrElse(0f)).replace("Infinity", "Inf.")}|"
      }).getOrElse("|--|--|--|--|--|--|--|")
      val homePlayer = homes.flatMap(_.lift(j))
      pitcherBox += homePlayer.map(entry => {
        s"${if (entry.replacedById.isDefined) "~~" else ""}[${entry.player.fullName}](${entry.player.user.redditName})${if (entry.replacedById.isDefined) "~~" else ""}|" +
          s"${"%.1f".format(game.actions.filter(p => p.pitcherId == entry.player.id.get).foldLeft(0)(_ + _.outsTracked) / 3.0).replace(".3", ".1").replace(".7", ".2")}|" +
          s"${game.actions.count(p => p.pitcherId == entry.player.id.get && p.isHit)}|" +
          s"${game.scoringPlays.count(_.scorer1ResponsibilityId.contains(entry.player.id.get)) + game.scoringPlays.count(_.scorer2ResponsibilityId.contains(entry.player.id.get)) + game.scoringPlays.count(_.scorer3ResponsibilityId.contains(entry.player.id.get)) + game.scoringPlays.count(_.scorer4ResponsibilityId.contains(entry.player.id.get))}|" +
          s"${game.actions.count(p => p.pitcherId == entry.player.id.get && (p.result == "BB" || p.result == "Auto BB"))}|" +
          s"${game.actions.count(p => p.pitcherId == entry.player.id.get && (p.result == "K" || p.result == "Auto K" || p.result == "Bunt K"))}|" +
          s"${"%.2f".format(entry.player.pitchingStats.find(_.season == game.season).map(_.era).getOrElse(0f)).replace("Infinity", "Inf.")}|\n"
      }).getOrElse("--|--|--|--|--|--|--|\n")
    }

    // Combine and return
    s"$header\n$pitcherBox"
  }

  def generateScoringPlaysBox(scoringPlays: Seq[ScoringPlay]): String = {
    val header = "|Inning|Play|Score|\n|:--|:--|:--|"

    val plays = scoringPlays
      .sortBy(_.gameAction.id)
      .map(sp => s"|${sp.gameAction.beforeState.inningStr}|${sp.description}|${sp.gameAction.afterState.scoreAway} - ${sp.gameAction.afterState.scoreHome}|")
      .mkString("\n")

    s"$header\n$plays"
  }

  def generateBatterPing(game: Game, batterEntry: LineupEntry, pitcherEntry: LineupEntry, batterPingFormat: String): String = {
    var hits = 0
    var abs = 0
    var plays = Seq[String]()
    game.actions.reverse.foreach { play =>
      if (play.batterId == batterEntry.player.id.get) {
        // Count hits/ABs
        if (play.result == "HR" || play.result == "3B" || play.result == "2B" || play.result == "1B" || play.result == "Bunt 1B") {
          hits += 1
          abs += 1
        } else if (play.result == "FO" && play.runsScored == 0) {
          abs += 1
        } else if (play.result == "K" || play.result == "Bunt K" || play.result == "Auto K" || play.result == "PO" || play.result == "RGO" || play.result == "LGO") {
          abs += 1
        }

        // Make list of plays
        val i = play.beforeState.inning
        val inning = if (i % 2 == 1) s"T${(i + 1) / 2}" else s"B${i / 2}"
        plays :+= s"${play.result} in $inning"
      }
    }

    val awayTag = game.awayTeam.tag
    val homeTag = game.homeTeam.tag
    val awayScore = game.state.scoreAway.toString
    val homeScore = game.state.scoreHome.toString
    val basesDiamonds = s" ${if (game.state.r3.isDefined) "\u25C6" else "\u25C7"} ^${if (game.state.r2.isDefined) "\u25C6" else "\u25C7"} ${if (game.state.r1.isDefined) "\u25C6" else "\u25C7"}"
    val inning = game.state.inningStr
    val outs = game.state.outs.toString
    val batterTeamTag = if (game.state.inning % 2 == 1) game.awayTeam.tag else game.homeTeam.tag
    val batterPosition = batterEntry.position
    val batterPing = s"[${batterEntry.player.fullName}](${batterEntry.player.user.redditName})"
    val batterRecord = s"$hits-$abs${if (plays.nonEmpty) s": ${plays.mkString(", ")}" else ""}"

    val pitcherName = pitcherEntry.player.fullName
    val pitchingChange =
      if (game.actions.exists(_.pitcherId == pitcherEntry.player.id.get))
        ""
      else
        s"**New pitcher: ${pitcherEntry.player.fullName}**"

    batterPingFormat
      .replace("%awayTag%", awayTag)
      .replace("%homeTag%", homeTag)
      .replace("%awayScore%", awayScore)
      .replace("%homeScore%", homeScore)
      .replace("%basesDiamonds%", basesDiamonds)
      .replace("%inning%", inning)
      .replace("%outs%", outs)
      .replace("%batterTeamTag%", batterTeamTag)
      .replace("%batterPosition%", batterPosition)
      .replace("%batterPing%", batterPing)
      .replace("%batterRecord%", batterRecord)
      .replace("%pitchingChange%", pitchingChange)
      .replace("%pitcherName%", pitcherName)
      .replaceAll("\n{3,}", "\n\n")
  }

  private def generateGameLineForScoreboard(g: Game): String =
    s"[${g.awayTeam.tag} @ ${g.homeTeam.tag}](https://redditball.com/games/${g.id.get})|${g.state.scoreAway} - ${g.state.scoreHome}|${g.state.inningStr}.${g.state.outs}|${if(g.state.r3Id.isDefined) "x" else "-"} ^${if(g.state.r2Id.isDefined) "x" else "-"} ${if(g.state.r1Id.isDefined) "x" else "-"}"

  def generateSessionScoreboard(games: Seq[Game], league: String): String =
    s"""**$league**
       |---
       |TEAMS|SCORE|INNING|BASES
       |:--|:-:|:-:|:-:
       |${games.map(generateGameLineForScoreboard).mkString("\n")}
       |""".stripMargin

}
