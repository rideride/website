package services

import java.net.URLEncoder
import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import play.Environment
import play.api.Configuration
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}
import play.api.libs.ws.{WSAuthScheme, WSClient, WSRequest}
import play.api.mvc.{AnyContent, Request}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}

@Singleton
class RedditAuthService @Inject()(config: Configuration, env: Environment, ws: WSClient)(implicit ec: ExecutionContext) {

  private implicit val RedditUserInfoReads: Reads[RedditUserInfo] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "name").read[String] and
      (JsPath \ "has_verified_email").readWithDefault[Boolean](false) and
      (JsPath \ "is_suspended").read[Boolean] and
      (JsPath \ "link_karma").read[Int] and
      (JsPath \ "comment_karma").read[Int] and
      (JsPath \ "created_utc").read[Long]
    ) ((id, name, isEmailVerified, isSuspended, linkKarma, commentKarma, creationTimestamp) => RedditUserInfo(id, s"/u/$name", isEmailVerified, isSuspended, linkKarma, commentKarma, creationTimestamp))

  private lazy val clientId = config.get[String]("reddit.clientId")
  private lazy val clientSecret = config.get[String]("reddit.clientSecret")

  def encodeRedirectUri(implicit req: Request[AnyContent]): String = URLEncoder.encode((if (env.isDev) "http://" else "https://") + req.host + "/auth/signin", "UTF-8")

  def oauthConsentUri(state: String, mobile: Boolean)(implicit req: Request[AnyContent]): String = {
    oauthUri(state, "temporary", "identity", mobile)
  }

  def oauthConsentUriForRefreshToken(state: String, mobile: Boolean)(implicit req: Request[AnyContent]): String = {
    oauthUri(state, "permanent", "identity edit flair read submit", mobile)
  }

  def oauthUri(state: String, duration: String, scopes: String, mobile: Boolean)(implicit req: Request[AnyContent]): String = {
    s"https://old.reddit.com/api/v1/authorize${if (mobile) ".compact" else ""}?client_id=$clientId&response_type=code&state=$state&redirect_uri=$encodeRedirectUri&duration=$duration&scope=$scopes"
  }

  private def baseOAuthReq(url: String): WSRequest =
    ws.url(url)
      .withAuth(clientId, clientSecret, WSAuthScheme.BASIC)
      .withHttpHeaders("User-Agent" -> "web:xyz.redditball:v1.0.0 (by /u/llamositopia)")
      .withHttpHeaders("Content-Type" -> "application/x-www-form-urlencoded")

  def getIdentity(accessToken: String)(implicit req: Request[AnyContent]): RedditUserInfo = {
    val myInfoResponse = Await.result(
      ws.url("https://oauth.reddit.com/api/v1/me")
        .withHttpHeaders("User-Agent" -> "web:xyz.redditball:v1.0.0 (by /u/llamositopia)")
        .withHttpHeaders("Authorization" -> s"Bearer $accessToken")
        .get(),
      Duration(15, TimeUnit.SECONDS)
    )
    myInfoResponse.json.as[RedditUserInfo]
  }

  def getAccessTokenFromRefresh(refreshToken: String): String = {
    val accessTokenResponse = Await.result(
      baseOAuthReq("https://www.reddit.com/api/v1/access_token")
        .post(s"grant_type=refresh_token&refresh_token=$refreshToken"),
      Duration(15, TimeUnit.SECONDS)
    )
    (accessTokenResponse.json \ "access_token").as[String]
  }

  def getOAuthTokens(authCode: String)(implicit req: Request[AnyContent]): (String, Option[String]) = {
    val accessTokenResponse = Await.result(
      baseOAuthReq("https://www.reddit.com/api/v1/access_token")
        .post(s"grant_type=authorization_code&code=$authCode&redirect_uri=$encodeRedirectUri"),
      Duration(15, TimeUnit.SECONDS)
    )
    val accessToken = (accessTokenResponse.json \ "access_token").as[String]
    val refreshToken = (accessTokenResponse.json \ "refresh_token").asOpt[String]

    (accessToken, refreshToken)
  }

  def revokeToken(token: String, tokenType: String): Unit = {
    Await.result(
      ws.url("https://www.reddit.com/api/v1/revoke_token")
        .withAuth(clientId, clientSecret, WSAuthScheme.BASIC)
        .withMethod("POST")
        .withHttpHeaders("User-Agent" -> "web:xyz.redditball:v1.0.0 (by /u/llamositopia)")
        .withHttpHeaders("Content-Type" -> "application/x-www-form-urlencoded")
        .post(s"token=$token&token_type_hint=$tokenType"),
      Duration(15, TimeUnit.SECONDS)
    )
  }

}
