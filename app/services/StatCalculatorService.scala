package services

import model.FBDatabaseProvider
import model.db._
import model.db.util._
import slick.jdbc.MySQLProfile.api._

object StatCalculatorService {

  // region batting

  def calculateHits(stats: BattingStat): Int = stats.totalHR + stats.total3B + stats.total2B + stats.total1B

  def calculateBA(stats: BattingStat): Float = if (stats.totalAB == 0) 0 else calculateHits(stats).toFloat / stats.totalAB

  def calculateOBP(stats: BattingStat): Float = if (stats.totalPA == 0) 0 else (calculateHits(stats) + stats.totalBB).toFloat / stats.totalPA

  def calculateSLG(stats: BattingStat): Float = if (stats.totalAB == 0) 0 else stats.tb.toFloat / stats.totalAB

  def calculateOPS(stats: BattingStat): Float = stats.obp + stats.slg

  def calculateTB(stats: BattingStat): Float = 4 * stats.totalHR + 3 * stats.total3B + 2 * stats.total2B + 1 * stats.total1B

  def calculateDPA(stats: BattingStat): Float = if (stats.totalPA == 0) 0 else stats.summedDiff.toFloat / stats.totalPA.toFloat

  def recalculateBattingStats(player: Int, season: Int)(implicit db: FBDatabaseProvider): Unit = {
    if (!BattingStats.filter(bs => bs.playerId === player && bs.season === season).exists.result.run)
      (BattingStats += BattingStat(player, season)).run
    sqlu"""
      update batting_stats t
      set total_pas=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and play_type != 6 and play_type != 7 and play_type != 9),
          total_abs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and play_type != 6 and play_type != 7 and play_type != 3 and play_type != 8 and play_type != 9 and result != '' and result != 'BB' and !(result = 'FO' and runs_scored = 1) and result != 'Bunt Sac'),
          total_rbi=(select (case when sum(runs_scored) is null then 0 else sum(runs_scored) end) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and play_type != 6 and play_type != 7 and play_type != 8 and play_type != 9 and play_type != 3 and result != 'BB' and outs_tracked < 2),
          total_r=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and FIND_IN_SET(t.player, a.scorers) != 0),
          total_hr=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result = 'HR'),
          total_3b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result = '3B'),
          total_2b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result = '2B'),
          total_1b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result in ('1B', 'Bunt 1B')),
          total_bb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result in ('BB', 'Auto BB', 'IBB')),
          total_fo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result = 'FO'),
          total_k=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result in ('K', 'Auto K', 'Bunt K')),
          total_po=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result = 'PO'),
          total_rgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result = 'RGO'),
          total_lgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result = 'LGO'),
          total_sb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result in ('Steal 2B', 'Steal 3B', 'Steal Home', 'MSteal 3B', 'MSteal Home')),
          total_cs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result in ('CS 2B', 'CS 3B', 'CS Home', 'CMS 3B', 'CMS Home')),
          total_cs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result in ('CS 2B', 'CS 3B', 'CS Home', 'CMS 3B', 'CMS Home')),
          total_dp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and outs_tracked=2),
          total_tp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and outs_tracked=3),
          total_gp=(select count(distinct lineup) from lineup_entries a where (select season from games b where (b.away_lineup=a.lineup or b.home_lineup=a.lineup) and b.stats_calculated and b.season = $season) is not null and a.player = t.player and a.batting_pos != 0),
          total_sac=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and result='FO' and runs_scored=1),
          summed_diff=ifnull((select sum(diff) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and batter = t.player and play_type in (1, 4, 5)), 0)
          where player=$player and season=$season
      """.run
  }

  // endregion

  // region pitching

  def calculateIP(stats: PitchingStat): String = s"${stats.totalOuts / 3}.${stats.totalOuts % 3}"

  def calculateERA(stats: PitchingStat): Float = if (stats.totalER == 0) 0 else if (stats.totalOuts == 0) Float.PositiveInfinity else (stats.totalER * 18).toFloat / stats.totalOuts.toFloat

  def calculateWHIP(stats: PitchingStat): Float = if (stats.totalOuts == 0) 0 else (stats.totalHR + stats.total3B + stats.total2B + stats.total1B + stats.totalBB) * 3 / stats.totalOuts.toFloat

  def calculateGO(stats: PitchingStat): Int = stats.totalLGO + stats.totalRGO

  def calculateK6(stats: PitchingStat): Float = stats.totalK.toFloat / (18 * stats.totalOuts)

  def calculateW6(stats: PitchingStat): Float = stats.totalBB.toFloat / (18 * stats.totalOuts)

  def calculateDBF(stats: PitchingStat): Float = if (stats.totalPAs == 0) 0 else stats.summedDiff.toFloat / stats.totalPAs.toFloat

  def recalculatePitchingStats(player: Int, season: Int)(implicit db: FBDatabaseProvider): Unit = {
    if (!PitchingStats.filter(ps => ps.playerId === player && ps.season === season).exists.result.run)
      (PitchingStats += PitchingStat(player, season)).run
    sqlu"""
      update pitching_stats t
      set total_pas=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and play_type != 6 and play_type != 7 and play_type != 9),
          total_abs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and play_type != 6 and play_type != 7 and play_type != 3 and play_type != 8 and play_type != 9 and result != '' and result != 'BB' and !(result = 'FO' and runs_scored = 1) and result != 'Bunt Sac'),
          total_outs=(select (case when sum(outs_tracked) is null then 0 else sum(outs_tracked) end) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and play_type != 3 and play_type != 9 and result != ''),
          total_er=(case when (select sum(case when scorer1_responsibility = $player then 1 else 0 end) + sum(case when scorer2_responsibility = $player then 1 else 0 end) + sum(case when scorer3_responsibility = $player then 1 else 0 end) + sum(case when scorer4_responsibility = $player then 1 else 0 end) from scoring_plays where game_id in (select id from games g where season = $season and g.stats_calculated and (select id from teams where id = g.home_team) is not null)) is null then 0 else ((select sum(case when scorer1_responsibility = $player then 1 else 0 end) + sum(case when scorer2_responsibility = $player then 1 else 0 end) + sum(case when scorer3_responsibility = $player then 1 else 0 end) + sum(case when scorer4_responsibility = $player then 1 else 0 end) from scoring_plays where game_id in (select id from games g where season = $season and g.stats_calculated and (select id from teams where id = g.home_team) is not null))) end),
          total_hr=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result = 'HR'),
          total_3b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result = '3B'),
          total_2b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result = '2B'),
          total_1b=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result in ('1B', 'Bunt 1B')),
          total_bb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result in ('BB', 'Auto BB', 'IBB')),
          total_fo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result = 'FO'),
          total_k=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result = 'K'),
          total_po=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result = 'PO'),
          total_rgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result = 'RGO'),
          total_lgo=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result = 'LGO'),
          total_sb=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result in ('Steal 2B', 'Steal 3B', 'Steal Home', 'MSteal 3B', 'MSteal Home')),
          total_cs=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result in ('CS 2B', 'CS 3B', 'CS Home', 'CMS 3B', 'CMS Home')),
          total_dp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and outs_tracked=2),
          total_tp=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and outs_tracked=3),
          total_gp=(select count(distinct lineup) from lineup_entries a where (select season from games b where (b.away_lineup=a.lineup or b.home_lineup=a.lineup) and b.stats_calculated and b.season = $season) is not null and a.player = t.player and a.batting_pos = 0),
          total_sac=(select count(*) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and result='FO' and runs_scored=1),
          summed_diff=ifnull((select sum(diff) from game_actions a where (select season from games b where b.id=a.game_id and b.stats_calculated and b.season = $season) is not null and pitcher = t.player and play_type in (1, 4, 5)), 0),
          wins=(select count(*) from game_awards a where (select season from games b where b.id=a.game and b.stats_calculated and b.season = $season) is not null and winning_pitcher = t.player),
          losses=(select count(*) from game_awards a where (select season from games b where b.id=a.game and b.stats_calculated and b.season = $season) is not null and losing_pitcher = t.player),
          saves=(select count(*) from game_awards a where (select season from games b where b.id=a.game and b.stats_calculated and b.season = $season) is not null and save = t.player)
          where player=$player and season=$season
        """.run
  }

  // endregion

}
