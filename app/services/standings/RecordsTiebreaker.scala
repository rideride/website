package services.standings

import javax.inject.{Inject, Singleton}
import model.db.Team

@Singleton
class RecordsTiebreaker @Inject()() extends Tiebreaker {

  private def wl(t: Team, season: Int): Int = {
    val records = t.records.getOrElse(season, (0, 0))
    records._1 - records._2
  }

  override def compare(t1: Team, t2: Team, season: Int): Int = {
    val t1WL = wl(t1, season)
    val t2WL = wl(t2, season)
    t1WL.compareTo(t2WL)
  }

}
