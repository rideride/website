package services.standings

import model.db.{Game, Team}

trait Tiebreaker {

  val key: String = this.getClass.getSimpleName

  def compare(t1: Team, t2: Team, season: Int): Int

  def wins(t: Team, games: Seq[Game]): Int = {
    games.foldLeft(0) { (acc, g) =>
      if (g.winningTeamId.contains(t.id.get))
        acc + 1
      else
        acc - 1
    }
  }

}




