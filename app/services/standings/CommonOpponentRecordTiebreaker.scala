package services.standings

import javax.inject.{Inject, Singleton}
import model.db.{Game, Team}
import services.GlobalSettingsProvider

@Singleton
class CommonOpponentRecordTiebreaker @Inject()(gsp: GlobalSettingsProvider) extends Tiebreaker {

  override def compare(t1: Team, t2: Team, season: Int): Int = {
    val (t1Games, t2Games) = commonOppGames(t1, t2, season)

    if (t1Games.size < gsp.STANDINGS_COMMON_OPPONENT_MINIMUM_GAMES)
      return 0

    wins(t1, t1Games).compareTo(wins(t2, t2Games))
  }

  private def opponent(g: Game, t: Team): Team =
    if (g.awayTeamId == t.id.get) g.homeTeam else g.awayTeam

  private def commonOppGames(t1: Team, t2: Team, season: Int): (Seq[Game], Seq[Game]) = {
    t1.games
      .filter(g => g.season == season && g.statsCalculated)
      .foldLeft((Seq[Game](), Seq[Game]())) { (gSeqTuple, g1) =>
        val g1Opp = opponent(g1, t1)
        val g2 = t2.games.find(g2 =>
          g2.season == season
            && g2.statsCalculated
            && opponent(g2, t2).id.get == g1Opp.id.get
        )

        if (g2.isEmpty) {
          gSeqTuple
        } else {
          (gSeqTuple._1 :+ g1, gSeqTuple._2 :+ g2.get)
        }
      }
  }

}
