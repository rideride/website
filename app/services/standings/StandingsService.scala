package services.standings

import javax.inject.{Inject, Singleton}
import model.db.Team
import services.GlobalSettingsProvider

@Singleton
class StandingsService @Inject()(private val commonOpponentRecordTiebreaker: CommonOpponentRecordTiebreaker,
                                 private val divisionRecordTiebreaker: DivisionRecordTiebreaker,
                                 private val headToHeadTiebreaker: HeadToHeadTiebreaker,
                                 private val recordsTiebreaker: RecordsTiebreaker)
                                (implicit gsp: GlobalSettingsProvider) {

  private val TIEBREAKERS = Seq(
    commonOpponentRecordTiebreaker,
    divisionRecordTiebreaker,
    headToHeadTiebreaker,
    recordsTiebreaker,
  )

  private val DEFAULT_DIVISION_TIEBREAKER_ORDER = Seq(
    recordsTiebreaker.key,
    headToHeadTiebreaker.key,
    divisionRecordTiebreaker.key,
    commonOpponentRecordTiebreaker.key,
  ).mkString(",")

  private val DEFAULT_WILDCARD_TIEBREAKER_ORDER = Seq(
    recordsTiebreaker.key,
    commonOpponentRecordTiebreaker.key,
  ).mkString(",")

  def divisionTiebreak(season: Int)(t1: Team, t2: Team): Boolean = {
    val computeOrder = gsp.STANDINGS_COMPUTE_ORDER
      .getOrElse(DEFAULT_DIVISION_TIEBREAKER_ORDER)
      .split(",")
      .map(key => TIEBREAKERS.find(_.key == key).getOrElse(recordsTiebreaker))

    computeOrder.foreach { tiebreaker =>
      val compareResult = tiebreaker.compare(t1, t2, season)
      if (compareResult != 0)
        return compareResult > 0
    }

    false
  }

}
