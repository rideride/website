package services.standings

import javax.inject.{Inject, Singleton}
import model.db.Team

@Singleton
class DivisionRecordTiebreaker @Inject()() extends Tiebreaker {

  private def divisionRecord(t: Team, season: Int): Int = {
    val divisionGames = t.games.filter(g =>
      g.season == season
        && g.statsCalculated
        && g.awayTeam.divisionId.contains(t.divisionId.get)
        && g.homeTeam.divisionId.contains(t.divisionId.get)
        && g.winningTeamId.isDefined
    )

    wins(t, divisionGames)
  }

  override def compare(t1: Team, t2: Team, season: Int): Int = {
    val t1DivRecord = divisionRecord(t1, season)
    val t2DivRecord = divisionRecord(t2, season)

    t1DivRecord.compareTo(t2DivRecord)
  }
}
