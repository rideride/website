package services.standings

import javax.inject.{Inject, Singleton}
import model.db.Team

@Singleton
class HeadToHeadTiebreaker @Inject()() extends Tiebreaker {

  override def compare(t1: Team, t2: Team, season: Int): Int = {
    val headToHead = t1.games.filter(g =>
      g.season == season
        && g.statsCalculated
        && (g.awayTeamId == t2.id.get || g.homeTeamId == t2.id.get)
        && g.winningTeamId.isDefined
    )

    wins(t1, headToHead)
  }

}
