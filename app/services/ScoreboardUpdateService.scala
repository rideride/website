package services

import akka.actor.ActorSystem
import javax.inject.{Inject, Singleton}
import model.{FBDatabaseProvider, REDDIT_ID36_REGEX}
import model.db._
import model.db.util._
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

@Singleton
class ScoreboardUpdateService @Inject()(implicit db: FBDatabaseProvider, redditPostService: RedditPostService, redditFormattingService: RedditFormattingService, authService: RedditAuthService, globalSettingsProvider: GlobalSettingsProvider, actorSystem: ActorSystem, executionContext: ExecutionContext) {

  actorSystem.scheduler.scheduleAtFixedRate(initialDelay = 10.seconds, interval = 15.minutes)(() => updateScoreboard())

  def updateScoreboard(currentSession: (Int, Int) = {
    val t = globalSettingsProvider.SCOREBOARD_SESSION.getOrElse("1.1").split("\\.").map(_.toInt)
    (t.headOption.getOrElse(1), t.lift(1).getOrElse(1))
  }): Option[String] = {
    Users
      .filter(_.redditName === globalSettingsProvider.ROBO_ACCOUNT)
      .map(_.refreshToken)
      .result
      .run
      .headOption
      .flatten
      .flatMap { refToken =>

        val accToken = authService.getAccessTokenFromRefresh(refToken)
        val games = Games.filter(g => g.season === currentSession._1 && g.session === currentSession._2)
          .withMap(_.state)
          .withMap(_.awayTeam)
          .withMap(_.homeTeam)
          .seq

        val scoreboard = redditFormattingService.generateSessionScoreboard(games, globalSettingsProvider.LEAGUE_NAME.getOrElse("MLR"))

        globalSettingsProvider.SCOREBOARD_THREAD.map { thread =>
          redditPostService.editThread(accToken, thread, scoreboard)
          Some(thread)
        } getOrElse {
          val response = redditPostService.createSelfPost(accToken, s"Session ${currentSession._1}.${currentSession._2} Scoreboard", scoreboard)
          (response.body \ "jquery" \ 10 \ 3 \ 0).asOpt[String]
            .map {
              case REDDIT_ID36_REGEX(matched) =>
                globalSettingsProvider.setScoreboardThread(Some(matched))
                Some(matched)
              case _ =>
                None
            }
        }.flatten
      }
  }

}
