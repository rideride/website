package services

import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import slick.jdbc.MySQLProfile.api._

import scala.util.Try

@Singleton
class GlobalSettingsProvider @Inject()(implicit db: FBDatabaseProvider) {

  private var localMap: Map[String, String] = Map()

  def INDEX_MESSAGE: Option[String] = getSetting("index.message")

  def AB_PINGS_CHANNEL: Option[String] = getSetting("discord.channel.ping")

  def UMPTY_COMMAND_CHANNEL: Option[String] = getSetting("discord.channel.command")

  def DISCORD_BOT_OFFICIAL: Option[String] = getSetting("discord.bot.official")
  def DISCORD_WEBHOOK: Option[String] = getSetting("discord.webhook")
  def DISCORD_SERVER: Option[String] = getSetting("discord.server")
  def DISCORD_CHANNEL_AB_PING: Option[String] = getSetting("discord.channel.abping")
  def DISCORD_CHANNEL_COMPLAINTS: Option[String] = getSetting("discord.channel.complaints")
  def DISCORD_CHANNEL_DRAFT: Option[String] = getSetting("discord.channel.draft")
  def DISCORD_CHANNEL_DRAFT_SECONDS: Option[String] = getSetting("discord.channel.draft.seconds")
  def DISCORD_CHANNEL_GAME_THREADS: Option[String] = getSetting("discord.channel.gamethreads")
  def DISCORD_CHANNEL_PROPOSALS: Option[String] = getSetting("discord.channel.proposals")
  def DISCORD_CHANNEL_SCOREBOARD: Option[String] = getSetting("discord.channel.scoreboard")
  def DISCORD_CHANNEL_SHITPOST: Option[String] = getSetting("discord.channel.shitpost")
  def DISCORD_CHANNEL_UMP_PING: Option[String] = getSetting("discord.channel.umpping")
  def DISCORD_CHANNEL_WEBHOOK: Option[String] = getSetting("discord.channel.webhook")
  def DISCORD_INVITE: Option[String] = getSetting("discord.invite")
  def DISCORD_LINK_RULEBOOK: Option[String] = getSetting("link.rulebook")
  def DISCORD_LINK_UMPIRE_HANDBOOK: Option[String] = getSetting("link.umphandbook")
  def DISCORD_ROLE_FREE_AGENT: Option[String] = getSetting("discord.role.freeagent")
  def SUBREDDIT: Option[String] = getSetting("reddit.subreddit")

  def ROBO_ACCOUNT: Option[String] = getSetting("reddit.roboaccount")

  def SCOREBOARD_SESSION: Option[String] = getSetting("scoreboard.session")

  def STANDINGS_ENABLED: Boolean = getSetting("standings.enabled").flatMap(s => Try(s.toBoolean).toOption).getOrElse(false)
  def STANDINGS_COMMON_OPPONENT_MINIMUM_GAMES: Int = getSetting("standings.commonopponent.mingames").flatMap(s => Try(s.toInt).toOption).getOrElse(4)
  def STANDINGS_COMPUTE_ORDER: Option[String] = getSetting("standings.computeorder")

  def SCOREBOARD_THREAD: Option[String] = getSetting("scoreboard.thread")
  def LEAGUE_NAME: Option[String] = getSetting("league.name")
  def LEAGUE_DISCORD: Option[String] = getSetting("league.discord")

  def RULES_ENFORCE_POSITIONS: Boolean = getSetting("rules.enforce_positions").flatMap(s => Try(s.toBoolean).toOption).getOrElse(true)
  def RULES_INNINGS: Int = getSetting("rules.innings").flatMap(s => Try(s.toInt).toOption).getOrElse(6)

  def setScoreboardSession(newSession: (Int, Int)): Unit = setSetting("scoreboard.session", s"${newSession._1}.${newSession._2}")

  def setScoreboardThread(thread: Option[String]): Unit =
    thread.map { thread =>
      setSetting("scoreboard.thread", thread)
    } getOrElse {
      deleteSetting("scoreboard.thread")
    }

  def all: Map[String, String] = {
    if (localMap.isEmpty)
      loadAll()

    localMap
  }

  def loadAll(): Unit =
    localMap =
      GlobalSettings
      .seq
      .map(r => r.key -> r.value)
      .toMap

  def getSetting(name: String): Option[String] = {
    if (localMap.contains(name))
      return localMap.get(name)

    loadAll()

    localMap.get(name)
  }

  def setSetting(name: String, value: String): Unit = {
    GlobalSettings.insertOrUpdate(GlobalSetting(name, value)).run

    loadAll()
  }

  def deleteSetting(name: String): Unit = {
    GlobalSettings
      .filter(_.key === name)
      .delete
      .run

    loadAll()
  }

}
