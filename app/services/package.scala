import play.api.libs.json.{JsValue, Json, Writes}

package object services {

  case class DiscordUserInfo(id: String, username: String, discriminator: String)

  case class RedditUserInfo(id: String, name: String, isEmailVerified: Boolean, isSuspended: Boolean, linkKarma: Int, commentKarma: Int, creationTimestamp: Long)

  case class RedditRequest(endpoint: String, body: Map[String, Seq[String]], get: Boolean)

  case class RedditResponse(status: Int, body: JsValue)

  def RedditSubmissionRequest(kind: String, sr: String, title: String, text: String): RedditRequest =
    RedditRequest(
      endpoint = "api/submit",
      body = Map(
        "kind" -> Seq(kind),
        "sr" -> Seq(sr),
        "title" -> Seq(title),
        "text" -> Seq(text)
      ),
      get = false
    )

  def RedditEditRequest(thing: String, text: String): RedditRequest =
    RedditRequest(
      endpoint = "api/editusertext",
      body = Map(
        "thing_id" -> Seq(s"t3_$thing"),
        "text" -> Seq(text),
      ),
      get = false
    )

  def RedditCommentRequest(thing: String, text: String): RedditRequest = RedditReplyRequest(s"t3_$thing", text)

  def RedditMessageRequest(targetUser: String, title: String, message: String): RedditRequest =
    RedditRequest(
      endpoint = "api/compose",
      body = Map(
        "subject" -> Seq(title),
        "message" -> Seq(message),
        "to" -> Seq(targetUser)
      ),
      get = false
    )

  val RedditInboxRequest: RedditRequest = RedditRequest(
    endpoint = "message/unread",
    body = Map(
      "mark" -> Seq("true")
    ),
    get = true
  )

  def RedditReplyRequest(parent: String, text: String): RedditRequest = RedditRequest(
    endpoint = "api/comment",
    body = Map(
      "thing_id" -> Seq(parent),
      "text" -> Seq(text)
    ),
    get = false
  )

  def RedditMarkReadRequest(thing: String): RedditRequest = RedditRequest(
    endpoint = "api/read_message",
    body = Map(
      "id" -> Seq(thing)
    ),
    get = false
  )

}
