package services

import controllers.routes
import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import slick.jdbc.MySQLProfile.api._
import model.db.Game
import play.api.mvc.{AnyContent, Request}
import model.REDDIT_ID36_REGEX

@Singleton
class GameFlowService @Inject()(implicit globalSettingsProvider: GlobalSettingsProvider, db: FBDatabaseProvider, redditFormattingService: RedditFormattingService, redditPostService: RedditPostService, redditAuthService: RedditAuthService, discordMessageService: DiscordMessageService) {

  def createRedditThread(game: Game, postingUser: User)(implicit req: Request[AnyContent]): Boolean = {
    // Team records for title
    val awaySeasonRecord = game.awayTeam.records.getOrElse(game.season, (0, 0))
    val homeSeasonRecord = game.homeTeam.records.getOrElse(game.season, (0, 0))

    // Generate
    val boxScore = redditFormattingService.generateBoxScore(game)

    // Create thread
    val accessToken = redditAuthService.getAccessTokenFromRefresh(postingUser.refreshToken.get)
    val league = globalSettingsProvider.LEAGUE_NAME.getOrElse("MLR")
    val response = redditPostService.createGameThread(accessToken, s"[$league GDT ${game.season}.${game.session}] ${game.awayTeam.name} (${awaySeasonRecord._1}-${awaySeasonRecord._2}) @ ${game.homeTeam.name} (${homeSeasonRecord._1}-${homeSeasonRecord._2})", boxScore)
    redditAuthService.revokeToken(accessToken, "access_token")

    // Set id36 and return
    (response.body \ "jquery" \ 10 \ 3 \ 0).asOpt[String].exists {
      case REDDIT_ID36_REGEX(matched) =>
        Games.filter(_.id === game.id).map(g => (g.id36, g.threadOwnerId)).update((Some(matched), Some(postingUser.id.get))).run
        discordMessageService.announceThread(game, game.umpireAssignments.map(_.umpire.value), matched)
        true
      case _ =>
        false
    }
  }

}
