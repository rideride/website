package services

import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import model.DiscordTypes._
import model.db._
import play.api.Configuration
import play.api.libs.json.Json
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class DiscordMessageService @Inject()(settingsProvider: GlobalSettingsProvider, ws: WSClient) {

  private def webhookRequest(discordMessageRequest: DiscordMessageRequest): Option[WSResponse] =
    settingsProvider.DISCORD_WEBHOOK.map(webhookUrl =>
      Await.result(
        ws.url(webhookUrl)
          .post(Json.toJson(discordMessageRequest)),
        Duration(15, TimeUnit.SECONDS)
      )
    )

  def sendABAlert(player: Player, game: Game, league: String): Unit = webhookRequest(
    UmptyABPingRequest(player, game, game.umpireAssignments.map(_.umpire.value), league)
  )

  def roleUser(user: String, role: String, remove: Boolean): Unit = webhookRequest(
    UmptyRoleRequest(user, role, remove)
  )

  def announceThread(game: Game, umpires: Seq[User], id36: String): Unit = webhookRequest(
    UmptyAnnounceThreadRequest(id36, game.awayTeam, game.homeTeam, umpires)
  )

}
