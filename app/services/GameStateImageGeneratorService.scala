package services

import java.awt.image.BufferedImage
import java.awt.{Color, GradientPaint}
import java.io.ByteArrayOutputStream

import javax.imageio.ImageIO
import javax.inject.{Inject, Singleton}
import model.db._
import play.api.Environment

@Singleton
class GameStateImageGeneratorService @Inject()(env: Environment) {

  def generateImage(game: Game): String = {
    // Load the template file
    val baseImgStream = env.resourceAsStream("public/images/diamond_template.svg").get
    val baos = new ByteArrayOutputStream()
    val buf = new Array[Byte](1024)
    var len = 0
    while ( {
      len = baseImgStream.read(buf); len
    } != -1)
      baos.write(buf, 0, len)

    // Copy the data into a string for replacing (SVG is plaintext anyway)
    baseImgStream.close()
    val svgData = new String(baos.toByteArray, "UTF-8")
    baos.close()

    val fieldingLineup = game.fieldingLineup.map(_.entries.value).getOrElse(Seq())
    val battingLineup = game.battingLineup.map(_.entries.value).getOrElse(Seq())

    // Get all the fielders' names
    val pitcher = fieldingLineup.find(e => e.battingPos == 0 && e.replacedById.isEmpty).map(_.player.lastName).getOrElse("")
    val catcher = fieldingLineup.find(e => e.position == "C" && e.replacedById.isEmpty).map(_.player.lastName).getOrElse("")
    val firstBasemen = fieldingLineup.find(e => e.position == "1B" && e.replacedById.isEmpty).map(_.player.lastName).getOrElse("")
    val secondBasemen = fieldingLineup.find(e => e.position == "2B" && e.replacedById.isEmpty).map(_.player.lastName).getOrElse("")
    val thirdBasemen = fieldingLineup.find(e => e.position == "3B" && e.replacedById.isEmpty).map(_.player.lastName).getOrElse("")
    val shortstop = fieldingLineup.find(e => e.position == "SS" && e.replacedById.isEmpty).map(_.player.lastName).getOrElse("")
    val leftFielder = fieldingLineup.find(e => e.position == "LF" && e.replacedById.isEmpty).map(_.player.lastName).getOrElse("")
    val centerFielder = fieldingLineup.find(e => e.position == "CF" && e.replacedById.isEmpty).map(_.player.lastName).getOrElse("")
    val rightFielder = fieldingLineup.find(e => e.position == "RF" && e.replacedById.isEmpty).map(_.player.lastName).getOrElse("")

    // Get the batter and baserunners
    val nextBatter = battingLineup.find(e => e.battingPos == game.state.nextBatterLineupPos && e.replacedById.isEmpty)
    val runnerOnFirst = game.state.r1Id.flatMap(p => battingLineup.find(_.player.id.contains(p))).map(_.player.lastName).getOrElse("")
    val runnerOnSecond = game.state.r2Id.flatMap(p => battingLineup.find(_.player.id.contains(p))).map(_.player.lastName).getOrElse("")
    val runnerOnThird = game.state.r3Id.flatMap(p => battingLineup.find(_.player.id.contains(p))).map(_.player.lastName).getOrElse("")

    // Replace all the placeholders and return
    svgData
      .replace("$PLAYER_P$", pitcher)
      .replace("$PLAYER_C$", catcher)
      .replace("$PLAYER_1B$", firstBasemen)
      .replace("$PLAYER_2B$", secondBasemen)
      .replace("$PLAYER_3B$", thirdBasemen)
      .replace("$PLAYER_SS$", shortstop)
      .replace("$PLAYER_LF$", leftFielder)
      .replace("$PLAYER_CF$", centerFielder)
      .replace("$PLAYER_RF$", rightFielder)
      .replace("$PLAYER_R1$", runnerOnFirst)
      .replace("$PLAYER_R2$", runnerOnSecond)
      .replace("$PLAYER_R3$", runnerOnThird)
      .replace("$1BFILL$", if (game.state.r1Id.isDefined) "black" else "white")
      .replace("$2BFILL$", if (game.state.r2Id.isDefined) "black" else "white")
      .replace("$3BFILL$", if (game.state.r3Id.isDefined) "black" else "white")
      .replace("$PLAYER_BL$", nextBatter.filterNot(_.player.rightHanded).map(_.player.lastName).getOrElse(""))
      .replace("$PLAYER_BR$", nextBatter.filter(_.player.rightHanded).map(_.player.lastName).getOrElse(""))
  }

  def generateBasicImage(game: Game): Array[Byte] = {
    // Create empty image
    val img = new BufferedImage(600, 300, BufferedImage.TYPE_INT_ARGB)
    val g = img.createGraphics()

    // Background
    g.setColor(Color.white)
    g.setPaint(new GradientPaint(0f, 0f, new Color(game.awayTeam.colorRosterBG), 600f, 300f, new Color(game.homeTeam.colorRosterBG)))
    g.fillRect(0, 0, 600, 300)

    // Away logo
    val awayImgStream = env.resourceAsStream(s"public/images/teams/${game.awayTeam.tag}.png")
    awayImgStream.foreach { awayImgStream =>
      val awayImg = ImageIO.read(awayImgStream)
      g.drawImage(awayImg, 50, 50, 200, 200, null)
      awayImgStream.close()
    }

    // Home logo
    val homeImgStream = env.resourceAsStream(s"public/images/teams/${game.homeTeam.tag}.png")
    homeImgStream.foreach { homeImgStream =>
      val homeImg = ImageIO.read(homeImgStream)
      g.drawImage(homeImg, 350, 50, 200, 200, null)
      homeImgStream.close()
    }

    // Write to byte arry
    val baos = new ByteArrayOutputStream()
    ImageIO.write(img, "png", baos)
    baos.toByteArray
  }

}
