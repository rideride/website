package services

import java.util.concurrent.TimeUnit

import javax.inject.{Inject, Singleton}
import model.db._
import play.api.libs.ws.WSClient

import scala.concurrent.Await
import scala.concurrent.duration._

@Singleton
class RedditPostService @Inject()(ws: WSClient, settingsProvider: GlobalSettingsProvider) {

  private def authenticatedRequest(accessToken: String, request: RedditRequest): RedditResponse = {
    val result = Await.result(
      {
        val base = ws.url(s"https://oauth.reddit.com/${request.endpoint}")
          .withHttpHeaders("User-Agent" -> "web:com.redditball:v1.0.0 (by /u/llamositopia)")
          .withHttpHeaders("Authorization" -> s"Bearer $accessToken")
        if (request.get)
          base.addQueryStringParameters(request.body.map(t => t._1 -> t._2.head).toSeq: _*)
            .get()
        else
          base.post(request.body)
      },
      Duration(15, TimeUnit.SECONDS)
    )
    RedditResponse(result.status, result.json)
  }

  def createSelfPost(accessToken: String, title: String, body: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditSubmissionRequest(
        kind = "self",
        sr = settingsProvider.SUBREDDIT.getOrElse("fakebaseball"),
        title = title,
        text = body,
      )
    )

  def editThread(accessToken: String, id36: String, newBody: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditEditRequest(
        thing = id36,
        text = newBody
      )
    )

  def createGameThread(accessToken: String, title: String, boxScoreText: String): RedditResponse = createSelfPost(accessToken, title, boxScoreText)

  def editGameThread(accessToken: String, id36: String, newbody: String): RedditResponse = editThread(accessToken, id36, newbody)

  def postReplyToThread(accessToken: String, id36: String, commentBody: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditCommentRequest(
        thing = id36,
        text = commentBody
      )
    )

  def sendMessage(accessToken: String, targetUser: User, messageTitle: String, messageBody: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditMessageRequest(
        targetUser = targetUser.redditName,
        title = messageTitle,
        message = messageBody
      )
    )

  def getMessages(accessToken: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditInboxRequest
    )

  def replyToMessage(accessToken: String, parent: String, message: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditReplyRequest(
        parent = parent,
        text = message
      )
    )

  def markMessageRead(accessToken: String, id36: String): RedditResponse =
    authenticatedRequest(
      accessToken,
      RedditMarkReadRequest(
        thing = id36
      )
    )

}
