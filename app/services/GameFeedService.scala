package services

import javax.inject.{Inject, Singleton}
import model.DiscordTypes.{DiscordRichEmbed, DiscordRichEmbedField}
import model.db._
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc.RequestHeader

import scala.concurrent.Await
import scala.concurrent.duration._

@Singleton
class GameFeedService @Inject()(ws: WSClient) {

  private def basesString(state: GameState): String = {
    if (state.r1Id.isEmpty && state.r2Id.isEmpty && state.r3Id.isEmpty)
      "Bases Empty"
    else {
      s"R${state.r1Id.map(_ => "1").getOrElse("")}${state.r2Id.map(_ => "2").getOrElse("")}${state.r3Id.map(_ => "3").getOrElse("")}"
    }
  }

  private def postToWebhook(game: Game, action: GameAction, webhook: String)(implicit req: RequestHeader): Unit = {
    try {
      Await.result(
        ws.url(webhook)
          .post(
            Json.obj(
              "embeds" -> Json.arr(
                Json.toJson(
                  DiscordRichEmbed(
                    title = Some(game.name),
                    url = game.id36.map("https://redd.it/" + _),
                    color = Some(if (game.state.inning % 2 == 1) game.awayTeam.colorDiscord else game.homeTeam.colorDiscord),
                    description = Some(
                      s"${action.batter.fullName} batting against ${action.pitcher.fullName} in ${game.state.inningStr} with ${game.state.outs} Out(s)\n\n" +
                        s"Swing: ${action.swing.map(_.toString).getOrElse("x")}\n" +
                        s"Pitch: ${action.pitch.map(_.toString).getOrElse("x")}\n" +
                        s"Diff: ${action.diff.map(_.toString).getOrElse("x")} -> ${if (action.result.contains("FO") && action.runsScored == 1) "Sac" else action.result}${if (action.outsTracked == 3) " (TP)" else if (action.outsTracked == 2) " (DP)" else ""}\n\n" +
                        s"${basesString(action.afterState)}, ${action.afterState.inningStr} with ${action.afterState.outs} Out(s)\n\n" +
                        s"${game.awayTeam.tag} ${action.afterState.scoreAway} - ${action.afterState.scoreHome} ${game.homeTeam.tag}"
                    ),
                    fields = Some(
                      Seq(
                        DiscordRichEmbedField(
                          name = "View On Redditball",
                          value = s"[Link](${controllers.routes.GameController.viewGame(game.id.get).absoluteURL})",
                          inline = Some(true)
                        ),
                        DiscordRichEmbedField(
                          name = "View On Reddit",
                          value = s"[Link](${game.id36.map("https://redd.it/" + _).getOrElse("x")})",
                          inline = Some(true)
                        )
                      )
                    )
                  )
                )
              )
            )
          ),
        15.seconds
      )
    } catch {
      case e: Exception => e.printStackTrace()
    }
  }

  def postResult(game: Game, action: GameAction)(implicit req: RequestHeader): Unit = {
    game.awayTeam.webhook.foreach(postToWebhook(game, action, _))
    game.homeTeam.webhook.foreach(postToWebhook(game, action, _))
  }

}
