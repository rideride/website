package filters

import java.sql.{Date, Timestamp}

import akka.stream.Materializer
import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db._
import model.db.util._
import play.api.Logging
import play.api.mvc.{Filter, RequestHeader, Result}
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ActionLoggingFilter @Inject()(implicit db: FBDatabaseProvider, val mat: Materializer, ec: ExecutionContext) extends Filter with Logging {

  def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    // Break early if path is excluded
    val path = requestHeader.path
    if (path.startsWith("/assets/") || path == "/favicon.ico")
      return nextFilter(requestHeader)

    val startTime = System.currentTimeMillis()
    val method = requestHeader.method
    val user = requestHeader.session.get("uid").map(_.toInt)
    val ip = requestHeader.headers.get("X-Real-IP").getOrElse(requestHeader.remoteAddress)

    nextFilter(requestHeader).map { result =>
      val endTime = System.currentTimeMillis()
      val code = result.header.status

      val message = s"$method ${requestHeader.uri} took ${endTime - startTime}ms returning $code"
      if (code >= 200 && code < 400)
        logger.info(message)
      else if (code >= 400 && code < 500)
        logger.warn(message)
      else if (code >= 500 && code < 600)
        logger.error(message)
      else
        logger.warn(s"UNKOWN RESPONSE TYPE: $message")

      (ActionLogs += ActionLog(None, path, method, new Timestamp(startTime), new Timestamp(endTime), code, user, ip, if (requestHeader.queryString.nonEmpty) Some(requestHeader.rawQueryString) else None)).run

      user.foreach { user =>
        UniqueAccessPoints.insertOrUpdate(UniqueAccessPoint(user, ip, new Date(startTime))).run
      }

      result
    }
  }

}
