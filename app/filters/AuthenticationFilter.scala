package filters

import akka.stream.Materializer
import javax.inject.{Inject, Singleton}
import model.FBDatabaseProvider
import model.db.{User, Users}
import play.api.Logging
import play.api.libs.typedmap.TypedKey
import play.api.mvc.{Filter, RequestHeader, Result}

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class AuthenticationFilter @Inject()(implicit db: FBDatabaseProvider, val mat: Materializer, ec: ExecutionContext) extends Filter with Logging {

  override def apply(nextFilter: RequestHeader => Future[Result])(requestHeader: RequestHeader): Future[Result] = {
    // Break early if path is excluded
    val path = requestHeader.path
    if (path.startsWith("/assets/") || path == "/favicon.ico")
      return nextFilter(requestHeader)

    // add user obj to req attr
    val userId = requestHeader.session.get("uid").map(_.toInt)
    val user = userId.flatMap(Users.findById)
    val newHeader = requestHeader.addAttr(AuthenticationFilter.USER_ATTRIBUTE_KEY, user)

    // continue chain
    nextFilter(newHeader)
  }

}

object AuthenticationFilter {

  private[filters] val USER_ATTRIBUTE_KEY: TypedKey[Option[User]] = TypedKey("FB_USER")

  implicit class ReqExtensions(requestHeader: RequestHeader) {
    def user: Option[User] = requestHeader.attrs.get(USER_ATTRIBUTE_KEY).flatten
  }

}
