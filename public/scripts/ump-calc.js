const PLAY_TYPE_AUTO_K = "2";
const PLAY_TYPE_AUTO_BB = "3";
const PLAY_TYPE_STEAL = "6";
const PLAY_TYPE_MULTI_STEAL = "7";
const PLAY_TYPE_IBB = "8";

function updateState(newVal) {
    // Swing and pitch on auto plays
    if (newVal === PLAY_TYPE_AUTO_K || newVal === PLAY_TYPE_AUTO_BB || newVal === PLAY_TYPE_IBB) {
        $("#pitch").attr('disabled', 'disabled');
        $("#swing").attr('disabled', 'disabled');
    } else {
        $("#pitch").attr('disabled', null);
        $("#swing").attr('disabled', null);
    }

    // Batter drop down on steals
    if (newVal === PLAY_TYPE_STEAL || newVal === PLAY_TYPE_MULTI_STEAL)
        $("#batter").attr('disabled', null);
    else
        $("#batter").attr('disabled', 'disabled');
}

$("#playType").change(function () {
    updateState($(this).val());
});

$("#batter").change(function () {
    $("#batter-hidden").val($(this).val());
});

updateState($("playType").val());
