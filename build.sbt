name := "fakebaseballwebsite"

version := "1.0"

lazy val `fakebaseballwebsite` = (project in file(".")).enablePlugins(PlayScala, BuildInfoPlugin)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "https://repo.akka.io/snapshots/"

scalaVersion := "2.12.10"

buildInfoKeys := Seq(
  name,
  version,
  scalaVersion,
  sbtVersion,
  BuildInfoKey.action("buildTime") {
    System.currentTimeMillis
  },
  BuildInfoKey.action("gitSha") {
    import sys.process._
    try {
      "git rev-parse --short HEAD".!!.trim
    } catch {
      case _: Exception => "Unknown"
    }
  }
)

watchSources := watchSources.value.filter { _.base.getName != "BuildInfo.scala" }

libraryDependencies ++= Seq(
  guice,
  ws,
  "mysql" % "mysql-connector-java" % "8.0.12",
  "com.typesafe.play" %% "play-slick" % "4.0.0",
  "org.jsoup" % "jsoup" % "1.11.3",
  "ai.x" %% "play-json-extensions" % "0.40.2",
  "org.flywaydb" % "flyway-core" % "6.3.3",
  "com.chuusai" %% "shapeless" % "2.3.3",
  "io.underscore" %% "slickless" % "0.3.6",
)

// don't compile docs
sources in (Compile, doc) := Seq.empty
publishArtifact in (Compile, packageDoc) := false
