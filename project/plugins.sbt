logLevel := Level.Warn

resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.8.0")
addSbtPlugin("io.github.davidmweber" % "flyway-sbt" % "5.2.0")
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.9.0")

libraryDependencies += "com.typesafe.slick" %% "slick-codegen" % "3.3.0"
libraryDependencies += "com.typesafe" % "config" % "1.3.3"
libraryDependencies += "mysql" % "mysql-connector-java" % "8.0.12"
libraryDependencies += "com.typesafe.slick" %% "slick-hikaricp" % "3.3.0"
