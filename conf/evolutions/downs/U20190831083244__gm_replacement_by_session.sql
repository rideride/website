alter table gm_assignments
    add column start_stamp timestamp not null default current_timestamp,
    add column end_stamp timestamp null;

update gm_assignments
    set end_stamp=current_timestamp
    where end_season is not null;

alter table gm_assignments
    drop column start_season,
    drop column start_session,
    drop column end_season,
    drop column end_session;
