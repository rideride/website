alter table pitching_stats
    drop primary key,
    add primary key (player, season);

alter table pitching_stats
    drop column milr;

alter table batting_stats
    drop column milr;

alter table batting_stats
    drop primary key,
    add primary key (player, season);