alter table games
    drop foreign key fk_games_state,
    drop column state;

alter table game_actions
    add column result_away_batting_position int not null,
    add column result_home_batting_position int not null,
    add column result_player_on_first int null,
    add column result_player_on_second int null,
    add column result_player_on_third int null,
    add column result_outs int not null,
    add column result_inning int not null,
    add column result_score_away int not null,
    add column result_score_home int not null,
    add foreign key fk_game_action_player_on_first (result_player_on_first) references players(id),
    add foreign key fk_game_action_player_on_second (result_player_on_second) references players(id),
    add foreign key fk_game_action_player_on_third (result_player_on_third) references players(id);

update game_actions a
set result_away_batting_position=(select away_batting_position from game_states where id = a.after_state),
    result_home_batting_position=(select home_batting_position from game_states where id = a.after_state),
    result_player_on_first=(select r1 from game_states where id = a.after_state),
    result_player_on_second=(select r2 from game_states where id = a.after_state),
    result_player_on_third=(select r3 from game_states where id = a.after_state),
    result_outs=(select outs from game_states where id = a.after_state),
    result_inning=(select inning from game_states where id = a.after_state),
    result_score_away=(select score_away from game_states where id = a.after_state),
    result_score_home=(select score_home from game_states where id = a.after_state);

alter table game_actions
    drop foreign key fk_game_action_before_state,
    drop foreign key fk_game_action_after_state,
    drop column before_state,
    drop column after_state;

drop table game_states;