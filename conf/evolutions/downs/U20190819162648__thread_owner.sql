alter table games
    drop foreign key fk_games_thread_owner,
    drop column thread_owner;
