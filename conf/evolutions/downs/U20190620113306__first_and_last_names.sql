ALTER TABLE players
    DROP COLUMN last_name,
    DROP COLUMN first_name;

ALTER TABLE player_applications
    RENAME COLUMN last_name TO requested_name;

ALTER TABLE player_applications
    DROP COLUMN first_name,
    MODIFY COLUMN requested_name VARCHAR(60) NOT NULL;
