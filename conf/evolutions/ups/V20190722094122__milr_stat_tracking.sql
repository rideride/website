alter table pitching_stats
    add column milr boolean not null after season;

alter table pitching_stats
    drop primary key,
    add primary key (player, season, milr);

alter table batting_stats
    add column milr boolean not null after season;

alter table pitching_stats
    drop primary key,
    add primary key (player, season, milr);