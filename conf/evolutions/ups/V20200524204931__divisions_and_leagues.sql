create table leagues (
    id int not null auto_increment primary key,
    name varchar(64) not null,
    tag varchar(4) not null
);

create table divisions (
    id int not null auto_increment primary key,
    name varchar(64) not null,
    tag varchar(4) not null,
    league int null,
    foreign key fk_divisions_league (league) references leagues (id)
);

alter table teams
    add column division int null,
    add foreign key fk_teams_division (division) references divisions (id);
