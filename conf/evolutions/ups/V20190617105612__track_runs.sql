ALTER TABLE game_actions
    ADD COLUMN scorers VARCHAR(64) NULL DEFAULT NULL AFTER runs_scored;
