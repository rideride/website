update game_actions b, game_actions a
set b.after_state=a.after_state,
    b.replaced_by=a.replaced_by
where b.replaced_by = a.id
  and a.play_type = 0;

delete from game_actions
where play_type = 0;

alter table game_actions
    drop foreign key fk_game_action_pitcher,
    drop foreign key fk_game_action_batter;

alter table game_actions
    modify column pitcher int not null,
    modify column batter int not null,
    modify column result varchar(20) not null;

alter table game_actions
    add foreign key fk_game_actions_pitcher (pitcher) references players (id),
    add foreign key fk_game_actions_batter (batter) references players (id);
