alter table gm_assignments
    add column start_season int not null after gm,
    add column start_session int not null after start_season,
    add end_season int null after start_session,
    add end_session int null after end_season;

update gm_assignments
    set end_season=0, end_session=0
    where end_stamp is not null;

alter table gm_assignments
    drop column start_stamp,
    drop column end_stamp;
