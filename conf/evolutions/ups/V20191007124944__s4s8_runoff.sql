create table s4s8_votes_runoff
(
    user                 int                                 not null primary key,
    submission_timestamp timestamp default CURRENT_TIMESTAMP not null,
    option1              varchar(30)                         not null,
    foreign key fk_s4s8_votes_runoff_user (user) references users (id)
);
