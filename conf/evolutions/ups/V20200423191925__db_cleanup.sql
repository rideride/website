drop table robo_umpire_games;

alter table player_applications
    add foreign key fk_player_applications_user (user) references users (id);

alter table teams
    modify column discord_role varchar(18) null;

alter table users
    drop column is_committee;

alter table players
    rename column rightHanded to right_handed;

# noinspection SqlWithoutWhere
update users u
    set is_player=((select count(*) from players where user=u.id) != 0);
