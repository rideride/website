alter table games
    drop column milr,
    drop column game_start;

delete from batting_stats where milr;

alter table batting_stats
    drop column milr;

delete from pitching_stats where milr;

alter table pitching_stats
    drop column milr;

alter table teams
    drop column milr,
    drop foreign key fk_team_milr_team;

alter table teams
    drop column milr_team;