create table s4s8_votes (
    user int not null primary key,
    submission_timestamp timestamp not null default current_timestamp,
    option1 varchar(30) not null,
    foreign key fk_s4s8_votes_user (user) references users (id)
);
