create table if not exists action_logs
(
    id              int         not null auto_increment primary key,
    request_path    text        not null,
    request_method  varchar(10) not null,
    request_stamp   timestamp   not null,
    response_stamp  timestamp   not null,
    response_code   int         not null,
    requesting_user int         null,
    src_ip          varchar(50) not null,
    foreign key fk_action_log_req_user (requesting_user) references users (id)
);