create table player_nicknames (
    player int not null primary key references players (id),
    first_name varchar(15) null,
    last_name varchar(15) not null
);
