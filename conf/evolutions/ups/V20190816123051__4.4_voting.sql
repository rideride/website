create table s4s4_votes (
    user int not null primary key,
    submission_stamp timestamp not null default current_timestamp,
    option1 varchar(30) not null,
    option2 varchar(30) not null,
    option3 varchar(30) not null,
    option4 varchar(30) not null,
    option5 varchar(30) not null,
    foreign key fk_s4s4_votes_user (user) references users(id)
);