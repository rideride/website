create table s4s13_votes (
                            user int not null primary key,
                            submission_timestamp timestamp not null default current_timestamp,
                            option1 boolean not null,
                            option2 boolean not null,
                            option3 boolean not null,
                            option4 boolean not null,
                            option5 boolean not null,
                            option6 boolean not null,
                            option7 boolean not null,
                            option8 boolean not null,
                            foreign key fk_s4s13_votes_user (user) references users (id)
);
