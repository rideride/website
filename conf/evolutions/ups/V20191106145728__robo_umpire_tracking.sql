create table robo_umpire_games (
    game int not null primary key,
    stored_pitch int null,
    waiting_on varchar(15) not null,
    waiting_until timestamp not null,
    paused boolean not null default false,
    foreign key fk_robo_umpire_games_game (game) references games (id)
);
