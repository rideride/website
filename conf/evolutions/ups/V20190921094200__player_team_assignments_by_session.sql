create table player_team_assignments (
    player int not null,
    team int null,
    start_season int not null,
    start_session int not null,
    primary key (player, start_season, start_session),
    foreign key fk_player_team_assignments_player (player) references players (id),
    foreign key fk_player_team_assignments_team (team) references teams (id)
);
