create table unique_access_points
(
    requesting_user int          not null,
    src_ip          varchar(128) not null,
    req_date        date         not null,
    foreign key fk_unique_access_points_requesting_user (requesting_user) references users (id),
    primary key (requesting_user, src_ip, req_date)
);

insert into unique_access_points (requesting_user, src_ip, req_date)
select a.requesting_user, a.src_ip, cast(a.request_stamp as date)
from action_logs a
where a.requesting_user is not null
on duplicate key update requesting_user=a.requesting_user;

delete
from action_logs
where response_code in (200, 301, 303);
