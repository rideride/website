alter table batting_stats
    add column summed_diff int not null default 0;

alter table pitching_stats
    add column total_abs int not null default 0 after total_pas,
    add column summed_diff int not null default 0,
    add column wins int not null default 0,
    add column losses int not null default 0,
    add column saves int not null default 0;
