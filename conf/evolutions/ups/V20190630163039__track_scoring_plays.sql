create table scoring_plays (
    game_action int not null primary key,
    scorer1 int null,
    scorer1_responsibility int null,
    scorer2 int null,
    scorer2_responsibility int null,
    scorer3 int null,
    scorer3_responsibility int null,
    scorer4 int null,
    scorer4_responsibility int null,
    description text not null,
    foreign key fk_scoring_plays_game_action (game_action) references game_actions(id),
    foreign key fk_scoring_plays_scorer1 (scorer1) references players(id),
    foreign key fk_scoring_plays_scorer1_responsibility (scorer1_responsibility) references players(id),
    foreign key fk_scoring_plays_scorer2 (scorer2) references players(id),
    foreign key fk_scoring_plays_scorer2_responsibility (scorer2_responsibility) references players(id),
    foreign key fk_scoring_plays_scorer3 (scorer3) references players(id),
    foreign key fk_scoring_plays_scorer3_responsibility (scorer3_responsibility) references players(id)
);