alter table game_actions
    drop foreign key fk_game_action_previous_play,
    drop foreign key fk_game_actions_batter,
    drop foreign key fk_game_actions_pitcher,
    drop foreign key fk_game_action_game,
    drop foreign key fk_game_action_after_state,
    drop foreign key fk_game_action_before_state;

alter table game_actions
    add foreign key fk_game_actions_replaced_by (replaced_by) references game_actions (id) on update cascade on delete set null,
    add foreign key fk_game_actions_batter (batter) references players (id) on update cascade on delete cascade,
    add foreign key fk_game_actions_pitcher (pitcher) references players (id) on update cascade on delete cascade,
    add foreign key fk_game_actions_game (game_id) references games (id) on update cascade on delete cascade,
    add foreign key fk_game_actions_after_state (after_state) references game_states (id) on update cascade on delete cascade,
    add foreign key fk_game_actions_before_state (before_state) references game_states (id) on update cascade on delete cascade;

alter table user_preferences
    drop foreign key user_preferences_user;

alter table user_preferences
    add foreign key fk_user_preferences_user (user) references users (id) on update cascade on delete cascade;
