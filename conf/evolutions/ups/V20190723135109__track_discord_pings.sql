create table discord_pings (
    id int not null auto_increment primary key,
    pinger int not null,
    receiver int not null,
    game int not null,
    game_state int not null,
    stamp timestamp not null default current_timestamp,
    foreign key fk_discord_pings_pinger (pinger) references users (id),
    foreign key fk_discord_pings_receiver (receiver) references users (id),
    foreign key fk_discord_pings_game (game) references games (id) on delete cascade on update cascade,
    foreign key fk_discord_pings_game_state (game_state) references game_states (id) on delete cascade on update cascade
);
