-- In V20190507221100__player_info.sql, the parks were entered with their factors backwards
-- this migration fixes those

UPDATE parks SET factor_hr=1.000, factor_3b=1.000, factor_2b=1.000, factor_1b=1.000, factor_bb=1.000 WHERE parks.name='No Stadium';
UPDATE parks SET factor_hr=0.879, factor_3b=1.042, factor_2b=1.032, factor_1b=0.963, factor_bb=1.000 WHERE parks.name='Fake Shea Stadium';
UPDATE parks SET factor_hr=0.763, factor_3b=0.699, factor_2b=0.966, factor_1b=0.969, factor_bb=1.000 WHERE parks.name='Urban Chestnut Field';
UPDATE parks SET factor_hr=0.756, factor_3b=0.696, factor_2b=1.100, factor_1b=0.919, factor_bb=0.950 WHERE parks.name='O.Co Coliseum';
UPDATE parks SET factor_hr=1.083, factor_3b=1.209, factor_2b=1.425, factor_1b=1.097, factor_bb=0.815 WHERE parks.name='New Angel Stadium';
UPDATE parks SET factor_hr=0.972, factor_3b=0.970, factor_2b=0.950, factor_1b=0.973, factor_bb=0.960 WHERE parks.name='Petco Park';
UPDATE parks SET factor_hr=1.040, factor_3b=1.270, factor_2b=1.060, factor_1b=1.090, factor_bb=1.000 WHERE parks.name='Dodger Stadium at Chase Field';
UPDATE parks SET factor_hr=0.650, factor_3b=1.034, factor_2b=1.354, factor_1b=0.969, factor_bb=1.000 WHERE parks.name='Astroworld';
UPDATE parks SET factor_hr=0.946, factor_3b=0.922, factor_2b=0.974, factor_1b=0.998, factor_bb=1.000 WHERE parks.name='Flamingo Park';
UPDATE parks SET factor_hr=1.025, factor_3b=1.199, factor_2b=1.007, factor_1b=0.996, factor_bb=1.039 WHERE parks.name='Wrigley Field';
UPDATE parks SET factor_hr=1.000, factor_3b=1.000, factor_2b=1.000, factor_1b=1.000, factor_bb=1.000 WHERE parks.name='Beisbolcat Park';
UPDATE parks SET factor_hr=1.057, factor_3b=0.696, factor_2b=1.003, factor_1b=0.939, factor_bb=0.808 WHERE parks.name='Dodger Stadium';
UPDATE parks SET factor_hr=1.394, factor_3b=1.513, factor_2b=1.332, factor_1b=1.232, factor_bb=1.232 WHERE parks.name='The \'Foam Doam';
UPDATE parks SET factor_hr=1.212, factor_3b=1.554, factor_2b=1.304, factor_1b=0.840, factor_bb=1.000 WHERE parks.name='Kauffman Stadium';
UPDATE parks SET factor_hr=0.990, factor_3b=0.950, factor_2b=0.950, factor_1b=1.060, factor_bb=1.000 WHERE parks.name='PNC Park';
UPDATE parks SET factor_hr=0.900, factor_3b=0.870, factor_2b=0.900, factor_1b=1.100, factor_bb=1.030 WHERE parks.name='Basketball Court';
UPDATE parks SET factor_hr=0.650, factor_3b=0.615, factor_2b=0.643, factor_1b=0.808, factor_bb=0.840 WHERE parks.name='Polo Grounds';
UPDATE parks SET factor_hr=1.080, factor_3b=0.971, factor_2b=0.975, factor_1b=0.972, factor_bb=1.000 WHERE parks.name='Wawa Park at Spring Gardens';
UPDATE parks SET factor_hr=0.872, factor_3b=0.911, factor_2b=1.032, factor_1b=0.925, factor_bb=0.925 WHERE parks.name='Steve Irwin Memorial Park';
UPDATE parks SET factor_hr=0.999, factor_3b=0.879, factor_2b=0.955, factor_1b=0.956, factor_bb=0.956 WHERE parks.name='Stade Enterprise';
UPDATE parks SET factor_hr=0.650, factor_3b=1.513, factor_2b=1.332, factor_1b=1.000, factor_bb=1.000 WHERE parks.name='DiNAMO Park';
