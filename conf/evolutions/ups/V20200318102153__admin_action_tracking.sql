create table admin_logs (
    id int not null primary key,
    acting_user int not null,
    action_type int not null,
    log_message text not null,
    foreign key fk_admin_logs_acting_user (acting_user) references users (id)
);
