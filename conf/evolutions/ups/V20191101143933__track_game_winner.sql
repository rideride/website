alter table games
    add column winning_team int null after completed,
    add foreign key fk_games_winning_team (winning_team) references teams (id);

update games g
set winning_team = (case
                        when (select score_away from game_states where id = g.state) >
                             (select score_home from game_states where id = g.state) then g.away_team
                        when (select score_home from game_states where id = g.state) >
                             (select score_away from game_states where id = g.state) then g.home_team
                        else null end);
