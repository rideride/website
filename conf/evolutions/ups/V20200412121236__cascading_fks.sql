alter table players
    drop foreign key fk_player_user;
alter table players
    add foreign key fk_player_user (user) references users (id) on update cascade;

alter table batting_stats
    drop foreign key fk_batting_stats_player;
alter table batting_stats
    add foreign key fk_batting_stats_player (player) references players (id) on update cascade;

alter table game_actions
    drop foreign key fk_game_action_batter,
    drop foreign key fk_game_action_pitcher;
alter table game_actions
    add foreign key fk_game_action_batter (batter) references players (id) on update cascade,
    add foreign key fk_game_action_pitcher (pitcher) references players (id) on update cascade;

alter table game_awards
    drop foreign key fk_game_awards_winning_pitcher,
    drop foreign key fk_game_awards_losing_pitcher,
    drop foreign key fk_game_awards_player_of_the_game,
    drop foreign key fk_game_awards_save;
alter table game_awards
    add foreign key fk_game_awards_winning_pitcher (winning_pitcher) references players (id) on update cascade,
    add foreign key fk_game_awards_losing_pitcher (losing_pitcher) references players (id) on update cascade,
    add foreign key fk_game_awards_player_of_the_game (player_of_the_game) references players (id) on update cascade,
    add foreign key fk_game_awards_save (save) references players (id) on update cascade;

alter table game_states
    drop foreign key fk_game_state_r1,
    drop foreign key fk_game_state_r2,
    drop foreign key fk_game_state_r3,
    drop foreign key fk_game_state_r1_responsibility,
    drop foreign key fk_game_state_r2_responsibility,
    drop foreign key fk_game_state_r3_responsibility;
alter table game_states
    add foreign key fk_game_state_r1 (r1) references players (id) on update cascade,
    add foreign key fk_game_state_r2 (r2) references players (id) on update cascade,
    add foreign key fk_game_state_r3 (r3) references players (id) on update cascade,
    add foreign key fk_game_state_r1_responsibility (r1_responsibility) references players (id) on update cascade,
    add foreign key fk_game_state_r2_responsibility (r2_responsibility) references players (id) on update cascade,
    add foreign key fk_game_state_r3_responsibility (r3_responsibility) references players (id) on update cascade;

alter table gm_assignments
    drop foreign key fk_gm_assignments_gm;
alter table gm_assignments
    add foreign key fk_gm_assignments_gm (gm) references players (id) on update cascade;

alter table lineup_entries
    drop foreign key lineup_substitutions_player;
alter table lineup_entries
    add foreign key lineup_substitutions_player (player) references players (id) on update cascade;

alter table pitching_stats
    drop foreign key fk_pitching_stats_player;
alter table pitching_stats
    add foreign key fk_pitching_stats_player (player) references players (id) on update cascade;

alter table player_team_assignments
    drop foreign key fk_player_team_assignments_player;
alter table player_team_assignments
    add foreign key fk_player_team_assignments_player (player) references players (id) on update cascade;

alter table scoring_plays
    drop foreign key fk_scoring_plays_scorer1,
    drop foreign key fk_scoring_plays_scorer2,
    drop foreign key fk_scoring_plays_scorer3,
    drop foreign key fk_scoring_plays_scorer1_responsibility,
    drop foreign key fk_scoring_plays_scorer2_responsibility,
    drop foreign key fk_scoring_plays_scorer3_responsibility;
alter table scoring_plays
    add foreign key fk_scoring_plays_scorer1 (scorer1) references players (id) on update cascade,
    add foreign key fk_scoring_plays_scorer2 (scorer2) references players (id) on update cascade,
    add foreign key fk_scoring_plays_scorer3 (scorer3) references players (id) on update cascade,
    add foreign key fk_scoring_plays_scorer4 (scorer4) references players (id) on update cascade,
    add foreign key fk_scoring_plays_scorer1_responsibility (scorer1_responsibility) references players (id) on update cascade,
    add foreign key fk_scoring_plays_scorer2_responsibility (scorer2_responsibility) references players (id) on update cascade,
    add foreign key fk_scoring_plays_scorer3_responsibility (scorer3_responsibility) references players (id) on update cascade,
    add foreign key fk_scoring_plays_scorer4_responsibility (scorer4_responsibility) references players (id) on update cascade;

alter table trade_block_entries
    drop foreign key fk_trade_block_entries_player;
alter table trade_block_entries
    add foreign key fk_trade_block_entries_player (player) references players (id);
