ALTER TABLE players
    ADD COLUMN last_name VARCHAR(15) NOT NULL AFTER name,
    ADD COLUMN first_name VARCHAR(15) NULL AFTER name;

ALTER TABLE player_applications
    RENAME COLUMN requested_name TO last_name;

ALTER TABLE player_applications
    MODIFY COLUMN last_name VARCHAR(15) NOT NULL,
    ADD COLUMN first_name VARCHAR(15) NULL AFTER last_name;

UPDATE players
SET first_name=SUBSTRING(SUBSTRING_INDEX(name, ' ', 1), 1, 15),
    last_name=SUBSTRING(SUBSTRING_INDEX(name, ' ', -1), 1, 15);
