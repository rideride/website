alter table teams
    add column visible bool not null default true,
    add column all_players bool not null default false;