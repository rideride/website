create table trade_block_entries
(
    player int  not null primary key,
    notes  text not null,
    foreign key fk_trade_block_entries_player (player) references players (id)
);

create table team_trade_block_notes
(
    team  int  not null primary key,
    notes text not null,
    last_updated timestamp not null default current_timestamp on update current_timestamp,
    foreign key fk_team_trade_block_notes_team (team) references teams (id)
);
