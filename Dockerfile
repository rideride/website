FROM llamos/sbt:1.3.8 AS sbt
RUN apt-get update && apt-get install git -y
WORKDIR /workspace
COPY . /workspace
RUN sbt clean dist && sync

FROM garthk/unzip AS unzip
WORKDIR /workspace
COPY --from=sbt /workspace/target/universal/fakebaseballwebsite-1.0.zip dist.zip
RUN unzip dist.zip -d dist/
RUN rm -f dist/fakebaseballwebsite-1.0/conf/development.conf

FROM java:8-jre-alpine
RUN apk update && apk upgrade && apk add bash
COPY --from=unzip /workspace/dist/fakebaseballwebsite-1.0/ /srv
ENTRYPOINT [ "/bin/bash", "/srv/bin/fakebaseballwebsite", "-Dhttp.port=80" ]
